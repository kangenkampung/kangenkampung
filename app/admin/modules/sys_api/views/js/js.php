<script type="text/javascript">
    $(document).ready(function() {

    	  var form_data = new FormData();
    	  form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_api","<?php echo base_url().$this->config->item('index_page'); ?>/sys_api/ajax_action_datatable_api_key",form_data);
        } );

 function ajax_action_add_api(){
        	var form_data = new FormData();
			form_data.append('name_api', $('#name_api').val());
			form_data.append('api_key', $('#api_key').val());
			form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
			addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/sys_api/ajax_action_add_api_key/","POST",form_data);
        }
</script>

<script type="text/javascript">
	// Ajax Edit Admin-----------------------------------------------------
   function ajax_action_edit_api(){
          var form_data = new FormData();
          var name_api =$('#edit_name_api').val();
          var api_key =$('#edit_api_key').val();
          var status_api =$('#edit_status_api ').val();
          var id = $('#edit_id_api').val();
          form_data.append('name_api',name_api);
          form_data.append('api_key', api_key);
          form_data.append('id_api', id);
          form_data.append('status_api', status_api);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>sys_api/ajax_action_get_api_key/"+id,"POST",form_data);
            }



        function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>sys_api/ajax_action_get_api_key/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var name_api =$('#edit_name_api').val(response.data[0].name_api);
              var api_key =$('#edit_api_key').val(response.data[0].api_key);
              var id_api_key =$('#edit_id_api').val(response.data[0].id_api);
              $("#edit_status_api option[value='"+response.data[0].status_api+"']").attr('selected','selected');
             $('#page-load').hide();
          });
         $('#edit-api').modal("show");
          
        }
</script>