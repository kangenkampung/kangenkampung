<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sys_api extends MY_Controller {

	public function __construct() {
		$this->load->model('M_sys');
		$this->load->library('recaptcha');
	}

		public function index(){
		$data['content'] = 'sys_api';
		$this->load->view('template',$data);	
		
	}


	public function ajax_action_add_api_key(){
		$this->form_validation->set_rules('name_api', 'name_api', 'required');
		$this->form_validation->set_rules('api_key', 'api_key', 'required');


		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$data = array(
				"name_api" => post("name_api"),
				"api_key" =>post("api_key"),
				"status_api" =>1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_sys->insert_table("sys_api",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/sys_api'
				);
				print json_encode($json_data);
			}

		}
	}

	function ajax_action_datatable_api_key(){
		$column = "*";
		$table = "sys_api";
		$column_order = array('name_api', 'api_key','created_at');
		$column_search = array('name_api', 'api_key','created_at');
		$order = array('id_api' => 'DESC'); 
		$where = '';
		$joins = '';
		$list = $this->M_sys->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		
		$link = ''.base_url().$this->config->item('index_page').'/sys_api';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
				if($key->status_api == 1){
				$status = "Active";
			}else{
				$status = "Disable";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_api;
			$row[] = $key->api_key;
			$row[] = $status;
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete('."'".$link."'".','."'ajax_action_delete_api_key'".','.$key->id_api.')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal('.$key->id_api.')" value="Edit">';
			$data[] = $row;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_sys->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_sys->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

    function ajax_action_get_api_key(){
		$id = $this->uri->segment(3);
		$data = $this->M_sys->fetch_table('*', 'sys_api', 'id_api = '. $id, '', '', 0, 0 ,TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_api_key(){
		$id = $this->uri->segment(3);
		// $delete = $this->M_sys->delete_table("sys_api","id_api",$id);
		// $this->db->trans_start();
		$data = array(
					"status_api" =>3,
		);
		

		$delete = $this->M_sys->update_table('sys_api', $data, 'id_api', $id);
		if($delete==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/sys_api'
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_edit_api_key(){
		$this->form_validation->set_rules('name_api', 'name_api', 'required');
		$this->form_validation->set_rules('api_key', 'api_key', 'required');
		$this->form_validation->set_rules('status_api', 'api_key', 'required');

		### FORM VALIDATION
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}
		$id = $this->uri->segment(3);

		### TRANSACTION START
		$this->db->trans_start();
				$data = array(
					"name_api" => post("name_api"),
					"api_key" => post("api_key"),
					"status_api" =>post("status_api"),
					'update_at' => date('Y-m-d H:m:s')
				);
		

		$edit = $this->M_sys->update_table('sys_api', $data, 'id_api', $id);

		$this->db->trans_complete();
		### TRANSACTION END

		if($edit == FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal update Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses update Data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/sys_api/'
			);
			print json_encode($json_data);
		}
	}
}
?>