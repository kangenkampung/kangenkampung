<script type="text/javascript">
    $(document).ready(function() {
        var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>payment_method/ajax_action_get_payment_method_all";
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_payment_account","<?php echo base_url().$this->config->item('index_page'); ?>payment_account/ajax_action_datatable_payment_account",form_data);

           ajaxShowData(link2,"POST",form_data,function(response) {
              var html = '<option value="" selected="selected">Pilih Metode layanan</option>';
            var i;
        for(i=0; i<response.data.length; i++){
          html += "<option value='"+response.data[i].id_payment_method+"'>"+response.data[i].name_payment_method+"</option>";
        }
        $('#id_payment_method').html(html);
        $('#edit_select_id_payment_method').html(html);
          });
           

    });

</script>

<script type="text/javascript">
  // Add Category

    function ajax_action_add_payment_account(){
          var form_data = new FormData();
          form_data.append('id_payment_method', $('#id_payment_method').val());
          form_data.append('name_payment_account', $('#name_payment_account').val());
          form_data.append('nomor_payment_account', $('#nomor_payment_account').val());
          form_data.append('type_payment_account', $('#type_payment_account').val());



          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_add_payment_account/","POST",form_data);
        }

     function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>payment_account/ajax_action_get_payment_account/"+id;
          var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>payment_method/ajax_action_get_payment_method_all";
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

           

          

            
          ajaxShowData(link,"POST",form_data,function(response) {
              var name_payment_account =$('#edit_name_payment_account').val(response.data[0].name_payment_account);
              var id_payment_acccount =$('#edit_id_payment_account').val(response.data[0].id_payment_account);
              var nomor_payment_account =$('#edit_nomor_payment_account').val(response.data[0].nomor_payment_account);
              $("#edit_status_payment_account option[value='"+response.data[0].status_payment_account+"']").attr('selected','selected');
              $("#edit_select_id_payment_method option[value='"+response.data[0].id_payment_method+"']").attr('selected','selected');
             $('#page-load').hide();
          });

          
         $('#edit-payment-account').modal("show");
       
        }

         function ajax_action_edit_payment_account(){
          var form_data = new FormData();
          var name_payment_account =$('#edit_name_payment_account').val();
          var nomor_payment_account =$('#edit_nomor_payment_account').val();
          var status_payment_account = $('#edit_status_payment_account :selected').val();
          var type_payment_account = $('#edit_type_payment_account :selected').val();
          var id_payment_method = $('#edit_select_id_payment_method :selected').val();
          var id_payment_acccount = $('#edit_id_payment_account').val();
          form_data.append('name_payment_account',name_payment_account);
          form_data.append('nomor_payment_account', nomor_payment_account);
          form_data.append('status_payment_account', status_payment_account);
          form_data.append('type_payment_account', type_payment_account);
          form_data.append('id_payment_method', id_payment_method);
          form_data.append('id_payment_account', id_payment_acccount);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>payment_account/ajax_action_edit/","POST",form_data);
            }

</script>