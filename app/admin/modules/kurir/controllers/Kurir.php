<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurir extends MY_Controller {

	public function __construct() {
		$this->load->model('M_kurir');
		$this->load->library('recaptcha');
	}

		public function index(){
		$data['content'] = 'kurir';
		$this->load->view('template',$data);	
		
	}

	public function ajax_action_add_kurir(){
		$this->form_validation->set_rules('name_kurir', 'name_kurir', 'required');
		$this->form_validation->set_rules('api_kurir', 'name_kurir', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

			 if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			/*-------------setting attachment upload -------------*/
			$config['upload_path'] = './uploads/kurir/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')){
				$data['file_name'] = null;
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$data = $this->upload->data();
			}
		}else{
			$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload icon "),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
			die();
        }

       	 $data_file = array(
				"nama_file" => $data['file_name'],
				"created_at" => date("Y-m-d H:i:s")
			);
		$add_file = $this->M_kurir->insert_table("a__file",$data_file);

		if($add_file){
			  $last_data= $this->M_kurir->get_last_data('id_file', 'a__file','DESC');
			  $data = array(
				"name_kurir" => post("name_kurir"),
				"api_kurir" => post("api_kurir"),
				"status_kurir" => 1,
				"id_file" => $last_data->id_file,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_kurir->insert_table("a__kurir",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/kurir'
				);
				print json_encode($json_data);
			}
		}else{
			$json_data =  array(
					"result" => FALSE,
					"message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/kurir'
				);
				print json_encode($json_data);
			  }
		}
	}

	public function ajax_list_kurir(){
		$column = "a.*,b.*";
		$column_order = array('name_kurir','api_kurir');
		$column_search = array('name_kurir','api_kurir');
		$order = array('id_kurir' => 'DESC');
		$table = "a__kurir a";
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$list = $this->M_kurir->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		$data = array();
		$link = ''.base_url().$this->config->item('index_page').'/kurir';
		$no = $_POST['start'];
		foreach ($list as $key) {
			$row = array();		
			$row[] = $no+1;
			$row[] = $key->name_kurir;
			$row[] = $key->api_kurir;			
			$row[] = $key->status_kurir;	
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete('."'".$link."'".','."'ajax_action_delete_kurir'".','.$key->id_kurir.')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal('.$key->id_kurir.')" value="Edit">';	
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_kurir->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_kurir->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_action_delete_kurir(){
		$id = $this->uri->segment(3);
		$data = array(
					"status_kurir" => 3
				);
		$edit = $this->M_kurir->update_table('a__kurir', $data, 'id_kurir', $id);
		$this->db->trans_complete();
		if($edit==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/kurir'
			);
			print json_encode($json_data);
		}
	}

	  function ajax_action_get_kurir(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$data = $this->M_kurir->fetch_joins('a__kurir a', '*', $joins,'id_kurir = '. $id, '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_kurir(){
		$this->form_validation->set_rules('name_kurir', 'name_kurir', 'required');
		$this->form_validation->set_rules('api_kurir', 'name_kurir', 'required');
		$this->form_validation->set_rules('status_kurir', 'status_kurir', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				/*-------------setting attachment upload -------------*/
				$config['upload_path'] = './uploads/kurir/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 1024 * 8;
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file')){
					$data['file_name'] = null;
					$json_data =  array(
						"result" => FALSE ,
						"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
						"form_error" => 'file proof',
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				}else{
					$data['file_name_unlink'] = $this->M_kurir->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
					unlink('./uploads/kurir/' .  $data['file_name_unlink']);  
					$data = $this->upload->data();

					$data_foto = array(
						"nama_file" => $data['file_name']
					);

					$edit_foto = $this->M_kurir->update_table("a__file",$data_foto,"id_file",post("id_file"));
				}
			}else{
				$data['file_name'] = $this->M_kurir->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
			}

			$data = array(
				"name_kurir" => post("name_kurir"),
				"api_kurir" => post("api_kurir"),
				"status_kurir" => post("status_kurir")
			);
			
			$edit = $this->M_kurir->update_table("a__kurir",$data,"id_kurir",post("id_kurir"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => TRUE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/kurir'
				);
				print json_encode($json_data);
			}
		}
	}

	
    public function get_data_ongkir(){
        $param = post('param');
        $get = $this->rajaongkir->get_kurir_data($param);
        echo $get;
    }


    
}
?>