<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class withdraw extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_withdraw');
    }
    
    public function index()
    {

                if(\strpos(get_role()->role, "6")){
            $data['content'] = 'withdraw';
            $data['page_active'] = 'withdraw';
            $this->load->view('template', $data); 
        }else{
            echo "Tidak Ada Akses";         
        }   
        
    }


    function ajax_action_datatable_withdraw(){
        $column = "a.*,b.name_merchant";
        $table = "m__withdraw a";
        $column_order = array('jumlah_withdraw','name_merchant');
        $column_search = array('jumlah_withdraw','name_merchant');
        $order = array('id_withdraw' => 'DESC'); 
        $where = '';
          $joins = array(
            array(
                'table' => 'mr__merchant b',
                'condition' => 'a.id_merchant = b.id_merchant',
                'jointype' => ''
            ),
        );
        $list = $this->M_withdraw->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $link = ''.base_url().$this->config->item('index_page').'/withdraw';
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->name_merchant;
            $row[] = $key->created_at;
            $row[] = addNumber($key->jumlah_withdraw);
            if($key->status_withdraw == 1){
               $row[] = "<badge class='badge badge-warning'>Segera  Proses</badge>";
            }else if($key->status_withdraw == 2){
               $row[] = "<badge class='badge badge-danger'>Penarikan  di tangguhkan</badge>";
            }else if($key->status_withdraw == 0){
               $row[] = "<badge class='badge badge-success'>Penarikan Berhasil</badge>";
            }

            if($key->update_at == ''){
               $row[] = "<badge class='badge badge-warning'>Segera  proses</badge>";
            }else{
               $row[] = "<badge class='badge badge-success'>Di Kirim tanggal ".$key->update_at."</badge>";
            }
             if($key->status_withdraw == 1){
               $row[] ='<button class="btn btn-warning btn-sm" onclick="modal_wd(\''.$key->id_withdraw.'\')">Cek Penarikan</button>';
            }else if($key->status_withdraw == 0){
               $row[] = '<button class="btn btn-success btn-sm" onclick="modal_wd(\''.$key->id_withdraw.'\')">Detail Penarikan</button>';
            }

            $data[] = $row;
        }
    
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_withdraw->count_all($table,$where,$joins),
            "recordsFiltered" => $this->M_withdraw->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

    public function ajax_action_get_detail_withdraw(){
        $id = $this->uri->segment(3);
        $joins ='';
        $data = $this->M_withdraw->fetch_joins('m__withdraw a', '*', $joins,'id_withdraw = '."'".$id."'", '',TRUE);

        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

     public function upload_wd(){
        $this->form_validation->set_rules('id_withdraw', 'id_withdraw', 'required');
        $this->form_validation->set_rules('id_merchant', 'id_merchant', 'required');
        $this->form_validation->set_rules('nama_bank', 'nama_bank', 'required');
        $this->form_validation->set_rules('no_rek', 'no_rek', 'required');
        $this->form_validation->set_rules('nama_rek', 'nama_rek', 'required');
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{

             if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
            /*-------------setting attachment upload -------------*/
            $config['upload_path'] = './uploads/withdraw/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')){
                $data['file_name'] = null;
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $data = $this->upload->data();
            }
        }else{
            $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload Bukti Pembayaran "),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
            die();
        }

         $data_file = array(
                "nama_file" => $data['file_name'],
                "id_relation" => post('id_withdraw'),
                "table_relation" => "m__withdraw",
                "created_at" => date("Y-m-d H:i:s")
            );
        $add_file = $this->M_withdraw->insert_table("a__file",$data_file);

        if($add_file){
              $last_data= $this->M_withdraw->get_last_data('id_file', 'a__file','DESC');
              $data = array(
                "status_withdraw" => 0,
                "nama_bank" => post('nama_bank'),
                "no_rek" => post('no_rek'),
                "nama_rek" => post('nama_rek'),
                "id_merchant"=>post('id_merchant'),
                "id_file" =>$last_data->id_file,
                "update_at" => date("Y-m-d H:i:s")
            );
            $edit = $this->M_withdraw->update_table("m__withdraw",$data,"id_withdraw",post("id_withdraw"));
            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'withdraw'
                );
                print json_encode($json_data);
            }
        }else{
            $json_data =  array(
                    "result" => FALSE,
                    "message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'withdraw'
                );
                print json_encode($json_data);
              }
        }
    }
  
}

?>