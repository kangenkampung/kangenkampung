<script type="text/javascript">
  $(function() {
      var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_wd","<?php echo base_url().$this->config->item('index_page'); ?>/withdraw/ajax_action_datatable_withdraw",form_data);
  });
</script>

<script type="text/javascript">
	function modal_wd(id) {
       var trHTML = '';
    $('#table_cart').empty();
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>withdraw/ajax_action_get_detail_withdraw/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        $('#wd_id_withdraw').val(response.data[0].id_withdraw);
        $('#wd_id_merchant').val(response.data[0].id_merchant);
    });
      $('#modal_withdraw').modal('show');
    }
</script>

<script>
  var loadwd = function(event) {
    var output = document.getElementById('preview_wd');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>

<script type="text/javascript">
    function ajax_action_update_widthdraw(){
          var file_data = $('#attachment_wd').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('id_merchant', $('#wd_id_merchant').val());
          form_data.append('id_withdraw', $('#wd_id_withdraw').val());
          form_data.append('no_rek', $('#no_rek_wd').val());
          form_data.append('nama_rek', $('#nama_rek_wd').val());
          form_data.append('nama_bank', $('#bank_rek_wd').val());
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>withdraw/upload_wd/","POST",form_data);
        }
</script>

