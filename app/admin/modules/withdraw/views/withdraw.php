<div class="main-content-wrap sidenav-open d-flex flex-column">
    <div class="breadcrumb">
        <ul>
            <li><a href="">Penarikan Saldo</a></li>
            <li>Proses pengiriman dilakukan saat hari rabu</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>

     <div class="row mb-4">

        <div class="col-md-12 mb-3">
            <div class="card text-left">
                <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="card-title mb-3">Daftar Penarikan Saldo</h4> 
                    </div>
                   
                </div>
                                   <div style="margin-top: 3%"></div>
                    <div class="table-responsive">
                        <table class="table" id="list_wd">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                     <th scope="col">Nama Merchant</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Di Kirim Pada</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        </div>
</div>

<?= $this->load->view('js/js')?>
   
        
  