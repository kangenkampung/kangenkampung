<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_category","<?php echo base_url().$this->config->item('index_page'); ?>category/ajax_list_category",form_data);
    });

</script>

<script type="text/javascript">
  // Add Category

    function ajax_action_add_category(){
      var file_data = $('#attachment').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('name_category', $('#name_category').val());
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/category/ajax_action_add_category/","POST",form_data);
        }

     function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>category/ajax_action_get_category/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var email =$('#edit_name_category').val(response.data[0].name_category);
              var id_file =$('#edit_id_file').val(response.data[0].id_file);
              var id_category =$('#edit_id_category').val(response.data[0].id_category);
              $("#edit_category_preview").attr("src","<?= base_url().'uploads/category/';?>"+response.data[0].nama_file);
              $("#status_category option[value='"+response.data[0].status_category+"']").attr('selected','selected');
             $('#page-load').hide();
          });
         $('#edit-category').modal("show");
       
        }

         function ajax_action_edit_category(){
           var file_data = $('#edit_attachment').prop('files')[0];
          var form_data = new FormData();
          var name =$('#edit_name_category').val();
          var id_category =$('#edit_id_category').val();
          var id_file =$('#edit_id_file').val();
          var status_category = $('#status_category :selected').val();
          form_data.append('status_category',status_category);
          form_data.append('id_file', id_file);
          form_data.append('id_category', id_category);
          form_data.append('name_category', name);
           form_data.append('file', file_data);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>category/ajax_action_edit_category/","POST",form_data);
            }

</script>