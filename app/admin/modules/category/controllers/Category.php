<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct() {
		$this->load->model('M_category');
		$this->load->library('recaptcha');
	}

		public function index(){
			if(\strpos(get_role()->role, "7")){
			$data['content'] = 'category';
			$this->load->view('template',$data);	
		}else{
			echo "Tidak Ada Akses";			
		}	
		
	}

	public function ajax_action_add_category(){
		$this->form_validation->set_rules('name_category', 'name_category', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

			 if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			/*-------------setting attachment upload -------------*/
			$config['upload_path'] = './uploads/category/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')){
				$data['file_name'] = null;
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$data = $this->upload->data();
			}
		}else{
			$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload icon "),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
			die();
        }

       	 $data_file = array(
				"nama_file" => $data['file_name'],
				"created_at" => date("Y-m-d H:i:s")
			);
		$add_file = $this->M_category->insert_table("a__file",$data_file);

		if($add_file){
			  $last_data= $this->M_category->get_last_data('id_file', 'a__file','DESC');
			  $data = array(
				"name_category" => post("name_category"),
				"status_category" => 1,
				"id_file" => $last_data->id_file,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_category->insert_table("a__category",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/category'
				);
				print json_encode($json_data);
			}
		}else{
			$json_data =  array(
					"result" => FALSE,
					"message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/category'
				);
				print json_encode($json_data);
			  }
		}
	}

	public function ajax_list_category(){
		$column = "a.*,b.*";
		$column_order = array('name_category');
		$column_search = array('name_category');
		$order = array('id_category' => 'DESC');
		$table = "a__category a";
		$where = "a.status_category != 3";
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$list = $this->M_category->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		$data = array();
		$link = ''.base_url().$this->config->item('index_page').'/category';
		$no = $_POST['start'];
		foreach ($list as $key) {
			if($key->status_category == 1){
				$status = "Active";
			}else{
				$status = "Hidden";
			}
			$row = array();		
			$row[] = $no+1;
			$row[] = $key->name_category;
			$row[] = $key->nama_file;			
			$row[] = $status;	
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete('."'".$link."'".','."'ajax_action_delete_category'".','.$key->id_category.')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal('.$key->id_category.')" value="Edit">';	
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_category->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_category->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_action_delete_category(){
		$id = $this->uri->segment(3);
		$data = array(
					"status_category" => 3
				);
		$edit = $this->M_category->update_table('a__category', $data, 'id_category', $id);
		$this->db->trans_complete();
		if($edit==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/category'
			);
			print json_encode($json_data);
		}
	}

	  function ajax_action_get_category(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$data = $this->M_category->fetch_joins('a__category a', '*', $joins,'id_category = '. $id, '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_get_all_category(){
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$data = $this->M_category->fetch_joins('a__category a', '*', $joins,'', '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_category(){
		$this->form_validation->set_rules('name_category', 'name_category', 'required');
		$this->form_validation->set_rules('status_category', 'status_category', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				/*-------------setting attachment upload -------------*/
				$config['upload_path'] = './uploads/category/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 1024 * 8;
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file')){
					$data['file_name'] = null;
					$json_data =  array(
						"result" => FALSE ,
						"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
						"form_error" => 'file proof',
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				}else{
					$data['file_name_unlink'] = $this->M_category->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
					unlink('./uploads/category/' .  $data['file_name_unlink']);  
					$data = $this->upload->data();

					$data_foto = array(
						"nama_file" => $data['file_name']
					);

					$edit_foto = $this->M_category->update_table("a__file",$data_foto,"id_file",post("id_file"));
				}
			}else{
				$data['file_name'] = $this->M_category->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
			}

			$data = array(
				"name_category" => post("name_category"),
				"status_category" => post("status_category")
			);
			
			$edit = $this->M_category->update_table("a__category",$data,"id_category",post("id_category"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => TRUE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/category'
				);
				print json_encode($json_data);
			}
		}
	}


    
}
?>