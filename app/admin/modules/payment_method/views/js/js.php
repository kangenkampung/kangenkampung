<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_payment_method","<?php echo base_url().$this->config->item('index_page'); ?>payment_method/ajax_list_payment_method",form_data);
    });

</script>

<script type="text/javascript">
  // Add Category
  function  add_desc_payment(){
     $("textarea#desc_payment").val(document.getElementById("catatan_method").childNodes[0].innerHTML);
  }

   function  edit_desc_payment(){
     $("textarea#edit_desc_payment").val(document.getElementById("edit_catatan_method").childNodes[0].innerHTML);
  }

    function ajax_action_add_payment_method(){
      var catatan_method = $('textarea#desc_payment').val();
      var file_data = $('#payment_method_attachment').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('name_payment_method', $('#name_payment_method').val());
          form_data.append('api_payment_method', $('#api_payment_method').val());
          form_data.append('code_payment_method', $('#code_payment_method').val());
          form_data.append('catatan_method', catatan_method);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_add_payment/","POST",form_data);
        }

     function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>payment_method/ajax_action_get_payment_method/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var name_payment_method =$('#edit_name_payment_method').val(response.data[0].name_payment_method);
              var code_payment_method =$('#edit_code_payment_method').val(response.data[0].code_payment_method);
              var api_payment_method =$('#edit_api_payment_method').val(response.data[0].api_payment_method);
              var id_payment_method =$('#edit_id_payment_method').val(response.data[0].id_payment_method);
              var id_file_payment_method =$('#edit_id_file_payment_method').val(response.data[0].id_file);
              var edit_catatan =$('#edit_desc_payment').val(response.data[0].catatan_method);
              // $('#edit_catatan_method').html(response.data[0].catatan_method);

              var sHTML = response.data[0].catatan_method.trim();
              quill.clipboard.dangerouslyPasteHTML(0, sHTML);


              $("#edit_payment_method_preview").attr("src","<?= base_url().'uploads/payment_method/';?>"+response.data[0].nama_file);
              $("#edit_status_payment_method option[value='"+response.data[0].status_payment_method+"']").attr('selected','selected');
             $('#page-load').hide();
          });
         $('#edit-payment-method').modal("show");
       
        }

         function ajax_action_edit_payment_method(){
          var catatan_method = $('textarea#edit_desc_payment').val();
          var file_data = $('#edit_attachment_payment_method').prop('files')[0];
          var form_data = new FormData();
          var name_payment_method =$('#edit_name_payment_method').val();
          var api_payment_method =$('#edit_api_payment_method').val();
          var code_payment_method =$('#edit_code_payment_method').val();
          var id_payment_method =$('#edit_id_payment_method').val();
          var id_file =$('#edit_id_file_payment_method').val();
          var status_payment = $('#edit_status_payment_method :selected').val();
          form_data.append('status_payment_method',status_payment);
          form_data.append('id_file', id_file);
          form_data.append('id_payment_method', id_payment_method);
          form_data.append('name_payment_method', name_payment_method);
          form_data.append('code_payment_method', code_payment_method);
          form_data.append('api_payment_method', api_payment_method);
           form_data.append('catatan_method', catatan_method);
           form_data.append('file', file_data);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>payment_method/ajax_action_edit_payment_method/","POST",form_data);
            }

</script>