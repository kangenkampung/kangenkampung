<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_method extends MY_Controller {

	public function __construct() {
		$this->load->model('M_paymentmethod');
		$this->load->library('recaptcha');
	}

		public function index(){
				if(\strpos(get_role()->role, "8")){
            $data['content'] = 'payment_method';
            $data['page_active'] = 'payment_method';
            $this->load->view('template', $data); 
        }else{
            echo "Tidak Ada Akses";         
        }   	
		
	}

	public function ajax_action_add_payment(){
		$this->form_validation->set_rules('name_payment_method', 'name_payment_method', 'required');
		// $this->form_validation->set_rules('api_payment_method', 'api_payment_method', 'required');
		$this->form_validation->set_rules('code_payment_method', 'code_payment_method', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

			 if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			/*-------------setting attachment upload -------------*/
			$config['upload_path'] = './uploads/payment_method/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')){
				$data['file_name'] = null;
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$data = $this->upload->data();
			}
		}else{
			$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload icon "),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
			die();
        }

       	 $data_file = array(
				"nama_file" => $data['file_name'],
				"created_at" => date("Y-m-d H:i:s")
			);
		$add_file = $this->M_paymentmethod->insert_table("a__file",$data_file);

		if($add_file){
			  $last_data= $this->M_paymentmethod->get_last_data('id_file', 'a__file','DESC');
			  $data = array(
				"name_payment_method" => post("name_payment_method"),
				"api_payment_method" => post("api_payment_method"),
				"code_payment_method" => post("code_payment_method"),
				"catatan_method" => post("catatan_method"),
				"status_payment_method" => 1,
				"id_file" => $last_data->id_file,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_paymentmethod->insert_table("a__payment_method",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/payment_method'
				);
				print json_encode($json_data);
			}
		}else{
			$json_data =  array(
					"result" => FALSE,
					"message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/payment_method'
				);
				print json_encode($json_data);
			  }
		}
	}

	public function ajax_list_payment_method(){
		$column = "a.*,b.*";
		$column_order = array('name_payment_method','code_payment_method','api_payment_method');
		$column_search = array('name_payment_method','code_payment_method','api_payment_method');
		$order = array('id_payment_method' => 'DESC');
		$table = "a__payment_method a";
		$where = "a.status_payment_method != 3";
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$list = $this->M_paymentmethod->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		$data = array();
		$link = ''.base_url().$this->config->item('index_page').'/payment_method';
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if($key->status_payment_method == 1){
				$status = "Active";
			}else{
				$status = "Hidden";
			}
			$row = array();		
			$row[] = $no;
			$row[] = $key->name_payment_method;
			$row[] = $key->code_payment_method;			
			$row[] = $key->api_payment_method;
			$row[] = $status;	
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete('."'".$link."'".','."'ajax_action_delete_payment_method'".','.$key->id_payment_method.')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal('.$key->id_payment_method.')" value="Edit">';	
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_paymentmethod->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_paymentmethod->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	 function ajax_action_get_payment_method(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$data = $this->M_paymentmethod->fetch_joins('a__payment_method a', '*', $joins,'id_payment_method = '. $id, '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	 function ajax_action_get_payment_method_all(){
		$data = $this->M_paymentmethod->fetch_joins('a__payment_method a', '*','','', '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_payment_method(){
		$id = $this->uri->segment(3);
		$data = array(
					"status_payment_method" => 3
				);
		$edit = $this->M_paymentmethod->update_table('a__payment_method', $data, 'id_payment_method', $id);
		$this->db->trans_complete();
		if($edit==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'payment_method'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_payment_method(){
		$this->form_validation->set_rules('name_payment_method', 'name_payment_method', 'required');
		$this->form_validation->set_rules('api_payment_method', 'api_payment_method', 'required');
		$this->form_validation->set_rules('code_payment_method', 'code_payment_method', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				/*-------------setting attachment upload -------------*/
				$config['upload_path'] = './uploads/payment_method/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 1024 * 8;
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file')){
					$data['file_name'] = null;
					$json_data =  array(
						"result" => FALSE ,
						"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
						"form_error" => 'file proof',
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				}else{
					$data['file_name_unlink'] = $this->M_paymentmethod->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
					unlink('./uploads/payment_method/' .  $data['file_name_unlink']);  
					$data = $this->upload->data();

					$data_foto = array(
						"nama_file" => $data['file_name']
					);

					$edit_foto = $this->M_paymentmethod->update_table("a__file",$data_foto,"id_file",post("id_file"));
				}
			}else{
				$data['file_name'] = $this->M_paymentmethod->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
			}

			$data = array(
				"name_payment_method" => post("name_payment_method"),
				"api_payment_method" => post("api_payment_method"),
				"code_payment_method" => post("code_payment_method"),
				"status_payment_method" => post("status_payment_method"),
				"catatan_method" => post("catatan_method"),
				"update_at" => date("Y-m-d H:i:s")
			);
			
			$edit = $this->M_paymentmethod->update_table("a__payment_method",$data,"id_payment_method",post("id_payment_method"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => TRUE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'payment_method'
				);
				print json_encode($json_data);
			}
		}
	}

    
}
?>