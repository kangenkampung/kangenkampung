<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller {

	public function __construct() {
		$this->load->model('M_member');
		$this->load->library('recaptcha');
	}

		public function index(){
			if(\strpos(get_role()->role, "2")){
			$data['content'] = 'member';
			$this->load->view('template',$data);	
		}else{
			echo "Tidak Ada Akses";			
		}		
		
	}

	function ajax_action_datatable_member(){
		$column = "*";
		$table = "m__member";
		$column_order = array('id_member', 'name_member', 'password_member', 'no_member','email_member','created_at');
		$column_search = array('id_member', 'name_member', 'password_member', 'no_member','email_member','created_at');
		$order = array('id_member' => 'DESC'); 
		$where = '';
		$joins = '';
		$list = $this->M_member->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		
		$link = ''.base_url().$this->config->item('index_page').'/member';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if($key->status_member == 1){
				$status = "checked";
			}else{
				$status = "";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_member;
			$row[] = $key->no_member;
			$row[] = $key->email_member;
			$row[] = '<input type="checkbox" '.$status.' onclick="ajax_action_edit_active_member('.'\''.$key->id_member.'\''.','.$key->status_member.')"  />';
			$data[] = $row;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_member->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_member->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	function ajax_action_active_member(){
		    $id_member= $this->input->post('id');
			$input_status= $this->input->post('status');

			if($input_status == 1){
				$status = 0;
			}else{
				$status = 1;
			}

			$data = array(
				"status_member" => $status
			);

			$edit = $this->M_member->update_table("m__member",$data,"id_member",$id_member);

			if($edit==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'member'
				);
				print json_encode($json_data);
			}
	}
    
}
?>