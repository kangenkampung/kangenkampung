<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends MY_Controller {

	public function __construct() {
		$this->load->model('M_Pemesanan');
		$this->load->library('recaptcha');
	}

		public function index(){
				if(\strpos(get_role()->role, "4")){
            $data['content'] = 'Pemesanan';
            $data['page_active'] = 'Pemesanan';
            $this->load->view('template', $data); 
        }else{
            echo "Tidak Ada Akses";         
        }   	
		
	}

	function ajax_action_datatable_pemesanan(){
		$column = "*";
		$table = "m__order a";
		$column_order = array('id_order', 'name_member', 'status_order');
		$column_search = array('id_order', 'name_member', 'status_order');
		$order = array('id_order' => 'DESC'); 
		$where = '';
		$joins = array(
			array(
				'table' => 'm__member b',
				'condition' => 'a.id_member = b.id_member',
				'jointype' => ''
			),
		);
		$list = $this->M_Pemesanan->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		
		$link = ''.base_url().$this->config->item('index_page').'/order';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;	
			if($key->status_order == 1){
				$status = "Belum Checkout";
				$cls  = "btn btn-warning btn-sm";
			}else if($key->status_order == 2){
				$status = "Admin Cek Pembayaran";
				$cls  = "btn btn-info btn-sm";
			}else if($key->status_order == 3){
				$status = "Pesanan di teruskan ke merchant";
				$cls  = "btn btn-success btn-sm";
			}else if($key->status_order == 4){
				$status = "Pesanan di Batalkan";
				$cls  = "btn btn-danger btn-sm";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->created_at;
			$row[] = $key->id_order;
			$row[] = $key->name_member;
			$row[] = addNumber($key->total_order,true);
			$row[] = '<input type="button" class="'.$cls.'" value="'.$status.'" >';
			if($key->status_order != 1){
				$row[] = '<input type="button" class="'.$cls.'" value="'."Ubah Status".'" onclick="modal_status(\''.$key->id_order.'\')">';
			}else{
				$row[] = '---';
			}
			
			$data[] = $row;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_Pemesanan->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_Pemesanan->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		
		echo json_encode($output);
	}



	  function ajax_action_get_detail_pemesanan(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file d',
				'condition' => 'a.id_file = d.id_file',
				'jointype' => ''
			),
			array(
				'table' => 'a__payment_account e',
				'condition' => 'a.id_payment_account = e.id_payment_account',
				'jointype' => ''
			),
			array(
				'table' => 'a__payment_method f',
				'condition' => 'e.id_payment_method = f.id_payment_method',
				'jointype' => ''
			),
		);
		$data = $this->M_Pemesanan->fetch_joins('m__order a', '*', $joins,'a.id_order ='."'". $id."'", '',TRUE);
		$joins_cart = array(
			array(
				'table' => 'mr__product b',
				'condition' => 'a.id_produk = b.id_product',
				'jointype' => ''
			),
			array(
				'table' => 'mr__merchant c',
				'condition' => 'a.id_merchant = c.id_merchant',
				'jointype' => ''
			),
		);


		$order_cart = $this->M_Pemesanan->fetch_joins('m__cart a', '*', $joins_cart,'a.id_order ='."'". $id."'"."GROUP BY id_product", '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data,
				'cart' => $order_cart
			);
			print json_encode($json_data);
		}
	}
	function ajax_transfer_to_merchant(){
		$id = $this->uri->segment(3);
		$joins_cart = array(
			array(
				'table' => 'mr__product b',
				'condition' => 'a.id_produk = b.id_product',
				'jointype' => ''
			),
			array(
				'table' => 'mr__merchant c',
				'condition' => 'a.id_merchant = c.id_merchant',
				'jointype' => ''
			),
			array(
				'table' => 'm__member d',
				'condition' => 'a.id_member = d.id_member',
				'jointype' => ''
			)
		);


		$order_cart = $this->M_Pemesanan->fetch_joins('m__cart a', '*', $joins_cart,'a.id_order ='."'". $id."'"." AND status_cart= '0' GROUP BY id_product", '',TRUE);

		foreach ($order_cart as $key ) {

		$count_data_detail = $this->M_Pemesanan->get_count_data('a__mutasi');
		$id_mutasi = date('his')."MTS".$count_data_detail;
			// echo $key->id_order;
			      $data = array(
			      	    "id_mutasi" => $id_mutasi,
						"id_order" => $key->id_order,
						"id_merchant" => $key->id_merchant,
						"total" => $key->subtotal,
						"kurir_price" => $key->kurir_price,
						"status_mutasi" => 1,
						"created_at" =>date("Y-m-d H:i:s"),
						"id_cart" => $key->id_cart,
						"desc_mutasi"=>"Pembelian ". $key->name_product." oleh ".$key->name_member
					);
					$add = $this->M_Pemesanan->insert_table("a__mutasi",$data);
		}

		if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Data Telah di transfer'),
					"form_error" => $error,
					// "redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengirim dana ke merchant'),
					"form_error" => '',
					// "redirect" => ''
				);
				print json_encode($json_data);
			}

	}


	function ajax_action_change_status(){
		$this->form_validation->set_rules('id_order', 'id_order', 'required');
		$this->form_validation->set_rules('status_order', 'status_order', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			if(post("status_order") == 3){
					$data_cart = array(
					"status_cart" => 4,
					"update_at" => date("Y-m-d H:i:s")
				);
				$edit_cart = $this->M_Pemesanan->update_table2("m__cart",$data_cart,"id_order = "."'".post("id_order")."'"." AND status_cart = 2");
			}


			$data = array(
				"status_order" => post("status_order"),
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_Pemesanan->update_table("m__order",$data,"id_order",post("id_order"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Merubah  Status'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Merubah Status'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'pemesanan'
				);
				print json_encode($json_data);
			}

		}
	}


	


    
}
?>