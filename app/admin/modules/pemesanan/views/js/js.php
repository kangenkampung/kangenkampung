<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_pemesanan","<?php echo base_url().$this->config->item('index_page'); ?>pemesanan/ajax_action_datatable_pemesanan",form_data);
    });

</script>
<script type="text/javascript">
	function modal_status(id) {
    var trHTML = '';
    $('#table_cart').empty();
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>pemesanan/ajax_action_get_detail_pemesanan/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        $("#pemesanan_preview").attr("src", "<?= base_url().'uploads/bukti_pembayaran/';?>" + response.data[0].nama_file);
        $('#pemesanan_price').val(addNumber(response.data[0].total_order,true));
        $('#id_order').val(id);
        if(response.data[0].status_order == 2){
            $('#status_text').text("Admin Cek Pembayaran");
            $('#teruskan').show();
        }else if(response.data[0].status_order == 3){
          $('#status_text').text("Telah di terus kan ke mechant");
          $('#teruskan').hide();
        }else if(response.data[0].status_order == 4){
          $('#status_text').text("Pesanan Telah Di Batalkan");
          $('#teruskan').hide();
        }

          var transfer = response.data[0].total_order;
          var str = transfer.toString();
          var kodeunik = str.slice(-3);
          $('#kode_unik').val(kodeunik);

        
        $('#pemesanan_bank').val(response.data[0].name_payment_method);
        $("#status_kurir option[value='" + response.data[0].status_kurir + "']").attr('selected', 'selected');
        $('#page-load').hide();

        for (var i = 0; i < response.cart.length; i++) { 
          if(response.cart[i].status_cart == 3){
              var text = "Proses Pengiriman";
               $('#status_text').text(text);
               $('#pesanan_slesai').hide();
          }else if(response.cart[i].status_cart == 0){
              var text = "Telah Sampai ke customer";
               $('#status_text').text(text);
               $('#pesanan_slesai').show();
          }else{
               var text = "Menunggu Merchant";
               $('#pesanan_slesai').hide();
          }
         var table = '<tr><td>' + response.cart[i].id_cart +
                  '</td><td>' + response.cart[i].name_product +
                  '</td><td>' + response.cart[i].name_merchant + 
                  '</td><td>' + addNumber(response.cart[i].price,true) +  
                  '</td><td>' +response.cart[i].name_kurir+
                  '</td><td>' +text+'</td></tr>'; 

    
        trHTML += table;
          }
          $('#table_cart').append(trHTML);

    });
    
    $('#modal_status').modal('show');


}
</script>

<script type="text/javascript">
  function transfer_to_merchant(){
          var id=  $('#id_order').val();
          var form_data = new FormData();
          form_data.append('id_order', $('#id_order').val());
          form_data.append('status_order', id);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>pemesanan/ajax_transfer_to_merchant/" + id,"POST",form_data);
  }
</script>

<script type="text/javascript">
  function change_status(id){
          var form_data = new FormData();
          form_data.append('id_order', $('#id_order').val());
          form_data.append('status_order', id);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>pemesanan/ajax_action_change_status/","POST",form_data);

  }
</script>
