<script type="text/javascript">
    $(document).ready(function() {
      set_level();
    	  var form_data = new FormData();
    	  form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_admin","<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_action_datatable_admin",form_data);
        } );

 function ajax_action_add_admin(){
        	var form_data = new FormData();
			form_data.append('email_admin', $('#email_admin').val());
			form_data.append('name_admin', $('#name_admin').val());
			form_data.append('ktp_admin', $('#name_admin').val());
			form_data.append('password_admin', $('#password_admin').val());
			form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
			addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_action_add_admin/","POST",form_data);
        }
</script>

<script type="text/javascript">
	// Ajax Edit Admin-----------------------------------------------------
   function ajax_action_edit_admin(){
          var form_data = new FormData();
          var email =$('#edit_email_admin').val();
          var name =$('#edit_name_admin').val();
          var password =$('#edit_password_admin').val();
          var ktp = $('#edit_ktp_admin').val();
          var id = $('#edit_id_admin').val();
          form_data.append('email_admin',email);
          form_data.append('name_admin', name);
          form_data.append('ktp_admin', name);
          form_data.append('password_admin', password);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>admin/ajax_action_edit/"+id,"POST",form_data);
            }



        function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>admin/ajax_action_get_admin/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var email =$('#edit_email_admin').val(response.data[0].email_admin);
              var name =$('#edit_name_admin').val(response.data[0].name_admin);
              var username = $('#edit_ktp_admin').val(response.data[0].ktp_admin);
              var username = $('#edit_id_admin').val(response.data[0].id_admin);
              selected_set_kota(response.data[0].id_level);
             $('#page-load').hide();
          });
         $('#edit-admin').modal("show");
          
        }
</script>

<script type="text/javascript">
    function set_level(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>/level/ajax_action_get_all_level";
        $('#page-load').show();
         var form_data = new FormData();
         form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.data.length; i++){
          html += "<option value='"+response.data[i].id_level+"'>"+response.data[i].name_level+"</option>";
        }
        $('#add_admin_akses').html(html);
        $('#page-load').hide();
        });
  }
</script>


<script type="text/javascript">
    function selected_set_kota(id){
        var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>/level/ajax_action_get_all_level";
        $('#page-load').show();
         var form_data = new FormData();
         form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.data.length; i++){
          html += "<option value='"+response.data[i].id_level+"'>"+response.data[i].name_level+"</option>";
        }
        $('#edit_admin_akses').html(html);
        $("#edit_admin_akses option[value="+id+"]").attr('selected', 'selected');
        $('#page-load').hide();
        });
  }
</script>