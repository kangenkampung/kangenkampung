<!--  -->
<script type = "text/javascript" >
    function ajax_action_add_merchant() {
        var form_data = new FormData();
        form_data.append('name_merchant', $('#name_merchant').val());
        form_data.append('no_merchant', $('#no_merchant').val());
        form_data.append('email_merchant', $('#email_merchant').val());
        form_data.append('owner_merchant', $('#owner_merchant').val());
        form_data.append('status_merchant', $('#status_merchant').val());
        form_data.append('username_merchant', $('#username_merchant').val());
        form_data.append('password_merchant', $('#password_merchant').val());
        form_data.append('alamat_lengkap_merchant', $('#alamat_lengkap_merchant').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/merchant/ajax_action_add_merchant/", "POST", form_data);
    }

function ajax_action_edit_merchant() {
    var form_data = new FormData();
    form_data.append('name_merchant', $('#edit_name_merchant').val());
    form_data.append('no_merchant', $('#edit_no_merchant').val());
    form_data.append('email_merchant', $('#edit_email_merchant').val());
    form_data.append('owner_merchant', $('#edit_owner_merchant').val());
    form_data.append('status_merchant', $('#edit_status_merchant').val());
    form_data.append('username_merchant', $('#edit_username_merchant').val());
    form_data.append('id_merchant', $('#edit_id_merchant').val());
    form_data.append('alamat_lengkap_merchant', $('#edit_alamat_lengkap_merchant').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/merchant/ajax_action_edit_merchant/", "POST", form_data);
} 
</script>

<!--call -->
    <script type = "text/javascript" >
    function ajax_show_edit_modal(id) {
        var link = "<?php echo base_url().$this->config->item('index_page'); ?>merchant/ajax_action_merchant/" + id;
         var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

        ajaxShowData(link, "POST", form_data, function(response) {
            $('#edit_name_merchant').val(response.data[0].name_merchant);
            $('#edit_email_merchant').val(response.data[0].email_merchant);
            $('#edit_owner_merchant').val(response.data[0].owner_merchant);
            $('#edit_status_merchant').val(response.data[0].status_merchant);
            $('#edit_username_merchant').val(response.data[0].username_merchant);
             $('#edit_no_merchant').val(response.data[0].no_merchant);
            // $('#password_merchant').val(response.data[0].name_merchant);
            $('#edit_provinsi_merchant').val(response.data[0].provinsi_merchant);
            $('#edit_kota_merchant').val(response.data[0].kota_merchant);
            $('#edit_kecamatan_merchant').val(response.data[0].kecamatan_merchant);
            $('#edit_kelurahan_merchant').val(response.data[0].kelurahan_merchant);
            $('#edit_kode_pos_merchant').val(response.data[0].kode_pos_merchant);
            $('#edit_alamat_lengkap_merchant').val(response.data[0].alamat_lengkap_merchant);
            $('#edit_id_merchant').val(response.data[0].id_merchant);
            $('#page-load').hide();
        });
        $('#edit-merchant').modal("show");

    } 
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_merchant","<?php echo base_url().$this->config->item('index_page'); ?>merchant/ajax_list_merchant",form_data);
    });

</script>

<script type="text/javascript">
  
  function set_provinci(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_data_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "province");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].province_id+"'>"+response.rajaongkir.results[i].province+"</option>";
        }
        $('#provinci').html(html);
        $('#page-load').hide();
        });
  }


    $('#provinci').on('change', function() {
           set_kota_detail(this.value);
           $('#page-load').hide();
    });

   function set_kota_detail(value){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_data_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "city?province="+value);
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#kota').html(html);
        $('#page-load').hide();
        });
  }

  </script>