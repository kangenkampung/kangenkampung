<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Controller {

	public function __construct() {
		$this->load->model('M_Banner');
		$this->load->library('recaptcha');
	}

		public function index(){
		if(\strpos(get_role()->role, "9")){
			$data['content'] = 'banner';
			$this->load->view('template',$data);	
		}else{
			echo "Tidak Ada Akses";			
		}		
		
	}

	public function ajax_action_add_banner(){
		$this->form_validation->set_rules('name_banner', 'name_banner', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

			 if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			/*-------------setting attachment upload -------------*/
			$config['upload_path'] = './uploads/banner/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')){
				$data['file_name'] = null;
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$data = $this->upload->data();
			}
		}else{
			$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload icon "),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
			die();
        }

       	 $data_file = array(
				"nama_file" => $data['file_name'],
				"created_at" => date("Y-m-d H:i:s")
			);
		$add_file = $this->M_Banner->insert_table("a__file",$data_file);

		if($add_file){
			  $last_data= $this->M_Banner->get_last_data('id_file', 'a__file','DESC');
			  $data = array(
				"name_banner" => post("name_banner"),
				"id_file" => $last_data->id_file,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_Banner->insert_table("a__banner",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/banner'
				);
				print json_encode($json_data);
			}
		}else{
			$json_data =  array(
					"result" => FALSE,
					"message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/banner'
				);
				print json_encode($json_data);
			  }
		}
	}

	public function ajax_list_banner(){
		$column = "a.*,b.*";
		$column_order = array('name_banner','name_banner');
		$column_search = array('name_banner','name_banner');
		$order = array('id_banner' => 'DESC');
		$table = "a__banner a";
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$list = $this->M_Banner->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		$data = array();
		$link = ''.base_url().$this->config->item('index_page').'/banner';
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			$row = array();		
			$row[] = $no;
			$row[] = $key->name_banner;	
			$row[] = "";	
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete('."'".$link."'".','."'ajax_action_delete_banner'".','.$key->id_banner.')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal('.$key->id_banner.')" value="Edit">';	
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_Banner->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_Banner->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_action_delete_banner(){
		$id = $this->uri->segment(3);
		$delete = $this->M_Banner->delete_table("a__banner","id_banner",$id);
		if($delete==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/banner'
			);
			print json_encode($json_data);
		}
	}

	  function ajax_action_get_banner(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file b',
				'condition' => 'a.id_file = b.id_file',
				'jointype' => ''
			),
		);
		$data = $this->M_Banner	->fetch_joins('a__banner a', '*', $joins,'id_banner = '. $id, '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_banner(){	
		$this->form_validation->set_rules('name_banner', 'name_banner', 'required');
		$this->form_validation->set_rules('id_file', 'id_file', 'required');
		$this->form_validation->set_rules('id_banner', 'id_banner', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				/*-------------setting attachment upload -------------*/
				$config['upload_path'] = './uploads/banner/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = 1024 * 8;
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file')){
					$data['file_name'] = null;
					$json_data =  array(
						"result" => FALSE ,
						"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
						"form_error" => 'file proof',
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				}else{
					$data['file_name_unlink'] = $this->M_Banner->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
					unlink('./uploads/banner/' .  $data['file_name_unlink']);  
					$data = $this->upload->data();

					$data_foto = array(
						"nama_file" => $data['file_name']
					);

					$edit_foto = $this->M_Banner->update_table("a__file",$data_foto,"id_file",post("id_file"));
				}
			}else{
				$data['file_name'] = $this->M_Banner->get_row("nama_file","a__file","id_file='".post("id_file")."'","","",FALSE)->nama_file;
			}

			$data = array(
				"name_banner" => post("name_banner")
			);
			
			$edit = $this->M_Banner->update_table("a__banner",$data,"id_banner",post("id_banner"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => TRUE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/banner'
				);
				print json_encode($json_data);
			}
		}
	}


    
}
?>