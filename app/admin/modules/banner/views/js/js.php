<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_banner","<?php echo base_url().$this->config->item('index_page'); ?>banner/ajax_list_banner",form_data);
    });

</script>

<script type="text/javascript">
  // Add kurir

    function ajax_action_add_banner(){
      var file_data = $('#add_attacment_banner').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('name_banner', $('#name_banner').val());
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/banner/ajax_action_add_banner/","POST",form_data);
        }

     function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>banner/ajax_action_get_banner/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var name_banner =$('#edit_name_banner').val(response.data[0].name_banner);
              var id_file =$('#edit_id_file_banner').val(response.data[0].id_file);
              var id_banner =$('#edit_id_banner').val(response.data[0].id_banner);
              $("#edit_banner_preview").attr("src","<?= base_url().'uploads/banner/';?>"+response.data[0].nama_file);
             $('#page-load').hide();
          });
         $('#edit-banner').modal("show");
       
        }

         function ajax_action_edit_banner(){
           var file_data = $('#edit_attachment_banner').prop('files')[0];
          var form_data = new FormData();
          var name_banner =$('#edit_name_banner').val();
          var id_banner =$('#edit_id_banner').val();
          var id_file =$('#edit_id_file_banner').val();
          form_data.append('id_banner', id_banner);
          form_data.append('id_file', id_file);
          form_data.append('name_banner', name_banner);
          form_data.append('file', file_data);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>banner/ajax_action_edit_banner/","POST",form_data);
            }

</script>