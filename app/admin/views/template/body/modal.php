<!-- Admin modal Side -->

<div id="edit-admin" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2>Edit Data Admin</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama</label>
                    <input type="text" name="" id='edit_name_admin' class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Password</label>
                    <input type="password" name="" id="edit_password_admin" class="form-control" placeholder="Masukkan Password disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>KTP</label>
                    <input type="text" name="" id="edit_ktp_admin" class="form-control" placeholder="Masukkan KTP disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Email</label>
                    <input type="email" name="" id="edit_email_admin" class="form-control" placeholder="Masukkan Email disini">
                </div>
                 <div style="display: none">
                    <label>Email</label>
                    <input type="email" name="" id="edit_id_admin" class="form-control" placeholder="Masukkan Email disini">
                </div>

                 <div>
                    <br>
                    <label>Akses Admin</label>
                    <select id="edit_admin_akses" class="form-control"></select>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_edit_admin()">Edit Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="add-admin" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Tambah Data Admin</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama</label>
                    <input type="text" name="" id="name_admin" class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Password</label>
                    <input type="password" name="" id="password_admin" class="form-control" placeholder="Masukkan Password disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>KTP</label>
                    <input type="text" name="" id="ktp_admin" class="form-control" placeholder="Masukkan KTP disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Email</label>
                    <input type="email" name="" id="email_admin" class="form-control" placeholder="Masukkan Email disini">
                </div>

                  <div>
                    <br>
                    <label>Akses Admin</label>
                    <select id="add_admin_akses" class="form-control"></select>
                </div>
                <div style="margin-top: 3%"></div>
               
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_add_admin()">Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end admin modal side -->

<!-- modal Kurir side-->

<div id="edit-kurir" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Tambah Kurir</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama Kurir</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="edit_name_kurir">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Api Kurir</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Api disini" id="edit_api_kurir">
                        </div>
                         <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Status</label>
                            <select class="form-control text-popins" id="status_kurir">
                                <option value="1">Active</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Logo Kurir</label>
                            <input type="file" name="" class="form-control" name="file" accept="image/*" id="edit_attachment_kurir"  onchange="loadFile(event,'edit_kurir_preview')">
                        </div>
                        <div style="margin-top: 8%">
                            <center>
                                <img src="" id="edit_kurir_preview">
                            </center> 
                        </div>
                    </div>
                    <div style="display: none">
                            <input type="" name="" id="edit_id_kurir">
                            <input type="" name="" id="edit_id_file_kurir">
                    </div>
                </div>
                <div class="row" style="margin-top: 8%">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary " onclick="ajax_action_edit_kurir()">Edit Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="add-kurir" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">TAMBAH Kurir</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama Kurir</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="name_kurir">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Api Kurir</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Api disini" id="api_kurir">
                        </div>
                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Logo Kurir</label>
                            <input type="file" name="" class="form-control" accept="image/*" id="attachment_kurir"  onchange="loadFile(event,'kurir_preview')">
                        </div>
                        <div style="margin-top: 8%">
                            <center>
                                <img src="" id="kurir_preview">
                            </center> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary text-popins" onclick="ajax_action_add_kurir()">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- End kurir side modal -->

<!-- modal category side-->

<div id="add-category" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Tambah Category</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Category</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="name_category">
                        </div>

                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Icon Category</label>
                            <input type="file" name="" class="form-control" name="file" accept="image/*" id="attachment"  onchange="loadFile(event,'image_preview_category')">
                        </div>
                        <div style="margin-top: 7%">
                            <center>
                                <img src="" id="image_preview_category">
                            </center>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 5%">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary " onclick="ajax_action_add_category()">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="edit-category" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Edit Category</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama Category</label>
                            <input type="text" name="" id="edit_name_category" class="form-control" placeholder="Masukkan Nama disini">
                        </div>

                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Status</label>
                            <select class="form-control text-popins" id="status_category">
                                <option value="1">Active</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Icon Category</label>
                            <input type="file" name="" onchange="loadFile(event,'edit_category_preview')" class="form-control" name="file" accept="image/*" id="edit_attachment">
                        </div>
                        <div style="margin-top: 5%">
                            <center>
                                <img src="" id="edit_category_preview">
                            </center>
                        </div>
                    </div>
                    <div style="display: none">
                        <input type="text" id="edit_id_category" name="" class="form-control">
                        <input type="text" id="edit_id_file" name="" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary text-popins" onclick="ajax_action_edit_category()">Edit Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- End category side modal -->

<!-- start side modal payment-method -->

<div id="add-payment-method" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Tambah Payment method</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama</label>
                            <input type="text" name=""  id="name_payment_method" class="form-control" placeholder="Masukkan Nama disini">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Code</label>
                            <input type="text" name="" id="code_payment_method" class="form-control" placeholder="Masukkan code disini">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Api</label>
                            <input type="text" name="" id="api_payment_method" class="form-control" placeholder="Masukkan Api disini">
                        </div>
                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Icon Metode</label>
                            <input type="file" name="file" id="payment_method_attachment" class="form-control" onchange="loadFile(event,'image_preview_payment_method')"  accept="image/*">
                        </div>
                        <div style="margin-top: 7%">
                            <center>
                                <img src="" id="image_preview_payment_method">
                            </center>
                        </div>
                    </div>
                    
                </div>
                    <br>
                     <label class="text-popins">Tata Cara Pembayaran</label>
                     <div class="editor" id="catatan_method" onkeyup="add_desc_payment()">
                    </div>
                        <textarea class="form-control" style="display: none" id="desc_payment" name="desc_payment" rows="8"></textarea>

                <div class="row" style="margin-top: 8%"> 
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary" onclick="ajax_action_add_payment_method()">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="edit-payment-method" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Edit Payment Method</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama</label>
                            <input type="text" name="" id="edit_name_payment_method" class="form-control" placeholder="Masukkan Nama disini">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Code</label>
                            <input type="text" name="" id="edit_code_payment_method" class="form-control" placeholder="Masukkan code disini">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Api</label>
                            <input type="text" name="" id="edit_api_payment_method" class="form-control" placeholder="Masukkan Api disini">
                        </div>

                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Status</label>
                            <select class="form-control text-popins" id="edit_status_payment_method">
                                <option value="1">Active</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Icon Category</label>
                            <input type="file" name="" class="form-control"  onchange="loadFile(event,'edit_payment_method_preview')"  accept="image/*" id="edit_attachment_payment_method">
                        </div>
                        <div style="margin-top: 8%">
                            <center>
                                <img src="" id="edit_payment_method_preview">
                            </center>
                        </div>
                    </div>
                    <div style="display: none">
                        <input type="text" id="edit_id_payment_method" name="" class="form-control">
                        <input type="text" id="edit_id_file_payment_method" name="" class="form-control">
                    </div>
                </div>

                  <br>
                     <label class="text-popins">Tata Cara Pembayaran</label>
                     <div class="edit_editor" id="edit_catatan_method" onkeyup="edit_desc_payment()">
                    </div>
                        <textarea class="form-control" style="display: none" id="edit_desc_payment" name="edit_desc_payment" rows="8"></textarea>

                <div class="row" style="margin-top: 8%">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary text-popins" onclick="ajax_action_edit_payment_method()">Edit Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- end side modal payment-method -->


<!-- start sied modal payment-account -->

<div id="add-payment-account" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Tambah Payment Account</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-12">
                         <div>
                            <label class="text-popins">Payment Method</label>
                            <select class="form-control text-popins" id="id_payment_method">
                                <option>Auto</option>
                                <option>Manual</option>
                            </select>
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Nama</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="name_payment_account">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Nomor</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan code disini" id="nomor_payment_account">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Type</label>
                            <select class="form-control text-popins" id="type_payment_account">
                                <option value="1">Auto</option>
                                <option value="2">Manual</option>
                            </select>
                        </div>
                    </div>
                </div>
                 <div style="margin-top: 3%"></div>
                <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary " onclick="ajax_action_add_payment_account()">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="edit-payment-account" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Edit Payment Account</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div>
                            <label class="text-popins">Payment Method</label>
                            <select class="form-control text-popins" id="edit_select_id_payment_method">
                                <option value="1">Auto</option>
                                <option value="2">Manual</option>
                            </select>
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Nama</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="edit_name_payment_account">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Nomor</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan code disini" id="edit_nomor_payment_account">
                        </div>
                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Type</label>
                            <select class="form-control text-popins" id="edit_type_payment_account">
                                <option value="1">Auto</option>
                                <option value="2">Manual</option>
                            </select>
                        </div>

                        <div style="margin-top: 3%"></div>
                        <div>
                            <label class="text-popins">Status</label>
                            <select class="form-control text-popins" id="edit_status_payment_account">
                                <option value="1">Active</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>
                        <div style="display: none">
                                <input type="" name="" id="edit_id_payment_account">
                        </div>

                    </div>
                </div>
                <div style="margin-top: 3%"></div>
                <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary text-popins" onclick="ajax_action_edit_payment_account()">Edit Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end side modal payment-account -->

<!-- API side -->

<div id="edit-api" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Edit API</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama Layanan api</label>
                    <input type="text" name="" id="edit_name_api" class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Api Key</label>
                    <input type="text" name="" id="edit_api_key" class="form-control" placeholder="Masukkan Api disini" >
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                            <label class="text-popins">Status</label>
                            <select class="form-control text-popins" id="edit_status_api">
                                <option value="1">Active</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>

                <div style="display: none">
                        <input type="" id="edit_id_api" name="">
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_edit_api()">Edit Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="add-api" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Tambah API</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama Layanan api</label>
                    <input type="text" name="" id="name_api" class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Api Key</label>
                    <input type="text" name="" id="api_key" class="form-control" placeholder="Masukkan Api disini" >
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_add_api()">Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end admin modal side -->

<!-- end api side -->

<!-- level modal -->


<div id="add-level" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Tambah Level</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama level</label>
                    <input type="text" name="" id="name_level" class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Akses</label>
                    <div>
                        <table class="table table-bordered" id="list_menu">
                        </table>
                    </div>
                </div>
                <div style="display: none">
                    <input type="text" name="" id="akses" name="akses">  
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_add_level()">Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div id="edit-level" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Edit Level</h2>
                    </center>
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Nama level</label>
                    <input type="text" name="" id="edit_name_level" class="form-control" placeholder="Masukkan Nama disini">
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <label>Akses</label>
                    <div>
                        <table class="table table-bordered" id="edit_list_menu">
                        </table>
                    </div>
                </div>
                <div style="display: none">
                    <input type="text" name="" class="form-control" id="edit_akses" name="edit_akses">  
                    <input type="text" name="" class="form-control" id="edit_id_level" name="edit_id_level">  
                </div>
                <div style="margin-top: 3%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_edit_level()">Edit Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- end level modal -->

<div id="add-merchant" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Tambah Merchant</h2>
                    </center>
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>Nama Merchant</label>
                    <input type="text" name="" id="name_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>Owner</label>
                    <input type="text" name="" id="owner_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>

<div style="margin-top: 1%"></div>
                 <div>
                    <label>Username</label>
                    <input type="text" name="" id="username_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>Password</label>
                    <input type="password" name="" id="password_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>

<div style="margin-top: 1%"></div>
                 <div>
                    <label>Email</label>
                    <input type="text" name="" id="email_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>No handphone</label>
                    <input type="number" name="" id="no_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div style="margin-top: 1%"></div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Alamat</label>
                    <textarea class="form-control" id="alamat_lengkap_merchant">
                    </textarea>
                </div>
            <div style="margin-top: 1%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_add_merchant()">Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>


<div id="edit-merchant" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div>
                    <center>
                        <h2 class="text-popins">Edit Merchant</h2>
                    </center>
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>Nama Merchant</label>
                    <input type="text" name="" id="edit_name_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>Owner</label>
                    <input type="text" name="" id="edit_owner_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>

<div style="margin-top: 1%"></div>
                 <div>
                    <label>Username</label>
                    <input type="text" name="" id="edit_username_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>

<div style="margin-top: 1%"></div>
                 <div>
                    <label>Email</label>
                    <input type="text" name="" id="edit_email_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div>
                    <label>No handphone</label>
                    <input type="text" name="" id="edit_no_merchant" class="form-control" placeholder="Masukkan  disini">
                </div>
<div style="margin-top: 1%"></div>
                 <div style="margin-top: 1%"></div>
                <div style="margin-top: 1%"></div>
                <div>
                    <label>Alamat</label>
                    <textarea class="form-control" id="edit_alamat_lengkap_merchant">
                    </textarea>
                </div>
                 <div style="display: none">
                    <input type="text" name="" id="edit_id_merchant" class="form-control" placeholder="Masukkan Email disini">
                </div>
            <div style="margin-top: 1%"></div>
                <div>
                    <div class="row">
                        <div class="col-12" style="text-align: right;">
                            <button class="btn btn-primary " onclick="ajax_action_edit_merchant()">Edit Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<!-- Merchant -->



<!-- end merchant -->

<!-- Banner -->

<div id="add-banner" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Tambah Banner</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama Banner</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="name_banner">
                        </div>
                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Banner</label>
                            <input type="file" name="" class="form-control" name="file" accept="image/*" id="add_attacment_banner"  onchange="loadFile(event,'banner_preview')">
                        </div>
                        <div style="margin-top: 8%">
                            <center>
                                <img src="" id="banner_preview">
                            </center> 
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 8%">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary " onclick="ajax_action_add_banner()">Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div id="edit-banner" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 3%">
                <div style="text-align: center;">
                    <h3 class="text-popins">Edit banner</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Nama Banner</label>
                            <input type="text" name="" class="form-control" placeholder="Masukkan Nama disini" id="edit_name_banner">
                        </div>
                    </div>
                    <div class="col-6">
                        <div>
                            <label class="text-popins">Logo Banner</label>
                            <input type="file" name="" class="form-control" name="file" accept="image/*" id="edit_attachment_banner"  onchange="loadFile(event,'edit_banner_preview')">
                        </div>
                        <div style="margin-top: 8%">
                            <center>
                                <img src="" id="edit_banner_preview">
                            </center> 
                        </div>
                    </div>
                    <div style="display: none">
                            <input type="" name="" id="edit_id_banner">
                            <input type="" name="" id="edit_id_file_banner">
                    </div>
                </div>
                <div class="row" style="margin-top: 8%">
                    <div class="col-12" style="text-align: right;">
                        <button class="btn btn-primary " onclick="ajax_action_edit_banner()">Edit Data</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end banner -->


<!-- modal_status -->

<div id="modal_status" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Pemesanan <label id="modal_id_order"></label></h4>
      </div>
      <div class="modal-body">
            <div class="row">
                    <div class="col-sm-6">
                        <img  style="width: 85% " id="pemesanan_preview">
                    </div>
                    <div class="col-sm-6">
                     <div style="display: none">
                            <label>Kode Unik</label>
                            <input type="text" name="" class="form-control" readonly="" id="id_order">
                        </div>
                        <div>
                            <label>Kode Unik</label>
                            <input type="text" name="" class="form-control" readonly="" id="kode_unik">
                        </div>
                        <div style="margin-top: 5px">
                            <label>Harga</label>
                            <input type="text" name="" class="form-control" readonly="" id="pemesanan_price">
                        </div>

                        <div style="margin-top: 5px">
                            <label>Bank Admin</label>
                            <input type="text" name="" class="form-control" readonly="" id="pemesanan_bank">
                        </div>
                        <hr>
                        <div style="margin-top: 5px">
                            <label>Status Pesanan</label>
                            <h3 id="status_text"></h3>
                        </div>
                        <div style="margin-top: 12px">
                        <div id="teruskan">
                            <div class="row">
                                <div class="col-sm-6"><input type="button" name="" class="btn btn-success btn-sm" value="Pembayaran Telah Sesuai" onclick="change_status(3)" id="to_merchant"></div>
                                <div class="col-sm-6"><input type="button" name="" class="btn btn-danger btn-sm" value="Pesanan di batalkan" onclick="change_status(4)" id="to_batalkan"></div>
                            </div>
                        </div>
                        <div id="pesanan_slesai" style="display: none">
                            <br>
                           <input type="button" name="" class="btn btn-success btn-sm" value="Kirimkan Saldo ke merchant" style="width: 100%" onclick="transfer_to_merchant()">
                        </div>
                        </div>
                    </div>
            </div>
            <div class="row container" >
                    <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Nama Product</td>
                                <td>Merchant</td>
                                <td>Harga</td>
                                <td>Kurir</td>
                                <td>Status</td>
                            </tr>
                            </thead>
                            <tbody id="table_cart">
                                
                            </tbody>
                    </table>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- end update kuri -->

<!-- modal withdraw -->

<div id="modal_withdraw" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Penarikan</h4>
      </div>
      <div class="modal-body">
            <div class="row">
                    <div style="display: none">
                        <input type="text" name="" id="wd_id_merchant">
                         <input type="text" name="" id="wd_id_withdraw">
                    </div>
                    <div class="col-sm-6">
                        <img  style="width: 85% " >
                        <div style="margin-top: 5px">
                            <label>Upload Bukti Transfer </label>
                           
                        <img  style="width: 85% "  id="preview_wd">
                            <input type="file" name="" class="form-control"  onchange="loadwd(event)" id="attachment_wd">
                        </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="col-sm-12">
                          <label>Nama Bank</label>
                            <input type="text" name="no_rek_wd" id="bank_rek_wd" class="form-control">
                    </div>
                    <div class="col-sm-12" style="margin-top: 5px">
                            <label>No Rekening</label>
                            <input type="text" name="no_rek_wd" id="no_rek_wd" class="form-control">
                    </div>
                    <div class="col-sm-12" style="margin-top: 5px">
                            <label>Nama Pengirim </label>
                            <input type="text" name="no_rek_wd" id="nama_rek_wd" class="form-control">
                    </div>
                    </div>
            </div>
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-success" onclick="ajax_action_update_widthdraw()">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- end modal withdraw -->