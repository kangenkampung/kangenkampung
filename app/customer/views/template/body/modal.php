<div>
    <!-- modal -->

    <!-- mdoal -login -->

    <!-- Modal -->
    <div class="modal fade" id="login-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered w-15">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="card-modal">
                    <div class="modal-body">
                        <div class="login-modal-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-size: 22px;font-weight: bold">Masuk</label>
                                            </div>
                                            <div class="col-6" style="text-align: right;">
                                                <label class="primary-color" style="font-size: 12px;font-weight: bold;">Daftar</label>
                                            </div>

                                        </div>
                                    </div>
                                    <form action="" id="form_login">
                                        <div>
                                            <label style="font-size: 12px;">Email</label>
                                            <input type="text" name="" class="form-control" id="email_member">
                                            <label style="font-size: 11px;color: grey">Contoh: email@kangenkampung.com</label>
                                        </div>

                                        <div>
                                            <label style="font-size: 12px;">Password</label>
                                            <input type="password" name="" class="form-control" id="password_member">
                                        </div>
                                        <br>
                                        <div>
                                            <input type='submit' class="btn btn-success" style="width: 100%" value="Masuk">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="category-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered w-15">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="card-modal">
                    <div class="modal-body">

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal burger icon-->
    <div class="modal fade" id="nav-menu" role="dialog">
        <div class="modal-dialog modal-dialog-centered w-15">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="card-modal">
                    <div class="modal-body">
                        <div class="login-modal-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-size: 22px;font-weight: bold">Menu</label>
                                            </div>

                                        </div>

                                        <div>
                                            <a href="<?php echo base_url().index_page(); ?>register">
                                                <button class="btn btn-success" style="width: 100%">Daftar</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- lokasi pengiriman -->
<div>
    <div>
        <div class="modal fade" id="alamat-modal" role="dialog">
            <div class="modal-dialog modal-dialog-centered dialog-w-3">

                <!-- Modal content-->

                <div class="modal-content">
                    <div class="checkout-card-modal">
                        <div class="">
                            <div class="" style="">
                                <div class="row">
                                    <div class="col-2" style="margin-left: 6px">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="col-9">
                                        <label class="title-checkout-modal">Alamat Tujuan</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-body">
                         <?php foreach ($alamat as $alamats) { ?>
                        <div>
                             <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <label style="font-weight: normal;">Nama Penerima</label>
                                        </div>
                                         <div class="col-6" style="text-align: right;">
                                           <button class="btn btn-success btn-sm" onclick="setAlamat('<?= $alamats->id_alamat ?>')">Gunakan Ini</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                          <?= $alamats->name_penerima?>
                                    </label>
                                </div>
                            </div>


                             <div class="row">
                                <div class="col-12">
                                    <label style="font-weight: normal;">Daerah Pengiriman</label>
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                           <?= $alamats->kota_alamat." | ".$alamats->kode_pos_alamat ?>
                                    </label>
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-12">
                                    <label style="font-weight: normal;">Alamat Lengkap</label>
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                              <?= $alamats->alamat_lengkap?>
                                    </label>
                                </div>
                            </div>
 <hr>
 <?php } ?>
                            <!-- <div class="checkout-modal-header">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div>
                                            <label style="font-size: 12px;">Alamat Lengkap</label>
                                            <textarea class="form-control">

                                            </textarea>
                                            <label style="font-size: 11px;color: grey">Contoh: Perumahan Junrejo indah no a25 pojok Junrejo, Kota Batu, 65321</label>
                                            <div style="height: 10px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- pembayaran  -->

<div>
    <div class="modal fade" id="checkout-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered ">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="checkout-card-modal">
                    <div class="">
                        <div class="" style="">
                            <div class="row">
                                <div class="col-2 step-two" style="margin-left: 6px;display: none">
                                    <button type="button" class="close back-payment"><i class="fa fa-arrow-left fa-xs" aria-hidden="true" style="color: #9a9898;font-size: 20px"></i></button>
                                </div>
                                <div class="col-2 step-one" style="margin-left: 6px;display: block">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="col-9" style="margin-left: -2px">
                                    <label class="title-checkout-modal">Pembayaran</label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="checkout-modal-header">
                            <div class="row step-one">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div>
                                            <div class="row">
                                                <div class="col-5" style="margin-top: 18px;margin-left: 14px">
                                                    <p style="font-weight: bold;font-size: 15px">Total Tagihan</p>
                                                </div>
                                                <div class="col-6" style="margin-top: 15px;text-align: right;">
                                                    <p class="primary-color total_price" style="font-weight: bold;font-size: 20px"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card" style="margin-top: 7px;margin-bottom: 12px" id="list_payment">
                                       
                                        <div style="height: 12px"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row step-two" style="display: none">
                                <div class="col-12">
                                    <div class="card">
                                        <div>
                                            <div class="row">
                                                <div class="col-5" style="margin-top: 18px;margin-left: 14px">
                                                    <p style="font-weight: bold;font-size: 15px">Total Tagihan</p>
                                                </div>
                                                <div class="col-6" style="margin-top: 15px;text-align: right;">
                                                    <p class="primary-color total_price" style="font-weight: bold;font-size: 20px"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card" style="margin-top: 15px">
                                        <div class="row card-detail-payment">
                                            <div class="col-4" style="margin-top: 18px;margin-left: 14px">
                                                <p style="font-size: 15px">Briva</p>
                                            </div>
                                            <div class="col-7" style="margin-top: 15px;text-align: right;">
                                                <img src="https://ecs7.tokopedia.net/img/toppay/sprites/bca.png" style="width: 60px">
                                            </div>
                                        </div>
                                        <div class="detail-text-payment">
                                            <label>
                                                1.Transaksi ini akan otomatis menggantikan tagihan BRIVA yang belum dibayar.
                                                <br> 2.Dapatkan kode pembayaran setelah klik “Bayar”.
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="card" style="margin-top: 5%">
                                            <a href="Pembayaran">
                                                <button class="btn btn-success" style="width: 100%">Bayar</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal profil-->
<div class="modal fade" id="add-alamat-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered w-15">

        <!-- Modal content-->

        <div class="modal-content">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style="font-size: 22px;font-weight: bold">Tambah Alamat </label>
                                        </div>
                                    </div>
                                </div>
                                <form action="" id="form_add_alamat">

                                    <div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label style="font-size: 12px;">Nama Alamat</label>
                                                <input type="text" name="" class="form-control" id="name_alamat">
                                                <label style="font-size: 11px;color: grey">Contoh: alamat rumah , kantor</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label style="font-size: 12px;">Nama Penerima</label>
                                                <input type="text" name="" class="form-control" id="name_penerima">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label style="font-size: 12px;">Nama Kota</label>

                                                <select class="form-control select2" id="kota_alamat" style="width: 100%;height: 21px"> 
                                                 </select> 
                                            </div>
                                              <div class="col-sm-4">
                                                <label style="font-size: 12px;">Nama Kecamatan</label>

                                                <select class="form-control select2" id="kecamatan_alamat" style="width: 100%;height: 21px"> 
                                                 </select> 
                                            </div>
                                            <div class="col-sm-4">
                                                <label style="font-size: 12px;">Kode Pos</label>
                                                <input type="text" name="" class="form-control" id="kode_pos_alamat">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <label style="font-size: 12px;">Alamat Lengkap</label>
                                        <textarea class="form-control" id="alamat_lengkap">

                                        </textarea>
                                    </div>
                                    <br>
                                    <div>
                                        <input type='submit' class="btn btn-success" style="width: 100%" value="Tambah">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


<!-- Modal profil-->
<div class="modal fade" id="edit-alamat-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered w-15">

        <!-- Modal content-->
            <input type="text" name="" class="form-control" id="id_alamat" style="display: none">
        <div class="modal-content">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style="font-size: 22px;font-weight: bold">Ubah Alamat </label>
                                        </div>
                                    </div>
                                </div>
                                <form action="" id="form_edit_alamat">

                                    <div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label style="font-size: 12px;">Nama Alamat</label>
                                                <input type="text" name="" class="form-control" id="edit_name_alamat">
                                                <label style="font-size: 11px;color: grey">Contoh: alamat rumah , kantor</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label style="font-size: 12px;">Nama Penerima</label>
                                                <input type="text" name="" class="form-control" id="edit_name_penerima">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label style="font-size: 12px;">Nama Kota</label>
                                                <select class="form-control select2" id="edit_kota_alamat" style="width: 100%;height: 21px"> 
                                                 </select> 
                                            </div>

                                             <div class="col-sm-4">
                                                <label style="font-size: 12px;">Nama Kecamatan</label>
                                                <select class="form-control select2" id="edit_kecamatan_alamat" style="width: 100%;height: 21px"> 
                                                 </select> 
                                            </div>
                                            <div class="col-sm-4">
                                                <label style="font-size: 12px;">Kode Pos</label>
                                                <input type="text" name="" class="form-control" id="edit_kode_pos_alamat">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <label style="font-size: 12px;">Alamat Lengkap</label>
                                        <textarea class="form-control" id="edit_alamat_lengkap">

                                        </textarea>
                                    </div>
                                    <br>
                                    <div>
                                        <input type='submit' class="btn btn-success" style="width: 100%" value="Ubah">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<!-- MModal pemabayran -->

<div class="modal fade" id="payment-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered w-15">

        <!-- Modal content-->
            <input type="text" name="" class="form-control" id="id_alamat" style="display: none">
        <div class="modal-content">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style="font-size: 22px;font-weight: bold">Ungah Bukti Pembayaran </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- <form action="" id="form_upload"> -->
                                   <center> <img src="" style="width: 85%" id="preview_payment"></center>
                                
                                 <div style="margin-top: 5px">
                                      <input type="file" name="" class="form-control" accept="image/*" onchange="loadpayment(event)" id="attachment_pembayaran">
                                      <br>
                                      <input type="submit" name="" class="btn btn-success" onclick="ajax_action_add_pembayaran()" style="width: 100%">
                                 </div>
                                <!-- </form> -->
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<!-- Modal pembayaran -->

<div class="modal fade" id="modal-pembelian" role="dialog">
    <div class="modal-dialog modal-dialog-centered w-15 modal-lg">
        <div class="modal-content">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="">
                        <div class="row">
                            <div class="col-sm-3" style="cursor: pointer" id="list_barang_checkout">
                               
                            </div>
                            <div class="col-sm-8" >
                                <div class="row" id="detail_tracking">
                                    
                                </div>
                                <br>
                                   <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td>ID</td>
                                                <td>Penerima</td>
                                                <td>Sampai</td>
                                                <td>Deskripsi</td>
                                                <td>Status Tracking</td>
                                            </tr>
                                        </thead>
                                        <tbody id="data_resi">
                                            
                                        </tbody>
                                 </table>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>