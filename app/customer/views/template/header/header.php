<!DOCTYPE html>
<html>
<head>
	<title>JualinAja</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/customer/css/kangenkampung.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customer/css/slick.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/customer/dist/easy-autocomplete.min.css" rel="stylesheet" type="text/css">
   	<script src="<?php echo base_url(); ?>assets/customer/dist/jquery.easy-autocomplete.min.js" type="text/javascript" ></script>
	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
</head>

<style type="text/css">
 .loader {
position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    color:darkred;
}
</style>
<div id="page-load" style="position: fixed; z-index: 999999; opacity: 0.7;  background: white; width: 100%; height: 100%; display: block;top: 0;  ">
    <div class="loader">
      <div>
        <img src="<?php echo base_url(); ?>/assets/loading.gif"></div>
      </div>
 </div>

<?php if($content != 'profil'){
		$style = 'background: #ffffff;';
	}else{
		$style = 'background: #f8f8f8;';
	} ?>
<body style="<?= $style ?>" id="body">

<nav class="navbar-alert fixed-top" id="navbar-alert"  style="zoom: 90%;">
		<input id="count_cart" style="display: none">
		<input id="count_pembayaran" style="display: none">
		<div class="navbar-header navbar-pointer scroll-hide close-overlay">
				    <div Class="navbar-left">
				    	<div class="navbar-contact">
                        Call us on (504) 305-0554
                    </div>
				    </div>
				    <div class="navbar-right">
				    	<div class="row">
                            <div class="col-sm-1 mr7">
                                Home
                            </div>
                            <div class="col-sm-1 mr7">
                                About 
                            </div>
                            <div class="col-sm-1">
                                Contact
                            </div>
                    </div>
				    </div>
		  </div>

		 
		  <?php if($this->session->userdata('id') == null){?>
		  <div class="navbar-body" style="zoom: 90%">
		  	<div class="row ">
		  		<div class="col-sm-2 " style="margin-top: 21px;margin-left: 29px">
					  	<div class="logo-header">
					  			<div class="text-logo" style="font-size: 38px">
					  				<center><label  style="font-weight: bold;color: grey">Jualin</label><label class="primary-color">Aja</label></center>
					  			</div>
					  	</div>	
		  		</div>
		  		<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend ">
										   <input type="button" name="" class="dropdown_product_cat " value="Kategori" data-toggle="modal" data-target="#category-modal" >
						  </div>
					      <input type="text" class="form-control search-box close-overlay search_bar" placeholder="Cari Barang anda di sini ..." >
					       <div class="input-group-prepend close-overlay navbar-pointer">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-1 close-overlay"  style="margin-top: 11px;">
		  				<div class="btn-group">
						   	<button type="button" class="navbar-login" data-toggle="modal" data-target="#login-modal">Masuk</button>
						    <a href="<?php echo base_url().index_page(); ?>register"><button type="button" class="navbar-daftar">Daftar</button></a>
					</div>
		  		s</div>
		  	</div>
		  </div>

		 <?php }else{?>
		  <div class="navbar-body" >
		  	<div class="row ">
		  		<div class="col-sm-2 " style="margin-top: 15px;">
					  	<div class="text-logo" style="font-size: 38px">
					  				<center><label  style="font-weight: bold;color: grey">Jualin</label><label class="primary-color">Aja</label></center>
					  			</div>
		  		</div>
		  			<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend ">
										   <input type="button" name="" class="dropdown_product_cat " value="Kategori" data-toggle="modal" data-target="#category-modal" >
						  </div>
					      <input type="text" class="form-control search-box close-overlay search_bar" placeholder="Cari Barang anda di sini ..." >
					       <div class="input-group-prepend close-overlay navbar-pointer">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-2">
		  				<div class="row" >
						    < <label style="padding-top: 12px;padding-top: 12px;">

						        <div id="ex4" onclick="riwayat()">
						            <span class="p1 fa-stack  has-badge" id="checkout_count" style="font-size: 19px">
						    		<i class="fa fa-credit-card fa-lg navbar-pointer notif-background  " data-count="4b" style="font-size: 19px"></i>
						  </span>
						        </div>

						    </label>
						    <label style="padding-top: 12px;padding-top: 12px;" >

						        <div id="ex4" onclick="cart()">
						            <span class="p1 fa-stack  has-badge cartIcon" id="cart_count"  style="font-size: 19px">
						    		<i class="fa fa-shopping-cart fa-lg navbar-pointer notif-background " data-count="4b" style="font-size: 19px"></i>
						  			</span>
						        </div>
						    </label>
						   
						    <!-- </div> -->
						    <label class="notif" style="padding-right: 3px;padding-top: 15px;">
						    </label>
						   <a href="<?php echo base_url().index_page(); ?>profil">
						    <label style="padding-left: 6px;padding-right: 6px;padding-top: 16px;">
						        <button onclick="detail_profil(1)" class="dropbtn navbar-login-name">
						            <?= substr(array_data_member($this->session->userdata('id'))->name_member,0,8);?>..
						        </button>
						    </label>
							</a>
						</div>
		  					<!-- 	<div class="col-sm-8 navbar-pointer" style="margin-left: -11%">
		  								<div class="notif-profil">
		  							<div class="dropdown">
	  								<button onclick="detail_profil(1)" class="dropbtn navbar-login-name"><?= array_data_member($this->session->userdata('id'))->name_member?></button>
	  								<div id="myDropdown" class="dropdown-content">
	    								<div class="card">
	    									<div style="margin: 3%">
	    											<a href="<?php echo base_url().index_page(); ?>profil">
	    											<div class="card-name-login" style="color: black;">
	    												 <label style="font-size: 14px;font-weight: bold;text-align: center;margin-top: 2%">
	    												 <?= array_data_member($this->session->userdata('id'))->name_member?></label>
	    											</div>
	    											</a>
	    									</div>
	    								<div class="row" style="text-align: left;padding-left: 3%;padding-right:3%">
	    										<div class="col-sm-12"  onclick="logout()">
	    											<div class="card-name-login navbar-pointer">
	    												<center>
	    													<label style="font-size: 14px;text-align: center;margin-top: 2%" class="navbar-pointer">Keluar</label>
	    												</center>
	    											</div>
	    									</div>
	    								</div>
	    								<div style="height: 8px"></div>
									    </div>
									    </div>
		  								</div>
		  						</div>
		  				</div>	 -->		
		  		</div>
		  	</div>
		  </div>
		  </div>
		  <?php } ?>
		  <div class="appointment close-overlay">
		  	Fitur terbaru telah hadir segera rasakan
		  </div>
	</nav>


<!-- Mobile Nav -->

<nav class="navbar-alert-mobile fixed-top ">
		<div class="col-sm-12">
		<div class="row">
			<div class="col-2 navbar-pointer"  style="margin-left:2%;margin-top: 15px">
			<?php if($this->uri->segment(1) != ''){ ?>
				<div>
					<i class="fa fa-arrow-left fa-lg primary-color"  aria-hidden="true" onclick="window.history.back();"></i>
				</div>
			<?php }else{?>
				<div>
					<i class="fa fa-align-justify fa-lg primary-color"  aria-hidden="true" data-toggle="modal" data-target="#nav-menu"></i>
				</div>
			<?php } ?>
			</div>
			<div class="col-8" style="text-align: center;margin-top: 11px">
				<div id="after_search">
					  			<input type="text" name="" class="form-control search_bar" placeholder="Cari di sini" style="height: 35px;width: 100%">
					  		</div>
	</div>

			<div class="col-1">
				<div style="margin-top: 15px" style="text-align: right;">
					<i class="fa fa-search fa-lg primary-color"  aria-hidden="true" ></i>
				</div>
			</div>
		</div>
		</div>
	</nav>
