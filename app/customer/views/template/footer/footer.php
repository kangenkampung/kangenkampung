</div>
</body>

<?php if($content != 'profil'){
        $style_footer = 'background: #f8f8f8;';
    }else{
        $style_footer = 'background: #ffffff';
    } ?>

<!-- Pc Footer Design -->
<div class="div-height-footer"></div>
<footer class="footer-div" style="<?= $style_footer ?>">
    <div>
        <img src="../kangenkampung/images/logo.png" class="logo-footer">
    </div>
    <div style="margin-top: 2%">
        <label>120 Jasmine Ln, Westwego, LA, 70094</label>
        <br>
        <label>(504) 305-0554 &nbsp;&nbsp;</label>
        <label class="primary-color">hello@tumbas.com</label>
    </div>
    <div class="hr-footer">
        <hr>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-4" style="text-align: left">
                <ul>
                    <ol style="padding-top: 5%">About</ol>
                    <ol style="padding-top: 5%">Contact</ol>
                    <ol style="padding-top: 5%">Careers</ol>
                    <ol style="padding-top: 5%">Press Releases</ol>
                    <ol style="padding-top: 5%">In The Media</ol>
                    <ol style="padding-top: 5%">Testimonials</ol>
                </ul>
            </div>

            <div class="col-sm-4" style="text-align: left">
                <ul>
                    <ol style="padding-top: 5%">About</ol>
                    <ol style="padding-top: 5%">Contact</ol>
                    <ol style="padding-top: 5%">Careers</ol>
                    <ol style="padding-top: 5%">Press Releases</ol>
                    <ol style="padding-top: 5%">In The Media</ol>
                    <ol style="padding-top: 5%">Testimonials</ol>
                </ul>
            </div>

            <div class="col-sm-4" style="text-align: left">
                <ul>
                    <ol style="padding-top: 5%">About</ol>
                    <ol style="padding-top: 5%">Contact</ol>
                    <ol style="padding-top: 5%">Careers</ol>
                    <ol style="padding-top: 5%">Press Releases</ol>
                    <ol style="padding-top: 5%">In The Media</ol>
                    <ol style="padding-top: 5%">Testimonials</ol>
                </ul>
            </div>

            <div class="col-sm-4">
            </div>

            <div class="col-sm-4">
            </div>
        </div>
    </div>
</footer>

<!-- Mobile Tab Design -->

<?php if($content == 'dashboard'){ 
	$active1 = 'background-primary-deactive'; 
	$active2 = 'background-primary-active'; 
	$active3 = 'background-primary-deactive';
    $style   =  'display:block';
 }else if($content == 'detail_product'){
	$active1 = 'background-primary-deactive';
	$active2 = 'background-primary-active'; 
	$active3 = 'background-primary-deactive'; 
    $style   =  'display:block';
 }else if($content == 'profil'){
    $active1 = 'background-primary-deactive';
    $active2 = 'background-primary-deactive'; 
    $active3 = 'background-primary-active'; 
    $style   =  'display:block';
 }else{
    $active1 = 'background-primary-deactive';
    $active2 = 'background-primary-deactive'; 
    $active3 = 'background-primary-active'; 
    $style   =  'display:none';
 }?>
 <?php if ($this->agent->is_mobile()) {?>  
<div class="bottom-navigation-mobile" style="<?= $style?>">
    <footer>
        <nav class="fixed-bottom">
            <div class="navbar-body-bottom">
                <div class="row">
                    <div class="col-4 nav-bottom-item" >
                     <a href="<?php echo base_url().index_page(); ?>keranjang">
                        <div style="margin-top: 12%">
                        <?php if($count_cart != 0){?>
                            <div class="navbar-item-image" style="padding-right: 33px">
                                <i class="fa fa-shopping-cart fa-lg primary-color"><span style="font-size: 12px;color: red;"><?= $count_cart?></span></i>
                            </div>
                        <?php }else{ ?>
                         <div class="navbar-item-image" >
                                <i class="fa fa-shopping-cart fa-lg primary-color"></i>
                            </div>
                        <?php } ?>
                            <div class="border-bottom-menu <?= $active1?>" style="height: 2px;">

                            </div>
                        </div>
                     </a>
                    </div>

                    <div class="col-4 nav-bottom-item">
                     <a href="<?php echo base_url().index_page(); ?>dashboard">
                        <div style="margin-top: 12%">
                            <div class="navbar-item-image">
                                <i class="fa fa-home fa-lg primary-color"></i>
                            </div>
                            <div class="border-bottom-menu <?= $active2?>" style="height: 2px;"></div>

                        </div>
                    </a>
                    </div>
  <?php if($this->session->userdata('id') == null){?>
                    <div class="col-4 nav-bottom-item" data-toggle="modal" data-target="#login-modal">
                        <div style="margin-top: 12%">
                            <div class="navbar-item-image">
                                <i class="fa fa-user-circle-o fa-lg primary-color"></i>
                            </div>
                            <div class="border-bottom-menu <?= $active3?>" style="height: 2px;"> </div>
                        </div>
                    </div>
                         <?php }else{?>

                          <div class="col-4 nav-bottom-item" >
                          <a href="<?php echo base_url().index_page(); ?>profil">
                        <div style="margin-top: 12%">
                            <div class="navbar-item-image">
                                <i class="fa fa-user-circle-o fa-lg primary-color"></i>
                            </div>
                            <div class="border-bottom-menu <?= $active3?>" style="height: 2px;"> </div>
                        </div>
                        </a>
                    </div>
                         <?php } ?>
                </div>
            </div>
        </nav>
    </footer>
</div>
<?php } ?>
<script type="text/javascript">
    $("#form_login").submit(function(e) {
            e.preventDefault();
            var form_data = new FormData();
            form_data.append('email_member', $('#email_member').val());
            form_data.append('password_member', $('#password_member').val());
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            addItemSerializeLogin("<?php echo base_url().$this->config->item('index_page'); ?>/dashboard/ajax_action_login/","POST",form_data);
          
    });


     $("#form_add_alamat").submit(function(e) {
            e.preventDefault();
            var form_data = new FormData();
            form_data.append('name_alamat', $('#name_alamat').val());
            form_data.append('name_penerima', $('#name_penerima').val());
            form_data.append('kota_alamat', $('#kota_alamat :selected').text());
            form_data.append('id_kota_alamat', $('#kota_alamat').val());
            form_data.append('id_kota_alamat', $('#kota_alamat :selected').val());

            form_data.append('kode_pos_alamat', $('#kode_pos_alamat').val());

            form_data.append('kecamatan_alamat', $('#kecamatan_alamat :selected').text());
            form_data.append('id_kecamatan', $('#kecamatan_alamat :selected').val());


            form_data.append('alamat_lengkap', $('#alamat_lengkap').val());
            form_data.append('status_alamat', 0);
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/profil/ajax_action_add_alamat/","POST",form_data);

            location.reload();
    });

     $("#form_edit_alamat").submit(function(e) {
            e.preventDefault();
            var form_data = new FormData();
            form_data.append('id_alamat', $('#id_alamat').val());
            form_data.append('name_alamat', $('#edit_name_alamat').val());
            form_data.append('name_penerima', $('#edit_name_penerima').val());
            form_data.append('kota_alamat', $('#edit_kota_alamat :selected').text());
            form_data.append('id_kota_alamat', $('#edit_kota_alamat :selected').val());

            form_data.append('kecamatan_alamat', $('#edit_kecamatan_alamat :selected').text());
            form_data.append('id_kecamatan', $('#edit_kecamatan_alamat :selected').val());


            form_data.append('kode_pos_alamat', $('#edit_kode_pos_alamat').val());
            form_data.append('alamat_lengkap', $('#edit_alamat_lengkap').val());
            form_data.append('status_alamat', 0);
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/profil/ajax_action_edit_alamat/","POST",form_data);
    });


     function logout(){
            var form_data = new FormData();
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/dashboard/ajax_action_logout/","POST",form_data);
     }


</script>

<script>
    $(function() {
        get_count_cart_data();
        get_count_pembayaran_data();
        set_kota();
        // $('#body').zoom = "90%";
        $('.select2').select2();
        var options = {
            url: "<?php echo base_url().$this->config->item('index_page'); ?>dashboard/get_data_product/",
            getValue: "name_product",
            list: {
                match: {
                    enabled: true
                }
            }
        };

        $(".search_bar").easyAutocomplete(options);
});
        </script>

<script type="text/javascript">
     function set_kota(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_provinci_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "city");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kota</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#kota_alamat').html(html);
        $('#edit_kota_alamat').html(html);
        $('#page-load').hide();
        });

            $('#kota_alamat').on('change', function() {
            ajax_action_get_kecamatan(this.value);
        });

              $('#edit_kota_alamat').on('change', function() {
            ajax_action_get_kecamatan(this.value);
        });

  }


  function ajax_action_get_kecamatan(id){
        var form_data = new FormData();
    var html = '';
    var i;
      form_data.append('param', "subdistrict?city="+id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_provinci_ongkir";
    ajaxShowData(link, "POST", form_data, function(response) {
         for(i=0; i<response.rajaongkir.results.length; i++){
            html += "<option value='" + response.rajaongkir.results[i].subdistrict_id + "'>" + response.rajaongkir.results[i].subdistrict_name + "</option>";
        }
        $('#edit_kecamatan_alamat').html(html);
        $('#kecamatan_alamat').html(html);
        $('#page-load').hide();
    });
  }

</script>

<script type="text/javascript">
    function cart(){
        var cart = $('#count_cart').val();
        if(cart < 1){
            message("Hai "+'<?= array_data_member($this->session->userdata('id'))->name_member?>', "keranjang anda sedang kosong", "info","info",10000);
        }else{
            location.href="<?php echo base_url().$this->config->item('index_page'); ?>keranjang"
            // alert(cart);
        }
    }

    function riwayat(){
        var cart = $('#count_pembayaran').val();
        if(cart < 1){
            message("Hai "+'<?= array_data_member($this->session->userdata('id'))->name_member?>', "Tidak ada pesaan aktif saat ini","info",10000);
        }else{
            location.href="<?php echo base_url().$this->config->item('index_page'); ?>checkout/riwayat_pembelian/"
        }
    }
</script>


<script type="text/javascript">
    function setAlamat(id_alamat){
         var form_data = new FormData();
          form_data.append('id_alamat', id_alamat);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/checkout/ajax_edit_alamat/","POST",form_data);
    }
</script>


<script type="text/javascript">
      function get_count_cart_data(){
        var html = '';
        var count = 0;
        var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_count_cart";      
        $.getJSON(link2, function(data) {           
               if(data.data  == 0){
                    $('.cart_count').hide();
                }else{
                    $('#cart_count').attr("data-count",data.data);
                    $('.cart_count').show();
                    $('#count_cart').val(data.data);
                }
        })
}
</script>

<script type="text/javascript">
      function get_count_pembayaran_data(){
        var html = '';
        var count = 0;
        var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_count_pembayaran";      
        $.getJSON(link2, function(data) {           
               if(data.data  == 0){
                    $('.checkout_count').hide();
                }else{
                    $('#checkout_count').attr("data-count",data.data);
                    $('.checkout_count').show();
                    $('#count_pembayaran').val(data.data);

                }
        })
}
</script>

 <script src="<?php echo base_url(); ?>assets/admin/ajaxFunction.js"></script>
 <script src="<?php echo base_url(); ?>assets/customer/js/slick.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/bootstrap.bundle.min.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/es5/script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/es5/sidebar.large.script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/kangenkampung.js"></script>
</html>