<?php
class ElasticEmail
{
    private $apikey = "ab9d7ceb-53ba-48f5-8901-5b012c14fcce";
    private $url =  'https://api.elasticemail.com/v2/email/send';


    public function send($from,$fromName,$subject,$to,$bodyText,$bodyHtml = "",$isTransactional = false){
        try{
            $post = array('from' => $from,
                'fromName' => $fromName,
                'apikey' => $this->apikey,
                'subject' => $subject,
                'to' => $to,
                'bodyHtml' => $bodyHtml,
                'bodyText' => $bodyText,
                'isTransactional' => $isTransactional);

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $this->url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => false
            ));

            $result=curl_exec ($ch);
            curl_close ($ch);

            return $result;
        }
        catch(Exception $ex){
            return $ex->getMessage();
        }
    }
}