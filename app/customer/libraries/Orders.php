<?php
class Orders{
    public function __construct(){
        $this->CI =& get_instance();
    }
    
    public function tes_curl($url){
		$header = array("Accept: application/json");

        $this->CI->curl->create($url);
		$this->CI->curl->option(CURLOPT_SSL_VERIFYPEER, 0);
		$this->CI->curl->option(CURLOPT_SSL_VERIFYHOST, 0);
		$this->CI->curl->option(CURLOPT_HTTPHEADER, $header);
		$this->CI->curl->option(CURLOPT_ENCODING, "gzip");
		$this->CI->curl->option(CURLOPT_CUSTOMREQUEST, 'GET');
		$this->CI->curl->option(CURLOPT_URL, $url);
		$this->CI->curl->option(CURLOPT_RETURNTRANSFER, true);
		$this->CI->curl->option(CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $this->CI->curl->execute();
        return json_encode($this->CI->curl->info['http_code']); 
    }
    
    function check_target($id_product, $target){
        $this->CI->db->select('*');
        $this->CI->db->from('a__product');
        $this->CI->db->where('id', $id_product);
        $query = $this->CI->db->get();
        $result = $query->row();
        
        if($result->validate_ig != '' || $result->validate_ig != null){
            $asArr = explode( ',', $result->validate_ig);
            if ($result->validate_ig!=null) {
                foreach( $asArr as $val ){
                    $tmp = explode( ':', $val );
                    $finalArray[ $tmp[0] ] = $tmp[1];
                }
                
            }
            
            /*proses validate*/
            foreach($finalArray as $letter => $number) 
            {
                $url = $letter == 'url' ? '#'.$number.'#' : '';
                switch($letter) 
                {
					case 'url':					
					if(strlen($number)>0){						
						if (filter_var($target, FILTER_VALIDATE_URL) === false) {
							$json_data =  array(
								"result" => FALSE ,
								"message" => array('head'=> 'Failed', 'body'=> 'Data yang anda masukkan salah , harus berisi full link contoh : https://www.instagram.com/....'),
								"form_error" =>'',
								"redirect" => ''
							);
							print json_encode($json_data);
							die();
						}
					}

                    if(!preg_match($url, $target)){                        
                        $json_data =  array(
                            "result" => FALSE ,
                            "message" => array('head'=> 'Failed', 'body'=> 'Data yang anda masukkan salah , harus berisi full link '. $number),
                            "form_error" =>'',
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    }
                    break;
                    case 'username':
                    $tes_username = $this->tes_curl('https://www.instagram.com/'. $target);
                    if ($tes_username!=200) {
                        $json_data =  array(
                            "result" => FALSE ,
                            "message" => array('head'=> 'Failed', 'body'=> 'Data yang anda masukkan salah, harus di isi username instagram '),
                            "form_error" =>'',
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    }
                    break;
                    case 'available':
                    $tes_curl = $this->tes_curl($target);
                    if ($tes_curl!=200) {
                        $json_data =  array(
                            "result" => FALSE ,
                            "message" => array('head'=> 'Failed', 'body'=> 'Link yang anda inputkan tidak bisa di buka 404'),
                            "form_error" =>'',
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    }
                    break;
                }
            }
        }
	}
}
?>