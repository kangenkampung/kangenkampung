<?php
defined('BASEPATH') OR exit('No direct script access allowed');
##------------------------------------------------------ GLOBAL & AJAX ------------------------------------------------------##
$config['login']					=	array('flag_class' => TRUE, 'role'=> array('reguler', 'premium'), 'dept'=> array('all'));

/* GLOBAL */
$config['welcome']					=	array('flag_class' => TRUE, 'role'=> array('reguler', 'premium'), 'dept'=> array('all'));
$config['verify']					=	array('flag_class' => TRUE, 'role'=> array('reguler', 'premium'), 'dept'=> array('all'));
$config['post']						=	array('flag_class' => TRUE, 'role'=> array('reguler', 'premium'), 'dept'=> array('all'));

/* PRIVATE PREMIUM USER */
$config['profile']					=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['mutation']					=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['notif']					=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['order']					=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['payment_account']			=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['s_district']				=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['s_regencies']				=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['s_villages']				=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['ticket']					=	array('flag_class' => TRUE, 'role'=> array('premium'), 'dept'=> array('all'));
$config['payment']					=	array('flag_class' => FALSE);
$config['payment/upgrade']			=	array('role' => array('reguler'),'dept' => array('all'));
$config['payment/topup']			=	array('role' => array('premium'),'dept' => array('all'));
$config['payment/new_payment_acc']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/payment_history']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_list_payment']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_get_admin_method']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_get_member_method']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_get_admin_account']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_get_member_account']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_add_payment']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_add_upgrade']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_add_topup']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_konfirmasi_pembayaran']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_konfirmasi_topup']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_cancel_topup']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_cancel_upgrade']	=	array('role' => array('premium'), 'dept' => array('all'));
$config['payment/ajax_action_konfirmasi_auto']	=	array('role' => array('premium'), 'dept' => array('all'));