<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/*-------- GET DATA ARRAY RESELLER --------*/
if(!function_exists('array_data_member')) {
	function array_data_member($param) {
		$CI = &get_instance();
		$CI->load->database();

		$CI->db->select('*');
		$CI->db->from('m__member');
		$CI->db->where('id_member', $param);
		$result = $CI->db->get();
		return $result->row();
	}
}

/*-------- GET DATA ARRAY RESELLER --------*/
if(!function_exists('array_data_member_alamat')) {
	function array_data_member_alamat($param) {
		$CI = &get_instance();
		$CI->load->database();

		$CI->db->select('*');
		$CI->db->from('m__member');
		$CI->db->where('id_member', $param);
		$CI->db->where('status_alamat', 1);
		$result = $CI->db->get();
		return $result->row();
	}
}

/*-------- SET IDR NOMINAL --------*/
if(!function_exists('addNumber')) {
	function addNumber($number, $idr = true) {
		$CI = &get_instance();
		$CI->load->database();
		return 'Rp. '. str_replace(',','.', number_format($number));
	}
}





