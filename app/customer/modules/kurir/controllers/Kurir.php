<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurir extends MY_Controller {

	public function __construct() {
		$this->load->model('M_kurir');
	}

	public function get_provinci_ongkir(){
		$param = post('param');
		$get = $this->ongkir->get_kurir_data($param);
		echo $get;
	}


	public function get_ongkir(){
		$origin = 'origin='.post('origin');
		$origintype = '&originType=subdistrict';
		$destination = '&destination='.post('destination');
		$destinationtype = '&destinationType=subdistrict';
		$weight = '&weight='.post('weight');
		$kurir = '&courier='.post('kurir');
		$diameter = '&diameter='.post('diameter');
		$get = $this->ongkir->cek_ongkir($origin.$origintype.$destination.$destinationtype.$weight.$kurir.$diameter);
		echo $get;
	}


	}


?>