<?php if (!$this->agent->is_mobile()) {?>
<div class="content">
        <div class="container" style="display: none;margin-top: 13%">
            <!-- jika kosong -->
            <div class="card cart-kosong">
                
            </div>
        </div>
        <div class="container"  style="margin-top: 13%">
        <div class="row">
            <div class="col-sm-8">
                <div class="card-cart-one">
                    <div class="cart-price-header">
                        <div class="cart-price-header-text">Keranjang Belanja</div>
                    </div>
                
                <?php  foreach ($product_cart as $cart) {?>
                <div class="row cart-item">
                   
                    <div class="col-8">
                     <a href="<?php echo base_url().index_page();?>products/<?= $cart['id_produk'] ?>" style="text-decoration:none">
                        <div class="row" style="text-align: left">
                            <div class="col-2 hide-mobile" > 
                            <div>
                                        <img src="<?php echo base_url();?>/uploads/product/<?= $cart['name_file'] ?>" class="cart-image-item">
                                    </div>
                            </div>
                            <div class="col-10">
                                <label style="font-weight: normal;color: black" class="cart-text-name"><?= $cart['name_product']?></label>
                                <div style="font-weight: normal;margin-top: -10px" class="primary-color"><?= addNumber($cart['subtotal'],true)?></div>
                            </div>
                            </div>
                        </a>
                        </div>
                    <div class="col-4" style="text-align: right;">
                            <div class="row">
                                <div class="col-sm-12" >
                                <label><i class="fa fa-trash fa-lg navbar-pointer" aria-hidden="true" style="color: red;font-size: 30px;padding-right: 12px" onclick="fungsi_hapus('<?= $cart['id_cart'] ?>')"></i></label>
                                <label><i class="fa fa-minus-circle fa-lg navbar-pointer" style="color: #03ac0e;font-size: 30px" aria-hidden="true" onclick="kurang('<?= $cart['id_cart']?>','<?= $cart['qty']?>')"></i></label>
                                <label style="text-decoration: underline;font-size: 22px"><?= $cart['qty']?></label>
                                <label><i class="fa fa-plus-circle fa-lg  navbar-pointer" style="color: #03ac0e;font-size: 30px" aria-hidden="true" onclick="tambah('<?= $cart['id_cart']?>','<?= $cart['qty']?>','<?= $cart['stok_product']?>')"></i></label> 
                                </div>
                            </div>
                    </div>
                </div>
                <hr>
                <?php }?>
            </div>
            </div>

            <div class="col-sm-4">
            <div class="bottom-navigation-mobile" style="margin-top: 8%"></div>
                <div class="card-cart-two">
                    <div class="cart-price-header">
                        <div class="cart-price-header-text">Ringkasan Belanja</div>
                    </div>
                    <div class="card-price-cart">
                    <div class="row">
                        <div class="col-6 cart-title-price-text" >Total Harga</div>
                        <div class="col-6" style="text-align: right;font-weight: bold;"><?= addNumber($total)?></div>
                    </div>
                    </div>
                        <div>
                        <a href="<?php echo base_url().index_page(); ?>checkout"><button class="btn btn-outline-success" style="margin-top: 8px;width: 100%">Beli</button></a>
                    </div>
                </div>
                <div class="bottom-navigation-mobile" style="margin-top: 22%"></div>
            </div>
        </div>
        </div>
    </div>  
  <?php }else{?>
    <div class=""  style="margin-top: 23%;"> 
    <div class="row">
            <div class="col-sm-12">
                <div class="container">
                
               <?php  foreach ($product_cart as $cart) {?>
                <div class="row cart-item">
                    <div class="col-6">
                        <div class="row" style="text-align: left">
                            <div class="col-12">
                            <a href="<?php echo base_url().index_page();?>products/<?= $cart['id_produk'] ?>" style="text-decoration:none">
                                <label style="font-weight: normal;color: black" class="cart-text-name"><?= $cart['name_product']?></label>
                                <div style="font-weight: normal;margin-top: -10px" class="primary-color"><?= addNumber($cart['subtotal'],true)?></div>
                                </a>
                            </div>
                            </div>
                        </div>
                    <div class="col-6" style="text-align: left;">
                            <div class="row">
                                <div class="col-sm-12" >
                                <label><i class="fa fa-trash fa-lg navbar-pointer" aria-hidden="true" style="color: red;font-size: 30px;padding-right: 12px" onclick="fungsi_hapus('<?= $cart['id_cart'] ?>')"></i></label>
                                <label><i class="fa fa-minus-circle fa-lg navbar-pointer" style="color: #03ac0e;font-size: 30px" aria-hidden="true" onclick="kurang('<?= $cart['id_cart']?>','<?= $cart['qty']?>')"></i></label>
                                <label style="text-decoration: underline;font-size: 22px"><?= $cart['qty']?></label>
                                <label><i class="fa fa-plus-circle fa-lg  navbar-pointer" style="color: #03ac0e;font-size: 30px" aria-hidden="true" onclick="tambah('<?= $cart['id_cart']?>','<?= $cart['qty']?>','<?= $cart['stok_product']?>')"></i></label> 
                                </div>
                            </div>
                    </div>
                </div>
                <hr>
                <?php }?>
            </div>
            </div>
            </div>
            </div>

        <nav class="fixed-bottom" style="">
            <div class="navbar-body-bottom-keranjang">
                <div class="row">
                        <div class="col-8" style="padding-top: 8px;padding-left: 12%">
                            <div style="font-size: 11px">Total Harga</div>
                            <div style="font-size: 25px"><?= addNumber($total)?></div>
                        </div>
                        <div class="col-4" style="padding-top: 20px;padding-right:19%">
                         <a href="<?php echo base_url().index_page();?>checkout" style="text-decoration:none">
                            <button class="btn btn-success" data-toggle="modal" data-target="#pembayaran-modal" data-backdrop="static">
                                    BAYAR
                            </button>
                            </a>
                        </div>
                </div>
            </div>
        </nav>
   


            

  <?php } ?>
             <?= $this->load->view('js/js')?>