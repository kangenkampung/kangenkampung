<script type="text/javascript">
    $(document).ready(function() {
       $('#page-load').hide();
    });
</script>

<script type="text/javascript">
	function kurang(id_cart,qty_kurang){
		 if(parseInt(qty_kurang) <= 1){
		 	hapus_keranjang("Mohon Maaf", "Product akan di hapus dari keranjang karna jumlah pesanan 0", "info","cart-min",1000,id_cart);
		 }else{
		  var form_data = new FormData();
	      form_data.append('id_cart', id_cart);
	      form_data.append('qty_kurang', qty_kurang);
	      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
	      addItemSerializecart("<?php echo base_url().$this->config->item('index_page'); ?>/keranjang/ajax_qty_kurang/","POST",form_data);
	      // location.reload();
		 }
	     
	}

	function tambah(id_cart,qty_tambah,stok){
		if(parseInt(stok) <= 0){
			message("Mohon Maaf", "Stok saat ini tidak tersedia", "info","info",1000);
		}else{
		  var form_data = new FormData();
	      form_data.append('id_cart', id_cart);
	      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
	      addItemSerializecart("<?php echo base_url().$this->config->item('index_page'); ?>/keranjang/ajax_qty_tambah	/","POST",form_data);
	      // location.reload();
		}

	}

	function fungsi_hapus(id_cart){
		hapus_keranjang("Mohon Maaf", "Product akan di hapus dari keranjang anda", "info","cart-min",1000,id_cart);
	}

	function hapus(id_cart){
		  var form_data = new FormData();
	      form_data.append('id_cart', id_cart);
	      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
	      addItemSerializecart("<?php echo base_url().$this->config->item('index_page'); ?>/keranjang/ajax_action_delete_cart/","POST",form_data);
	      // location.reload();
	}
</script>