<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keranjang extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_keranjang');
    }
    
    
    public function index()
    {
        $data['content'] = 'keranjang';
        $joins_product        = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product ',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail c',
                'condition' => 'b.id_product = c.id_product ',
                'jointype' => ''
            ),
             array(
                'table' => 'mr__file d',
                'condition' => 'd.id_relation = b.id_product  AND d.position_file = 1 ',
                'jointype' => ''
            )
        );
        $data['product_cart'] = $this->M_keranjang->fetch_joins('m__cart a', 'a.*,b.*,c.*,d.name_file', $joins_product, 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', TRUE);
        $data['total'] = $this->M_keranjang->get_sum_cart($this->session->userdata('id'));
        
        $this->load->view('template', $data);
    }
    
    public function ajax_qty_kurang()
    {
        $id_cart        = post('id_cart');
        $cart           = $this->M_keranjang->get_row('*', 'm__cart', 'id_cart = ' . "'" . $id_cart . "'", '', '', TRUE);
        $product        = $this->M_keranjang->get_row('*', 'mr__product_detail', 'id_product = ' . "'" . $cart['id_produk'] . "'", '', '', TRUE);
        $qty_all        = $cart['qty'] - 1;
        $data_edit_cart = array(
            "qty" => $qty_all,
            "subtotal" => $qty_all*$cart['price']
        );
        $cart_edit      = $this->M_keranjang->update_table('m__cart', $data_edit_cart, 'id_cart', $id_cart);
        if ($cart_edit) {
            $data_edit = array(
                "stok_product" => $product['stok_product'] + $qty_all
            );
            $this->M_keranjang->update_table('mr__product_detail', $data_edit, 'id_product', $cart['id_produk']);
        } else {
        }    
    }
    
    public function ajax_qty_tambah()
    {
    	$id_cart        = post('id_cart');
        $cart           = $this->M_keranjang->get_row('*', 'm__cart', 'id_cart = ' . "'" . $id_cart . "'", '', '', TRUE);
        $product        = $this->M_keranjang->get_row('*', 'mr__product_detail', 'id_product = ' . "'" . $cart['id_produk'] . "'", '', '', TRUE);
        $qty_all        = $cart['qty'] + 1;
        $data_edit_cart = array(
            "qty" => $qty_all,
            "subtotal" => $qty_all * $cart['price']
        );
        $cart_edit      = $this->M_keranjang->update_table('m__cart', $data_edit_cart, 'id_cart', $id_cart);
        if ($cart_edit) {
            $data_edit = array(
                "stok_product" => $product['stok_product'] - $qty_all
            );
            $this->M_keranjang->update_table('mr__product_detail', $data_edit, 'id_product', $cart['id_produk']);
        } else {
            // echo "error";
        }    
        
    }

    public function ajax_action_delete_cart(){
		$id_cart        = post('id_cart');
		$delete = $this->M_keranjang->delete_table("m__cart","id_cart",$id_cart);
		if($delete==FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
		}
	}

     
}
?>