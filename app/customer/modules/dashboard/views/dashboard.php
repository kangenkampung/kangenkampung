<div class="content close-overlay">
    <div id="overlay"></div>

 <div class="slick_banner" style="margin-left: 3%;margin-right: 3%;margin-top: 2%">
    <?php foreach ($banner as $baner_one) { ?>
      <img src="<?= base_url().'uploads/banner/';?><?= $baner_one->nama_file ?>" alt="OTHERS_PG_GROCERIES_CONTROL_SLOT4_ALLUSER_PG2_16OCT19" class="image-banner" style="margin-top:20px"/>
  <?php } ?>
 </div>



    <div class="content-body">
        <div style="margin-left: 3%;margin-right: 3%;margin-top: 2%">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card" style="border-radius: 10px;">
                        <div class="col-sm-6" style="margin-top: 3%">
                            <p class="title-kategory">Kategori Pilihan</p>
                        </div>
                        <div class="multiple-items row" style="margin: 20px">
          <?php $no=0 ;foreach ($category as $category_one) { ?>
                       <?php if (!$this->agent->is_mobile()) {?>  
                                                        <div class="border-category" style="padding: 10px;margin: 9px">
                                                            <div style="text-align: center;">
                                                                <img src="<?= base_url().'uploads/category/';?><?= $category_one->nama_file ?>" alt="" class="category-size">
                                                            </div>
                                                        </div>    
                          <?php }else{ ?>
                          <div>                 
                                                        <div class="border-category" >
                                                            <div style="text-align: center;">
                                                                <img src="<?= base_url().'uploads/category/';?><?= $category_one->nama_file ?>" alt="" class="category-size-mobile">
                                                            </div>
                                                        </div>                                          
                                                </div>  
                          <?php } ?>
          <?php } ?>
                       </div>
                            
                        </div>
                        </div>
                <div class="col-sm-6">
                    <div class="div-motto-image">
                        <center>
                            <img src="https://ecs7.tokopedia.net/img/content/register_new.png" class="motto-image">
                            <center>
                                <label class="text-register-image">Cari Barang Zaman Kecil Anda di sini </label>
                            </center>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <div  class="card-item-tranding-item">
            <div class="" style="margin-top: 2%">
                <div class="ml4percent mr4percent">
                    <div class="row">
                        <div class="col-8" style="margin-top: 3%">
                            <p class="title-kategory">Paling Sering Di Beli</p>
                        </div>
                        <div class="col-4 text-show-all primary-color text-show-all-item">
                            <p>Lihat Semua</p>
                        </div>

                    </div>
                </div>
                <!-- multuplie -->
                 <div class=" mr4percent multiple-items-tranding" style="margin-left: 22px">
                  <?php foreach ($product_tranding as $product_trandings) { ?>
                  <?php
                    $diskon = $product_trandings['diskon_product'] / 100 * $product_trandings['harga_product'];
                    $diskon_total =  $product_trandings['harga_product'] - $diskon;
                    if($product_trandings['diskon_product'] > 0 ){
                        $discount_title = "<div class='price-diskon'>".$product_trandings['diskon_product']."% OFF"."</div>";
                        $discount_price = "<span class='price-diskon-text'>".addNumber($product_trandings['harga_product'],true)."</span>";
                        $real_price     =    "<div style='' class='primary-color price-item'><b>".addNumber($diskon_total,true)."</b></div>";
                    }else{
                        $discount_price = "<span class=''></span>";
                        $discount_title =  "<div class=''></div>";
                        $real_price         =    "<div style='' class='primary-color price-item'><b>".addNumber($product_trandings['harga_product'],true)."</b></div>";
                    }
                  ?>
                 <div>
                <div class="  " style="margin-top: 2%;margin-left: 32px">
                     <div class="row">
                                            <div class="card-item">
                                                <div class="card">
                                                    <a href="<?php echo base_url().index_page(); ?>products/<?= $product_trandings['id_product'] ?>">
                                                        <div class="img-hover-zoom ">
                                                             <img src="<?= base_url().'uploads/product/';?><?= $product_trandings['name_file'] ?>" style="height: fit-content;max-width: 100%;height: 200px"> 
                                                            <div class="top-right">
                                                                <?= $discount_title; ?>
                                                            </div>
                                                        </div>
                                                        <div style="text-align: left;padding: 6px">
                                                            <div style="" class="title-item"><?= $product_trandings['name_product'] ?></div>
                                                            <?= $discount_price; ?>
                                                            <div style="" class="primary-color price-item"><b><?= $real_price ?></b></div>
                                                            <div style="" class="location-item"><?= $product_trandings['lokasi_product'] ?></div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                    </div>
                    </div>
                </div>
                 <?php } ?>
                </div>
                </div>
                <!-- End Tranding product -->
            </div>
        </div>
        <?php if ($this->agent->is_mobile()) {?>
        <div class="div-product-list ">
            <div class="card-item-box" style="margin-top: 3%">

                <div style="margin-top: 2%">
                    <div class="ml4percent mr4percent">
                        <div class="row">
                            <div class="col-sm-6" style="margin-top: 3%">
                                <p class="title-kategory">Rekomendasi Untuk kamu</p>
                            </div>
                        </div>
                    </div>
                    <!-- multuplie -->
                    <div class="ml4percent mr4percent">
                        <div class="row"  id="load_data">

                        </div>
                        <br>
                        <center>
                        <div id="load_data_message"></div>
                        </center>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <?php }else{?>
        <div class="">
            <div class="" style="margin-top: 3%">

                <div style="margin-top: 2%">
                    <div class="ml4percent mr4percent">
                        <div class="row">
                            <div class="col-sm-6" style="margin-top: 3%">
                                <p class="title-kategory">Rekomendasi Untuk kamu</p>
                            </div>
                        </div>
                    </div>
                    <!-- multuplie -->
                    <div class="ml4percent mr4percent">
                        <div class="row"  id="load_data">

                        </div>
                        <br>
                        <center>
                        <div id="load_data_message"></div>
                        </center>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <?php } ?>
    </div>

    <?= $this->load->view('js/js') ?>