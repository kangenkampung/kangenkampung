<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_dashboard');
        $this->view_primary = 'dashboard';
    }
    
    
    public function index()
    {
        $data['content']    = 'dashboard';
        $joins              = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            )
        );
        $data['banner']     = $this->M_dashboard->fetch_joins('a__banner a', '*', $joins, '', '', TRUE);
        $data['banner_one'] = $this->M_dashboard->fetch_joins('a__banner a', '*', $joins, '', '', TRUE);
        
        $joins_category   = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            )
        );
        $data['category'] = $this->M_dashboard->fetch_joins('a__category a', '*', $joins_category, '', '', TRUE);
        
        $joins_product            = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            )
        );
        $data['product_tranding'] = $this->M_dashboard->fetch_joins('mr__product a', 'a.*,b.*,c.*', $joins_product, 'a.status_product != 3', TRUE);
        $data['count_cart'] = $this->M_dashboard->count_all('m__cart', 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1','');
        
        $this->load->view('template', $data);
    }
    
    
    function ajax_action_get_banner()
    {
        $joins = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            )
        );
        $data  = $this->M_dashboard->fetch_joins('a__banner a', '*', $joins, '', '', TRUE);
        
        if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
    
    
    
    function ajax_action_get_category()
    {
        $joins = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            )
        );
        $data  = $this->M_dashboard->fetch_joins('a__category a', '*', $joins, '', TRUE);
        
        if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

  //     public function get_data_product_by_all(){
  //       $id= $this->session->userdata('id');
		// $joins = array(
		// 	array(
		// 		'table' => 'mr__product_detail b',
		// 		'condition' => 'a.id_product = b.id_product',
		// 		'jointype' => ''
		// 	),
  //           array(
  //               'table' => 'mr__file c',
  //               'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
  //               'jointype' => ''
  //           ),
		// );
		// $data['product'] = $this->M_dashboard->infinity($this->input->post('limit'), $this->input->post('start'))->result();
		// if(count($data) == 0){
		// 	$json_data =  array(
		// 		"result" => FALSE ,
		// 		"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
		// 		"form_error" => '',
		// 		"redirect" => ''
		// 	);
		// 	print json_encode($json_data);
		// 	die();
		// }else{
		// 	$json_data =  array(
		// 		"result" => TRUE,
		// 		"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
		// 		"form_error" => '',
		// 		"redirect" => '',
		// 		"data" => $data
		// 	);
		// 	print json_encode($json_data);
		// }
  //   }

    public function get_data_product(){
        $data= $this->M_dashboard->fetch_joins('mr__product a', '*','', 'a.status_product != 3', TRUE);
        print json_encode($data);
        
    }

      

    public function get_data_product_by_all()
    {
    	$output = '';
        $data = $this->M_dashboard->infinity($this->input->post('limit'), $this->input->post('start'));
        // echo $this->db->last_query();
        // die();
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $row) {
            	 $diskon = $row->diskon_product / 100 * $row->harga_product;
                    $diskon_total =  $row->harga_product - $diskon;
                    if($row->diskon_product > 0 ){
                        $discount_title = "<div class='price-diskon'>".$row->diskon_product."% OFF"."</div>";
                        $discount_price = "<span class='price-diskon-text'>".addNumber($row->harga_product,true)."</span>";
                        $real_price     =    "<div style='' class='primary-color price-item'><b>".addNumber($diskon_total,true)."</b></div>";
                    }else{
                        $discount_price = "<span class=''></span>";
                        $discount_title =  "<div class=''></div>";
                        $real_price         =    "<div style='' class='primary-color price-item'><b>".addNumber($row->harga_product,true)."</b></div>";
                    }
                    $output .= '<div class="card-item">
									 <div class="card">
                                        <a href="'.base_url().index_page()."products/".$row->id_product.'">
                                            <div class="img-hover-zoom ">
                                                <img src="'.base_url().'uploads/product/'.$row->name_file.'" style="height: fit-content;max-width: 100%;height: 200px"> 
                                                <div class="top-right">
                                                    '.$discount_title.'
                                                </div>

                                            </div>
                                            <div style="text-align: left;padding: 6px">
                                                <div style="" class="title-item">' . $row->name_product . '</div>
                                                '.$discount_price.'
                                                <div style="" class="primary-color price-item"><b>'.$real_price.'</b></div>
                                                <div style="" class="location-item">'.$row->lokasi_product.'</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>';
            }
        }
        echo $output;
    }

        public function ajax_action_login(){
        $this->form_validation->set_rules('email_member', 'email_member',  'trim|required');
        $this->form_validation->set_rules('password_member', 'password_member', 'trim|required');

        if($this->form_validation->run() == FALSE){
            $form_error = $this->form_validation->error_array();
            $response =  array(
                'result' => FALSE,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan data terisi semua.'),
                'form_error' => $form_error,
            );
            echo json_encode($response, JSON_PRETTY_PRINT);
            die();
        }else{
            $email_member = post('email_member');
            $password_member = md5(post('password_member'));




            $dataarr = array(
                'email_member' => $email_member,
                'password_member' => $password_member
            );

            $login = $this->db->get_where('m__member', $dataarr);
            if($login->num_rows() > 0){
                $sess = $login->row_array();
                
            
                $data = array(
                    'id' => $sess['id_member']
                );

                $this->session->set_userdata($data);

                $data = array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Selamat anda telah masuk di kangenkampung'),
                    "form_error" => '',
                    "redirect" => ''
                );
                echo json_encode($data);
                die();
            }else{
                $data = array(
                    "result" => FALSE,
                    "message" => array('head'=> 'failed', 'body'=> 'Pastikan email dan password anda benar'),
                    "form_error" => '',
                    "redirect" => ''
                );
                echo json_encode($data);
                die();
            }
        }
    }

    function ajax_action_logout(){
        $this->session->sess_destroy();
        $data = array(
            "result" => TRUE,
            "message" => array('head'=> 'Success', 'body'=> 'Anda telah keluar dai kangenkampung'),
            "form_error" => '',
            "redirect" => base_url().$this->config->item('index_page')
        );
        echo json_encode($data);
        die();
    }

}
?>