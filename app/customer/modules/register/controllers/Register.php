<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

	public function __construct() {
		$this->load->model('M_register');
	}


	public function index(){
	    $data = 'register';
		$this->load->view($data);
	}

	public function ajax_action_add_member(){
		$this->form_validation->set_rules('email_member', 'email_member', 'required');
		$this->form_validation->set_rules('nama_member', 'nama_member', 'required');
		$this->form_validation->set_rules('no_member', 'no_member', 'required');
        $this->form_validation->set_rules('password_member', 'password_member', 'required');
       if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$count_data = $this->M_register->get_count_data('m__member');
			$id_member = "K".date('his').$count_data;
			$data = array(
				"id_member" => $id_member,
				"no_member" => post("no_member"),
				"name_member" => post("nama_member"),
				"email_member" => post("email_member"),
				"password_member" => md5(post("password_member")),
				"status_member" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_register->insert_table("m__member",$data);
			if($add==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'dashboard'
				);
				print json_encode($json_data);
			}

		}
	}


}
?>