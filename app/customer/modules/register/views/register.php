<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customer/css/kangenkampung.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
</head>
<style type="text/css">
 .loader {
position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    color:darkred;
}
</style>
<div id="page-load" style="position: fixed; z-index: 999999; opacity: 0.7;  background: white; width: 100%; height: 100%; display: block;top: 0;  ">
    <div class="loader">
      <div>
        <img src="<?php echo base_url(); ?>/assets/loading.gif"></div>
      </div>
 </div>


<body class="body-register">
    <nav>
        <center>
            <div class="logo-register">
                <div class="text-logo" style="font-size: 34px">
                                    <label  style="font-weight: bold;color: grey">Jualin</label><label class="primary-color">Aja</label>
                 </div>
            </div>
        </center>
    </nav>
    <div class="content-daftar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card-cart-one">
                        <div style="text-align: center;margin-top: 2%">
                            <label style="font-size: 23px;font-weight: bold;">Daftar Sekarang</label>
                            <div style="color: grey">Sudah punya aku kangen.com ? <span class="primary-color navbar-pointer">Masuk</span></div>
                        </div>
                        <div style="padding: 5%">
                        <form action="">

                         <!-- email -->
                            <div>
                                <label style="font-size: 12px;color: grey">Nama</label>
                                <input type="text" name="" class="form-control" id="nama_member">
                                <label style="font-size: 11px;color: grey">Contoh: kangenkampung</label>
                            </div>


                            <!-- email -->
                            <div>
                                <label style="font-size: 12px;color: grey"> Email</label>
                                <input type="email" name="" class="form-control" id="email_member">
                                <label style="font-size: 11px;color: grey">Contoh: email@kangenkampung.com</label>
                            </div>

                            <!-- email -->
                            <div>
                                <label style="font-size: 12px;color: grey">Nomor Ponsel</label>
                                <input type="number" name="" class="form-control" id="no_member">
                                <label style="font-size: 11px;color: grey">Contoh: 0813456786</label>
                            </div>

                            <div>
                                <label style="font-size: 12px;color: grey">Password</label>
                                <input type="password" name="" class="form-control" id="password_member">
                            </div>

                            <!-- email -->
                            <div>
                                <div style="margin-top: 2%"></div>
                                <input type="submit" class="btn btn-success" value="Daftar" style="width: 100%">
                                <label style="font-size: 11px;color: grey">*Menyetujui Syarat Dan ketentuan</label>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 hide-mobile">
                    <div class="div-register-image">
                        <img src="https://ecs7.tokopedia.net/img/content/register_new.png" class="register-image">
                        <center>
                            <label class="text-register-image">Ingin Barang Zaman Kecil Anda </label>
                            <label style="font-weight: bold;color: black;font-size: 15px">Gabung dan rasakan transaksi di kangen.com</label>
                        </center>
                    </div>
                </div>
            </div>
            <div class="bottom-navigation-mobile" style="margin-top: 5%">
            </div>
        </div>
</body>

        
        <div class="modal fade" id="login-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered w-15">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="card-modal">
                    <div class="modal-body">
                        <div class="login-modal-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-size: 22px;font-weight: bold">Masuk</label>
                                            </div>
                                            <div class="col-6" style="text-align: right;">
                                                <label class="primary-color" style="font-size: 12px;font-weight: bold;" data-toggle="modal" data-target="#login-modal">Daftar</label>
                                            </div>

                                        </div>
                                    </div>
                                     <form action="" id="form_login">
                                    <div>
                                        <label style="font-size: 12px;">Email</label>
                                        <input type="text" name="" class="form-control" id="email_member">
                                        <label style="font-size: 11px;color: grey">Contoh: email@kangenkampung.com</label>
                                    </div>

                                     <div>
                                        <label style="font-size: 12px;">Password</label>
                                        <input type="password" name="" class="form-control" id="password_member">
                                    </div>
                                    <br>
                                    <div>
                                        <input type='submit' class="btn btn-success" style="width: 100%" value="Masuk">
                                    </div>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>




    <?= $this->load->view('js/js')?>
 <script src="<?php echo base_url(); ?>assets/admin/ajaxFunction.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/bootstrap.bundle.min.js "></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/perfect-scrollbar.min.js "></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/echarts.min.js "></script>

<script src="<?php echo base_url(); ?>assets/admin/js/es5/echart.options.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/es5/dashboard.v4.script.min.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/es5/script.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/es5/sidebar.large.script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/kangenkampung.js"></script>