<script type="text/javascript">
	$('#page-load').hide()
</script>

<script type="text/javascript">
	$("form").submit(function(e) {
    		e.preventDefault();
    		var form_data = new FormData();
			form_data.append('email_member', $('#email_member').val());
			form_data.append('nama_member', $('#nama_member').val());
			form_data.append('no_member', $('#no_member').val());
			form_data.append('password_member', $('#password_member').val());
			form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
			addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/register/ajax_action_add_member/","POST",form_data);
	});
	
</script>