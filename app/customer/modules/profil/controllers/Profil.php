<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_profil');
        $this->view_primary = 'profil';
    }
    
    
    public function index()
    {
        $data['content']    = 'profil';
        $data['alamat'] = $this->M_profil->fetch_joins('m__alamat', '*','', 'id_member ='."'".$this->session->userdata('id')."'", '', TRUE);
        $data['count_cart'] = $this->M_profil->count_all('m__cart', 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1','');
        $this->load->view('template', $data);
    }

        public function ajax_action_add_alamat(){
        $this->form_validation->set_rules('name_alamat', 'name_alamat', 'required');
        $this->form_validation->set_rules('name_penerima', 'name_penerima', 'required');
        $this->form_validation->set_rules('kota_alamat', 'kota_alamat', 'required');
        $this->form_validation->set_rules('kode_pos_alamat', 'kota_alamat', 'required');
        $this->form_validation->set_rules('alamat_lengkap', 'kota_alamat', 'required');
       if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $count_data = $this->M_profil->get_count_data('m__alamat');
            $id_alamat = "A".date('his').$count_data;
            $data = array(
                "id_alamat" => $id_alamat,
                "id_member" => $this->session->userdata('id'),
                "name_alamat" => post("name_alamat"),
                "name_penerima" => post("name_penerima"),
                "kota_alamat" => post("kota_alamat"),
                "id_kota_alamat" => post("id_kota_alamat"),
                "kecamatan_alamat" => post("kecamatan_alamat"),
                "id_kecamatan" => post("id_kecamatan"),
                "kode_pos_alamat" => post("kode_pos_alamat"),
                "alamat_lengkap" => post("alamat_lengkap"),
                "status_alamat" => post("status_alamat"),
                "created_at" => date("Y-m-d H:i:s")
            );
            $add = $this->M_profil->insert_table("m__alamat",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Alamat Anda telah di tambahkan'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'profil?link=alamat'
                );
                print json_encode($json_data);
            }

        }
    }

    function ajax_action_get_alamat(){
        $id = $this->uri->segment(3);
        $data = $this->M_profil->fetch_joins('m__alamat a', '*', '','id_alamat = \''. $id.'\'', '',TRUE);

        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    function ajax_action_edit_alamat(){
        $this->form_validation->set_rules('name_alamat', 'name_alamat', 'required');
        $this->form_validation->set_rules('name_penerima', 'name_penerima', 'required');
        $this->form_validation->set_rules('kota_alamat', 'kota_alamat', 'required');
        $this->form_validation->set_rules('kode_pos_alamat', 'kota_alamat', 'required');
        $this->form_validation->set_rules('alamat_lengkap', 'kota_alamat', 'required');
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "name_alamat" => post("name_alamat"),
                "name_penerima" => post("name_penerima"),
                "kota_alamat" => post("kota_alamat"),
                "kode_pos_alamat" => post("kode_pos_alamat"),
                "alamat_lengkap" => post("alamat_lengkap"),
                "status_alamat" => post("status_alamat"),
                "kota_alamat" => post("kota_alamat"),
                "id_kota_alamat" => post("id_kota_alamat"),
                "kecamatan_alamat" => post("kecamatan_alamat"),
                "id_kecamatan" => post("id_kecamatan"),
                "update_at" => date("Y-m-d H:i:s")
            );
            $edit = $this->M_profil->update_table("m__alamat",$data,"id_alamat",post("id_alamat"));
            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Merubah Alamat'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Merubah Alamat'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'profil?link=alamat'
                );
                print json_encode($json_data);
            }

        }
    }

    public function ajax_action_delete_alamat(){
        $id = $this->uri->segment(3);
        $delete = $this->M_profil->delete_table("m__alamat","id_alamat",$id);
        if($delete==FALSE){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Alamat'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus Alamat'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'profil?link=alamat'
            );
            print json_encode($json_data);
        }
    }


    
    

}
?>