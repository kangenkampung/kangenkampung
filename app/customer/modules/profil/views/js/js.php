<script type="text/javascript">
    $(document).ready(function() {
       $('#page-load').hide();
    });
</script>

<script type="text/javascript">
	
	function get_alamat(id){
		 var link = "<?php echo base_url().$this->config->item('index_page'); ?>profil/ajax_action_get_alamat/" + id;
         var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

        ajaxShowData(link, "POST", form_data, function(response) {
            $('#edit_name_alamat').val(response.data[0].name_alamat);
            $('#id_alamat').val(id);
            $('#edit_name_penerima').val(response.data[0].name_penerima);
            // $('#edit_kota_alamat').val(response.data[0].kota_alamat);
            $('#edit_kode_pos_alamat').val(response.data[0].kode_pos_alamat);
            $('#edit_alamat_lengkap').val(response.data[0].alamat_lengkap);
            $("#edit_kota_alamat").select2("val", response.data[0].id_kota_alamat);
            $("#edit_kecamatan_alamat").select2("val", response.data[0].id_kecamatan);
            $('#page-load').hide();
        });
        $('#edit-alamat-modal').modal("show");
        $('#page-load').hide();

    }
</script>
