<?php if (!$this->agent->is_mobile()) {?>
    <div style="">
        <!-- Pc View -->
        <?php 
        if($this->input->get('link') == "riwayat"){
                $active = 'active';
                $active2 = '';
                $active3 = '';
        }else if($this->input->get('link') == "alamat"){
                $active = '';
                $active2 = 'active';
                $active3 = '';
        }else{
                $active = '';
                $active2 = '';
                $active3 = 'active';
        }
    ?>
            <div class="container">
                <div style="margin-top: 15%"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <label style="font-weight: bold;color: black;"><i class="fa fa-user-o"></i>
                                <label style="padding-left: 12px"></label>
                                <?= array_data_member($this->session->userdata('id'))->name_member?>
                            </label>
                        </div>
                        <div class="card-tipis" style="background: white">
                            <div>
                                <div class="">

                                    <ul class="nav nav-tabs " role="tablist">
                                        <li class="nav-item navbar-pointer <?= $active3 ?>">
                                            <a class="nav-link  " href="<?= base_url().$this->config->item('index_page') ?>profil">
                                                <label class="navbar-pointer" style="font-weight: normal;color: grey;font-size: 14px">Biodata Diri </label>
                                            </a>
                                        </li>
                                        <li class="nav-item <?= $active2 ?>">
                                            <a class="nav-link navbar-pointer" href="<?= base_url().$this->config->item('index_page') ?>profil?link=alamat" >
                                                <label class="navbar-pointer" style="font-weight: normal;color: grey;font-size: 14px">Daftar Alamat</label>
                                            </a>
                                        </li>
                                        <li class="nav-item <?= $active ?>">
                                            <a class="nav-link navbar-pointer" href="#riwayat" role="tab" data-toggle="tab">
                                                <label class="navbar-pointer" style="font-weight: normal;color: grey;font-size: 14px">Riwayat Pembelian</label>
                                            </a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content" style="color: black">
                                        <div role="tabpanel" class="tab-pane <?= $active3 ?>" id="biodata">

                                            <div class="card-tipis">
                                                <div class="row">
                                                    <div class="col-sm-3">

                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div style="padding-top: 12px"></div>
                                                        <label style="font-weight: bold;">Biodata Diri</label>

                                                        <div>
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label style="font-weight: normal;">Nama</label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <label style="font-weight: normal;">
                                                                        <?= array_data_member($this->session->userdata('id'))->name_member?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label style="font-weight: normal;">Tanggal Lahir</label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <label style="font-weight: normal;">7 November 1999</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div style="padding-top: 12px"></div>
                                                        <label style="font-weight: bold;">Informasi Kontak</label>

                                                        <div>
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label style="font-weight: normal;">No Handphone</label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <label style="font-weight: normal;">
                                                                        <?= array_data_member($this->session->userdata('id'))->no_member?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label style="font-weight: normal;">Email</label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <label style="font-weight: normal;">
                                                                        <?= array_data_member($this->session->userdata('id'))->email_member?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div style="margin-top: 12px"></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane  <?= $active2 ?>" id="daftaralamat">
                                            <div style="padding: 2%">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label style="font-weight: bold;font-size: 26px">Daftar Alamat</label>
                                                        </div>
                                                        <div class="col-sm-6" style="text-align: right;">
                                                            <input type="button" name="" class="btn btn-success" value="Tambah Daftar Alamat" data-toggle="modal" data-target="#add-alamat-modal">

                                                        </div>
                                                    </div>
                                                    <label>Rekening Bank digunakan untuk proses penarikan Saldo Tokopedia. Anda hanya dapat menambahkan 3 rekening bank.</label>
                                                </div>

                                                <div style="padding-top: 2%"></div>
                                                <table class="table">
                                                    <tr>
                                                        <td>Penerima</td>
                                                        <td>Alamat Pengiriman</td>
                                                        <td>Daerah Pengiriman</td>
                                                        <td style="text-align: center;">Aksi</td>
                                                    </tr>
                                                    <tbody>
                                                        <?php foreach ($alamat as $alamats) { ?>
                                                            <tr>
                                                                <td>
                                                                    <?= $alamats->name_penerima?>
                                                                </td>
                                                                <td>
                                                                    <?= $alamats->alamat_lengkap?>
                                                                </td>
                                                                <td>
                                                                    <?= $alamats->kota_alamat." | ".$alamats->kode_pos_alamat ?>
                                                                </td>
                                                                <td>
                                                                    <div class="row">

                                                                        <div class="col-sm-6" style="text-align: right;">
                                                                            <button class="btn btn-danger btn-sm" onclick="ajaxItemDelete2('<?php echo base_url().$this->config->item('index_page'); ?>','profil/ajax_action_delete_alamat','<?= $alamats->id_alamat?>')">Hapus</button>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <button class="btn btn-success btn-sm" onclick="get_alamat('<?= $alamats->id_alamat?>')">Ubah</button>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade <?= $active ?>" id="riwayat">
                                            <div>
                                                <div style="padding: 2%">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label style="font-weight: bold;font-size: 26px">Riwayat Pembelian</label>
                                                            </div>
                                                        </div>
                                                        <label>Riwayat Pembelian Anda.</label>
                                                    </div>

                                                    <div style="padding-top: 2%"></div>
                                                    <table class="table">
                                                        <tr>
                                                            <td>Penerima</td>
                                                            <td>Alamat Pengiriman</td>
                                                            <td>Daerah Pengiriman</td>
                                                            <td>Aksi</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
    <?php }else{?>
        <div style="margin-top: 25%"></div>
        <div class="container">
            <div>
                <div class="col-12">
                    <div class="card-tipis" style="background: white;padding: 12px">
                        <label style="font-weight: bold">Biodata</label>

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <label style="font-weight: normal;">Nama</label>
                                </div>
                                <div class="col-9">
                                    <label style="font-weight: normal;">
                                        <?= array_data_member($this->session->userdata('id'))->name_member?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <label style="font-weight: normal;">Tanggal</label>
                                </div>
                                <div class="col-9">
                                    <label style="font-weight: normal;">7 November 1999</label>
                                </div>
                            </div>
                        </div>
                        <label style="font-weight: bold">Informasi Kontak</label>

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <label style="font-weight: normal;">No </label>
                                </div>
                                <div class="col-9">
                                    <label style="font-weight: normal;">
                                        <?= array_data_member($this->session->userdata('id'))->no_member?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <label style="font-weight: normal;">Email</label>
                                </div>
                                <div class="col-9">
                                    <label style="font-weight: normal;">
                                        <?= array_data_member($this->session->userdata('id'))->email_member?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div>
                <div class="col-12">
                 <div class="card-tipis" style="background: white;padding: 12px">
                        <div class="row">
                            <div class="col-6">
                                 <label style="font-weight: bold;">Daftar Alamat</label>
                            </div>
                            <div class="col-6" style="text-align: right;padding-right: 25px">
                                <i class="fa fa-plus-circle fa-lg primary-color" aria-hidden="true" data-toggle="modal" data-target="#add-alamat-modal"></i>
                            </div>
                        </div>
                      
                       <hr>
                        <?php foreach ($alamat as $alamats) { ?>
                        <div>
                             <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <label style="font-weight: normal;">Nama Penerima</label>
                                        </div>
                                         <div class="col-6" style="text-align: right;">
                                            <label style="padding-right: 12px" onclick="ajaxItemDelete('<?php echo base_url().$this->config->item('index_page'); ?>','profil/ajax_action_delete_alamat','<?= $alamats->id_alamat?>')"><i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i></label>
                                            <label onclick="get_alamat('<?= $alamats->id_alamat?>')"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" ></i></label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                          <?= $alamats->name_penerima?>
                                    </label>
                                </div>
                            </div>


                             <div class="row">
                                <div class="col-12">
                                    <label style="font-weight: normal;">Daerah Pengiriman</label>
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                           <?= $alamats->kota_alamat." | ".$alamats->kode_pos_alamat ?>
                                    </label>
                                </div>
                            </div>

                              <div class="row">
                                <div class="col-12">
                                    <label style="font-weight: normal;">Alamat Lengkap</label>
                                </div>
                                <div class="col-12">
                                    <label style="font-weight: normal;">
                                              <?= $alamats->alamat_lengkap?>
                                    </label>
                                </div>
                            </div>
 <hr>
 <?php } ?>
                        </div>
                       
                 </div>
            </div>
            </div>
        </div>
        <?php } ?>

            <?= $this->load->view('js/js')?>