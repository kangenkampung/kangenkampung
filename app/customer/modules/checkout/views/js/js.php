<script type="text/javascript">
    $(document).ready(function() {
       $('#page-load').hide();
       get_cart_data();
       ajax_get_alamat();
       get_payment_account();
    });
</script>

<script type="text/javascript">
    function ajax_get_alamat() {
      var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/ajax_get_alamat";
     var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
         if(response.result){
            if(response.active != 1){
                $('.close').hide();
                $('#alamat-modal').modal({
                backdrop: 'static'
            })         
            }else if(response.active != 1){
              $('.close').hide();
                $('#alamat-modal').modal({
                backdrop: 'static'
                 })   
            }
         }else{
          $('.close').hide();
          $('.ganti_alamat').hide();
          $('#add-alamat-modal').modal({
                backdrop: 'static'
            })
         }
    });

}
</script>

<script type="text/javascript">
    function ajax_get_pemesanan() {
      var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_pemesanan";
     var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
         if(response.result){
            if(response.active != 1){
                $('.close').hide();
                $('#alamat-modal').modal({
                backdrop: 'static'
            })         
            }else if(response.active != 1){
              $('.close').hide();
                $('#alamat-modal').modal({
                backdrop: 'static'
                 })   
            }
         }else{
          $('.close').hide();
          $('.ganti_alamat').hide();
          $('#add-alamat-modal').modal({
                backdrop: 'static'
            })
         }
    });

}
</script>


<script type="text/javascript">
      function get_cart_data(){
        var html = '';
        var berat = 0;
        var kurir_price = 0;
        var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_cart_data";
        $.getJSON(link2, function(data) {
        $.each(data.data,function(i,v){
              kurir_price += parseInt(v.kurir_price);
            $.each(v.items,function(b,item){
                berat += parseInt(item.berat_product);
               if(item.urut == 0){
                   
                    <?php if (!$this->agent->is_mobile()) {?>

                           if( v.name_kurir != ""){
                                var header = '<div class="cart-price-header">'+
                                   '<div class="row">'+
                                '<div class="cart-price-header-text col-10">'+item.name_merchant+'<br>'+"<div class='primary-color' >"+v.name_kurir+'</div>'+'</div>'+
                                '<div style="text-align:right;margin-top:12px" class="col-2" >'+
                                '<button class="btn btn-success btn-sm" onclick="select('+i+')"><i class="fa fa-truck"></i></button>'+
                                '</div>'+
                                '</div>'+
                            '</div>';
                            var style = 'display:none';
                            }else{
                                 var header = '<div class="cart-price-header">'+
                                '<div class="cart-price-header-text">'+item.name_merchant+'</div>'+
                            '</div>';
                             var style =  '';
                            }


                    datatext = header+
                           '<div class="row">'+
                           '<div class="col-6">'+
                               '<a href="<?php echo base_url().index_page();?>products/'+item.id_produk+'" style="text-decoration:none">'+
                                   ' <div class="row" style="text-align: left">'+
                                       ' <div class="col-3 hide-mobile">'+
                                           '<div>'+
                                                '<img src="<?php echo base_url();?>/uploads/product/'+item.name_file+'" class="cart-image-item">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-9">'+
                                            '<label style="font-weight: normal;color: black" class="cart-text-name">'+
                                             item.name_product+
                                            '</label>'+
                                            '<div style="font-weight: normal;margin-top: -10px" class="primary-color">'+
                                             addNumber(item.price,true)+
                                            '</div>'+
                                       ' </div>'+
                                    '</div>'+
                                '</a>'+
                               
                            '</div>'+
                            '<div class="col-6" style="'+style+'"  id="select'+i+'">'+
                            '<div class="row">'+
                                '<div class="col-sm-5">'+
                                '<label> Pilih Kurir</label>'+
                                '<select class="form-control"  id="kurir'+i+'" onchange="check_ongkir('+item.id_merchant.replace(/[^0-9]/gi, '')+','+item.id_kecamatan+','+berat+','+i+','+i+')">'+
                                    '<option value="">Pilih Kurir</option>'+
                                    '<option value="jne">JNE</option>'+
                                    '<option value="pos">POS</option>'+
                                    '<option value="jnt">J&T</option>'+
                                '</select>'+
                                '</div>'+
                                 '<div class="col-sm-7">'+
                                '<div style="margin-top: -1%">'+
                                '<label> Pilih Layanan</label>'+
                                '<select class="form-control" id="layanan'+i+'">'+
                                '</select>'+
                                '</div>'+
                                '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                             '<hr>'+
                        '</div>';
                        <?php }else{?>
                             if( v.name_kurir != ""){
                              var style = 'display:none';
                                var headers = '<div class="cart-price-header" >'+
                                '<div class="row">'+
                                '<div class="cart-price-header-text col-10">'+item.name_merchant+'<br>'+"<div class='primary-color' style='font-size: 12px' >"+v.name_kurir+'</div>'+'</div>'+
                                '<div style="text-align:right;margin-top:12px" class="col-2" >'+
                                '<button class="btn btn-success btn-sm" onclick="select('+i+')"><i class="fa fa-truck"></i></button>'+
                                '</div>'+
                                '</div>'+
                                '</div>';
                            }else{
                                 var headers = '<div class="cart-price-header">'+
                                '<div class="cart-price-header-text">'+item.name_merchant+'</div>'+
                            '</div>';
                            var style = '';
                            }

                           

                            datatext =headers+
                             '<div class="row" style="'+style+'"  id="select'+i+'">'+
                                '<div class="col-6">'+
                                '<label> Pilih Kurir</label>'+
                                '<select class="form-control"  id="kurir'+i+'" onchange="check_ongkir('+item.id_merchant.replace(/[^0-9]/gi, '')+','+item.id_kecamatan+','+berat+','+i+','+i+')">'+
                                    '<option value="">Pilih Kurir</option>'+
                                    '<option value="jne">JNE</option>'+
                                    '<option value="pos">POS</option>'+
                                '</select>'+
                                '</div>'+
                                 '<div class="col-6">'+
                                '<div style="margin-top: -1%">'+
                                '<label> Pilih Layanan</label>'+
                               '<select class="form-control" id="layanan'+i+'">'+
                                '</select>'+
                                '</div>'+
                             '</div>'+
                            '</div>'+
                           '<div class="row" style="margin-top: 7%">'+
                           '<div class="col-12">'+
                               '<a href="<?php echo base_url().index_page();?>products/'+item.id_produk+'" style="text-decoration:none">'+
                                   ' <div class="row" style="text-align: left">'+
                                       ' <div class="col-3 ">'+
                                           '<div>'+
                                                '<img src="<?php echo base_url();?>/uploads/product/'+item.name_file+'" class="cart-image-item">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-9">'+
                                            '<label style="font-weight: normal;color: black" class="cart-text-name">'+
                                             item.name_product+
                                            '</label>'+
                                            '<div style="font-weight: normal;margin-top: -10px" class="primary-color">'+
                                             addNumber(item.price,true)+
                                            '</div>'+
                                       ' </div>'+
                                    '</div>'+
                                '</a>'+
                                '<hr>'+
                            '</div>'+
                         
                            '</div>'+
                            '</div>'+
                            '</div>'+

                        '</div>';
                        <?php } ?> 
                // }else if(){

                }else{
                       <?php if (!$this->agent->is_mobile()) {?>
                    datatext =  '<div class="row">'+
                           '<div class="col-6">'+
                               '<a href="<?php echo base_url().index_page();?>products/'+item.id_produk+'" style="text-decoration:none">'+
                                   ' <div class="row" style="text-align: left">'+
                                       ' <div class="col-3 hide-mobile">'+
                                           '<div>'+
                                                '<img src="<?php echo base_url();?>/uploads/product/'+item.name_file+'" class="cart-image-item">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-9">'+
                                            '<label style="font-weight: normal;color: black" class="cart-text-name">'+
                                             item.name_product+
                                            '</label>'+
                                            '<div style="font-weight: normal;margin-top: -10px" class="primary-color">'+
                                             addNumber(item.price,true)+
                                            '</div>'+
                                       ' </div>'+
                                    '</div>'+
                                '</a>'+
                            '</div>'+
                        '</div>'+
                        '<hr>';  
                          <?php }else{ ?>
                    datatext =  '<div class="row">'+
                          '<div class="col-12">'+
                               '<a href="<?php echo base_url().index_page();?>products/'+item.id_produk+'" style="text-decoration:none">'+
                                   ' <div class="row" style="text-align: left">'+
                                       ' <div class="col-3 ">'+
                                           '<div>'+
                                                '<img src="<?php echo base_url();?>/uploads/product/'+item.name_file+'" class="cart-image-item">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-9">'+
                                            '<label style="font-weight: normal;color: black" class="cart-text-name">'+
                                             item.name_product+
                                            '</label>'+
                                            '<div style="font-weight: normal;margin-top: -10px" class="primary-color">'+
                                             addNumber(item.price,true)+
                                            '</div>'+
                                       ' </div>'+
                                    '</div>'+
                                '</a>'+
                                '<hr>'+
                            '</div>'+
                        '</div>'; 
                            <?php } ?>  

                }
                html += datatext; 
                
            })           
        });
        $('#cart_preview').html(html);
        var all_price  = parseInt(kurir_price+<?= $total ?>);
        $('.ongkir_text').text(addNumber(kurir_price,true));
        $('.total_price').text(addNumber(all_price,true));
})

  }
</script>
<script type="text/javascript">
    
</script>
<script type="text/javascript">

    $('#jasa_pengiriman').on('change', function() {
           check_ongkir(this.value);
    });



    function check_ongkir(merchant,origin,berat,id,jasas) {
    $('#page-load').show();
    var berat = berat;
    var destination = $('#id_kecamatan').val();
    var origin = origin;
    var jasa = $("#kurir"+jasas).val();
    var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    if (jasa != "pos") {
        var text = "Hari";
    } else {
        var text = "";
    }
    form_data.append('weight', berat);
    form_data.append('destination', destination);
    form_data.append('kurir', jasa);
    form_data.append('origin', origin);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
         var html = '<option value="" selected="selected">Pilih </option>';
        var i;
        try {
            var data = response.rajaongkir.results[0].costs;
            // alert(data.length);
            if(data.length != 0){
              for (i = 0; i < response.rajaongkir.results[0].costs.length; i++) {
                 html += "<option value='"+data[i].cost[0].value+"'>"+response.rajaongkir.results[0].code.toUpperCase()+"("+data[i].service+")|"+data[i].cost[0].etd+" "+text+"|"+addNumber(data[i].cost[0].value,true)+"</option>";
              }
            }else{
               message("Mohon maaf", "Saat ini Jasa  tidak tersedia di kota ini", "error", "info", 3000);
            }
            $('#preview_ongkir').html(html);
        } catch (err) {
              message("Mohon maaf", "Saat ini Jasa  Masih ada perbaikan", "error", "info", 3000);
        }
         $('#page-load').hide();
         $('#layanan'+id).html(html);

        $('#layanan'+id).on('change', function() {
            // alert(merchant);
           simpan_kurir(merchant,this.value,jasa,$(this).find("option:selected").text());
        });

       
    });

}

function simpan_kurir(id_merchant,price,jasa,text){
     var form_data = new FormData();
      form_data.append('id_merchant', id_merchant);
      form_data.append('value', text);
      form_data.append('jasa', jasa);
      form_data.append('price', price);
      form_data.append('id_alamat', $('#id_alamat').val());
      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/checkout/simpan_kurir/","POST",form_data);
}

function select(i){
    $('#select'+i).toggle(500);
}
</script>

<script type="text/javascript">
  function get_payment_account(){
    var html = '';
    var trHTML = ''; 
    var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>pembayaran/get_admin_account";
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
           var data = response.data;
            for (var i = 0 ; i < data.length; i++) {
                 var datas = '<div class="card-metode-pembayaran">'+
                                            '<div class="row" style="margin-top: 13px;margin-left: 7px;margin-bottom: 2px">'+
                                                '<div class="col-2" style="margin-top: 12px">'+
                                                    '<img src="<?= base_url().'uploads/payment_method/';?>'+data[i].nama_file+'" style="width: 60px">'+
                                                '</div>'+

                                                '<div class="col-6" style="margin-left: 19px">'+
                                                    '<p style="font-weight:600;font-size: 14px;margin-top: 2px;">'+data[i].name_payment_method+'</p>'+
                                                    '<p style="font-size: 13px;margin-top: -12px">'+data[i].code_payment_method+'</p>'+
                                                '</div>'+
                                                '<div class="col-3" style="margin-top: 12px">'+
                                                    '<button class="btn btn-outline-success btn-sm next-payment" onclick="save_checkout('+data[i].id_payment_account+')"><i class="fa fa-arrow-right fa-md" aria-hidden="true"></i></button>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>';
                trHTML += datas;
              }
              $('#list_payment').html(trHTML);  
    });
  }
</script>

<script type="text/javascript">
  function save_checkout(id_payment_account){
      var form_data = new FormData();
      form_data.append('id_payment_account', id_payment_account);
      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/checkout/ajax_action_checkout/","POST",form_data);
  }
</script>