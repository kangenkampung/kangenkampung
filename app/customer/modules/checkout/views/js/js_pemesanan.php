<script type="text/javascript">
    $(document).ready(function() {
       $('#page-load').hide();
       ajax_get_pemesanan();
       
    });
</script>



<script type="text/javascript">
    function ajax_get_pemesanan() {
         var trHTML = ''; 
      var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_pemesanan";
     var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
        // alert(response.result);
             var data = response.data;
            for (var i = 0 ; i < data.length; i++) {
                if(data[i].status_order == 1){
                var status = "Belum Checkout";
                }else if(data[i].status_order == 2){
                    var status = "Admin Cek Pembayaran";
                }else if(data[i].status_order == 3){
                    var status = "Menungu Konfirmasi Admin";
                }else if(data[i].status_order == 4){
                    var status = "Pesanan di Batalkan";
                }
                 var datas = '<hr> <div style="padding-left: 15px;padding-top: 15px">'+
                                                  '<b>'+data[i].created_at+'</b>'+
                                                '</div>'+
                                                '<div class="row" style="padding-left: 15px;padding-right:  15px">'+
                                                    '<div class="col-sm-3">'+
                                                      '<label style="">Nomor Transaksi</label>'+
                                                      '<p style="font-size: 14px;">'+data[i].id_order+'</p>'+
                                                    '</div>'+
                                                    '<div class="col-sm-3">'+
                                                         '<label style="">Status</label>'+
                                                         '<p style="font-size: 14px;font-weight: bold;">'+status+'</p>'+
                                                    '</div>'+
                                                    '<div class="col-sm-3">'+
                                                        '<label style="">Total Belanja</label>'+
                                                         '<p style="font-size: 14px;font-weight: bold;">'+addNumber(data[i].total_order,true)+'</p>'+
                                                    '</div>'+
                                                    '<div class="col-sm-3">'+
                                                        '<label style="">Detail Pesanan</label>'+
                                                         '<p style="font-size: 14px;font-weight: bold;cursor:pointer" onclick="ajax_get_detail(\''+data[i].id_order+'\')"><a>Lihat Di Sini</a></p>'+
                                                    '</div>'+
                                                '</div>';
                trHTML += datas;
              }
              $('#divPemesanan').html(trHTML);  

    });

}
</script>


<script type="text/javascript">
    function ajax_get_detail(id){
        $('#modal-pembelian').modal('show'); 
        ajax_get_pemesanan_cart(id,1)
    }

    function ajax_tracking(id_order,id_cart,kode_pengiriman){
        ajax_get_pemesanan_cart(id_order,0);
        ajax_get_pemesanan_detail_cart(id_cart,kode_pengiriman)
    }
</script>
<script type="text/javascript">
    function ajax_get_pemesanan_cart(id,status) {
         var trHTML = ''; 
      var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_pemesanan_cart/ORD0656080";
     var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
          var data = response.data;
          if(status == 1){
             ajax_get_pemesanan_detail_cart(data[0].id_cart,data[0].kode_pengiriman);
          }
         
            for (var i = 0 ; i < data.length; i++) {
                
       var datas = '<div onclick="ajax_tracking(\''+data[i].id_order+'\',\''+data[i].id_cart+'\',\''+data[i].kode_pengiriman+'\')" id="tracking'+data[i].id_cart+'"><div style="font-size: 14px">Tanggal</div>'+
                               '<div style="font-size: 14px">'+data[i].created_at+'</div>'+
                               '<div style="font-size: 14px">'+data[i].name_product+'</div></div>'+
                               '<hr>';
                trHTML += datas;
        }
              $('#list_barang_checkout').html(trHTML);


    });

}
</script>



<script type="text/javascript">
    function ajax_get_pemesanan_detail_cart(id,resi) {
         var trHTML = ''; 
         var trHTML2 = ''; 
         $('#id'+id+'').css('color','green');
         $('#data_resi').empty();
         $('#detail_tracking').empty();
         data_detail = "";
      var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>checkout/get_pemesanan_detail_cart/"+id+"/"+resi;
     var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
          $('#page-load').hide();
       for (var i = 0; i < response.tracking.length; i++) { 
          if(response.tracking[i].pod_receiver == "kosong"){
            var penerima = "Belum Sampai";
          }else{
            var penerima = response.tracking[i].pod_receiver;
          }
         var table = '<tr style="font-size:13px"><td>' + response.tracking[i].id_tracking +
                  '</td style="font-size:13px"><td>' + penerima +
                  '</td style="font-size:13px"><td>' + response.tracking[i].city_name + 
                  '</td style="font-size:13px"><td>' + response.tracking[i].manifest_description +
                  '</td style="font-size:13px"><td>' +response.tracking[i].status_tracking+'</td></tr>' 
        trHTML += table;
          }
          $('#data_resi').append(trHTML);

             var data = response.data;
            for (var i = 0 ; i < data.length; i++) {
            data_detail = '<div class="col-sm-3">'+
                                          '<label style="font-size: 14px">Nama Barang</label>'+
                                                      '<p style="font-size: 14px;font-size: 14px">'+response.data[i].name_product+'</p>'+
                                    '</div>'+
                                    '<div class="col-sm-6">'+
                                          '<label style="font-size: 14px">Kurir</label>'+
                                                      '<p style="font-size: 14px;font-size: 14px">'+response.data[i].name_kurir+'</p>'+
                                    '</div>'+
                                     '<div class="col-sm-3">'+
                                          '<label style="font-size: 14px">Total</label>'+
                                                      '<p style="font-size: 14px;font-size: 14px">'+addNumber(response.data[i].subtotal,true)+'</p>'+
                                    '</div>';
                trHTML2 += data_detail;
        }
              $('#detail_tracking').html(trHTML2);


    });

}
</script>
