<div class="content">

    <div class="container" style="margin-top: 13%">
        <div class="row">
            <div class="col-sm-8">
                <div class="card-cart-one" style="padding-left: 24px">
                    <div class="checkout-text-header">
                        <div class="checkout-header-text-title" style="font-size: 19px!important">Checkout</div>
                        <label class="checkout-header-text" style="font-size: 15px">Alamat Pengirim</label>
                    </div>

                    <div style="margin-top: 2%">
                    <?php foreach ($alamat_active as $alamat_active) { ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-name-checkout" style="font-size: 17px">Alenovan Wiradhika Putra</p>
                                <p class="text-phone-checkout" style="font-size: 15px">081334367717</p>
                                <p class="text-phone-checkout" style="font-size: 15px"><?= $alamat_active->kota_alamat ?> <?= $alamat_active->kode_pos_alamat ?></p>
                                <p class="text-alamat-checkout" style="font-size: 15px"><?= $alamat_active->alamat_lengkap ?></p>
                                <input type="" name="" style="display: none;" id="id_kecamatan" value="<?= $alamat_active->id_kecamatan ?>">
                                <input type="" name="" style="display: none;" id="id_alamat" value="<?= $alamat_active->id_alamat ?>">
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <div>
                        <hr>
                        <div class="bottom-navigation-mobile" ></div>
                        <div class="row">
                            <div class="col-sm-4">
                                <button class="btn btn-success  ganti_alamat " style="font-weight: bold" data-toggle="modal" data-target="#alamat-modal">Ganti Alamat Tujuan</button>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-cart-one" style="margin-top: 14px">
                    <div class="col-sm-12">
                        <div id="cart_preview">                                
                                  </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="bottom-navigation-mobile" style="margin-top: 9%"></div>
                <div class="card-cart-two">
                    <div class="cart-price-header">
                        <div class="cart-price-header-text">Ringkasan Belanja</div>
                    </div>
                    <div class="card-price-cart">
                        <div class="row">
                            <div class="col-6 cart-title-price-text">Total Harga</div>
                            <div class="col-6" style="text-align: right;font-weight: bold;"><?= addNumber($total,true)?></div>
                        </div>

                        <div class="row" style="margin-top: 4px">
                            <div class="col-6 cart-title-price-text " >Ongkos kirim</div>
                            <div class="col-6 ongkir_text" style="text-align: right;font-weight: bold;"></div>
                        </div>
                    </div>
                    <div>

                        <div class="row" style="margin-top: 7px">
                            <div class="col-6 cart-title-price-text" style="font-weight: bold;font-size: 20px">Total</div>
                            <div class="col-6 primary-color total_price" style="text-align: right;font-weight: bold;font-size: 20px "></div>
                        </div>
                        <div class=" " style="margin-top: 9px ">
                             <button class="btn btn-success btn-md" style='width: 100%;font-weight: bold' data-toggle="modal" data-target="#checkout-modal">Pembayaran</button>
                        </div>
                    </div>
                </div>
                <div class="bottom-navigation-mobile " style="margin-top: 22% "></div>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('js/js') ?>