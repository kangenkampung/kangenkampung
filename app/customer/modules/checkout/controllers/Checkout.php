<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {

	public $rand_num = 0;

	public function __construct() {
		$this->load->model('M_checkout');

		$this->rand_num = rand(1, 99);

		$this->rand_number = $this->rand_num;
	}


	public function index(){
	    $data['content'] = 'checkout';
	     $joins_product        = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product ',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail c',
                'condition' => 'b.id_product = c.id_product ',
                'jointype' => ''
            ),
             array(
                'table' => 'mr__file d',
                'condition' => 'd.id_relation = b.id_product  AND d.position_file = 1 ',
                'jointype' => ''
            )
        );
	    $data['alamat'] = $this->M_checkout->fetch_joins('m__alamat', '*','', 'id_member ='."'".$this->session->userdata('id')."'", '', TRUE);
	    $data['alamat_active'] = $this->M_checkout->fetch_joins('m__alamat', '*','', 'id_member ='."'".$this->session->userdata('id')."'"." AND status_alamat=1", '', TRUE);
	    $data['product_cart'] = $this->M_checkout->fetch_joins('m__cart a', 'a.*,b.*,c.*,d.name_file', $joins_product, 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', TRUE);
        $data['total'] = $this->M_checkout->get_sum_cart($this->session->userdata('id'));
		$this->load->view('template',$data);
	}


    public function riwayat_pembelian(){
        $data['content'] = 'riwayat_pembelian';
        $this->load->view('template',$data);
    }

    public function get_pemesanan()
    {
        $joins = array(
            array(
                'table' => 'm__member b',
                'condition' => 'a.id_member = b.id_member',
                'jointype' => ''
            ),
        );
      
        $datas = $this->M_checkout->fetch_joins('m__order a', '*', $joins, 'a.id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_order != 0', TRUE);
        $data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses Ambil Data'),
                "form_error" => '',
                "data" => $datas
            );
        print json_encode($data);
        
    }


      

    public function get_pemesanan_cart()
    {
        $id = $this->uri->segment(3);
        
        $joins_cart = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant c',
                'condition' => 'a.id_merchant = c.id_merchant',
                'jointype' => ''
            ),
        );


        $order_cart = $this->M_checkout->fetch_joins('m__cart a', '*', $joins_cart,'a.id_order ='."'". $id."'", '',TRUE);
        if(count($order_cart) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                'data' => $order_cart
            );
            print json_encode($json_data);
        }
        
    }

     public function get_pemesanan_detail_cart()
    {
       
        $id = $this->uri->segment(3);
        $no_resi = $this->uri->segment(4);
        $joins_cart = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant c',
                'condition' => 'a.id_merchant = c.id_merchant',
                'jointype' => ''
            ),
        );


        $order_cart = $this->M_checkout->fetch_joins('m__cart a', '*', $joins_cart,'a.id_cart ='."'". $id."'", '',TRUE);
        if(count($order_cart) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                'data' => $order_cart,
                'tracking' => $this->ajax_action_get_detail_tracking($no_resi)
            );
            print json_encode($json_data);
        }
        
    }

     function ajax_action_get_detail_tracking($id){
       
        $data = $this->M_checkout->fetch_joins('a__tracking ', '*', '','no_resi ='."'". $id."'", '',TRUE);
        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data,
            );
            return $data;
        }
    }

	public function ajax_get_alamat(){
		 $data = $this->M_checkout->fetch_joins('m__alamat', '*','', 'id_member ='."'".$this->session->userdata('id')."'", '', TRUE);
		 $active = $this->M_checkout->fetch_joins('m__alamat', '*','', 'id_member ='."'".$this->session->userdata('id')."'"." AND status_alamat = 1", '', TRUE);

		   if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "active" => count($active),
                "data" => $data
            );
            print json_encode($json_data);
        }

	}
	public function ajax_edit_alamat(){
		$this->form_validation->set_rules('id_alamat', 'id_alamat', 'required');
		$id = post('id_alamat');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}

		$datanull = array(
					"status_alamat" => 0,
					'update_at' => date('Y-m-d H:m:s')
				);
		$this->M_checkout->update_table('m__alamat', $datanull, 'id_member', $this->session->userdata('id'));

		$data = array(
					"status_alamat" => 1,
					'update_at' => date('Y-m-d H:m:s')
				);
		$edit = $this->M_checkout->update_table('m__alamat', $data, 'id_alamat', $id);

		if($edit == FALSE){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal update Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses update Data'),
				"form_error" => '',
				"redirect" => ''.base_url().$this->config->item('index_page').'/checkout/'
			);
			print json_encode($json_data);
		}
	}

		public function get_cart_data(){
		  $joins_product        = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product ',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail c',
                'condition' => 'b.id_product = c.id_product ',
                'jointype' => ''
            ),
             array(
                'table' => 'mr__merchant e',
                'condition' => 'b.id_merchant = e.id_merchant ',
                'jointype' => ''
            ),
               array(
                'table' => 'mr__merchant_detail f',
                'condition' => 'f.id_detail_merchant = e.id_detail_merchant ',
                'jointype' => ''
            ),
             array(
                'table' => 'mr__file d',
                'condition' => 'd.id_relation = b.id_product  AND d.position_file = 1 ',
                'jointype' => ''
            )
        );
		$data = $this->M_checkout->fetch_joins('m__cart a', 'a.*,b.*,c.*,d.*,e.*,f.*', $joins_product, 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', TRUE);
        $json = json_encode($data);
        $objects = json_decode($json);
        $grouped = array();
        $i = 0;
        foreach($objects as $object) {
       	
	    if(!array_key_exists($object->name_merchant, $grouped)) { 
	         $newObject = new stdClass();
	         $newObject->name_merchant = $object->name_merchant;
	         $newObject->id_merchant = $object->id_merchant;
	         $newObject->name_kurir = $object->name_kurir;
	         $newObject->kurir_price = $object->kurir_price;
	         $newObject->items = array();
	         $grouped[$object->name_merchant] = $newObject;
	    }
		    $taskObject = new stdClass();
		    
		    if(count($newObject->items) == 0){
		    	$taskObject->name_product = $object->name_product;
		    	$taskObject->berat_product = $object->berat_product;
		    	$taskObject->id_cart = $object->id_cart;
		    	$taskObject->id_kecamatan = $object->id_kecamatan;
		    	$taskObject->id_produk = $object->id_produk;
			    $taskObject->urut = count($newObject->items);
			    $taskObject->price = $object->price;
			    $taskObject->name_file = $object->name_file;
			    $taskObject->id_merchant = $object->id_merchant;
			    $taskObject->name_merchant = $object->name_merchant;
		    }else{
		    	$taskObject->urut = count($newObject->items);
		    	$taskObject->id_produk = $object->id_produk;
		    	$taskObject->id_cart = $object->id_cart;
		    	$taskObject->berat_product = $object->berat_product;
		    	$taskObject->id_kecamatan = $object->id_kecamatan;
		    	$taskObject->name_product = $object->name_product;
			    $taskObject->price = $object->price;
			    $taskObject->name_file = $object->name_file;
		    }
		    
		    $grouped[$object->name_merchant]->items[] = $taskObject;
		}
		$grouped = array_values($grouped);

		$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Ambil Data'),
				"form_error" => '',
				"data" => $grouped
			);
			print json_encode($json_data);

        }

        public function get_count_cart(){
		$datas = $this->M_checkout->fetch_joins('m__cart a', '*', '', 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', TRUE);
		$data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses Ambil Data'),
				"form_error" => '',
				"data" => count($datas)
			);
			print json_encode($data);
        }


        public function get_count_pembayaran(){
         $datas = $this->M_checkout->fetch_joins('m__order a', '*', '', 'a.id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_order != 0', TRUE);
        $data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses Ambil Data'),
                "form_error" => '',
                "data" => count($datas)
            );
            print json_encode($data);
        }


	public function simpan_kurir(){
		$this->form_validation->set_rules('value', 'value', 'required');
		$this->form_validation->set_rules('jasa', 'jasa', 'required');
		$this->form_validation->set_rules('id_merchant', 'id_merchant', 'required');

		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

		

			$data = array(
				"name_kurir" => post("value"),
				"kurir_price" => post("price"),
				"id_alamat" => post("id_alamat"),
				"layanan_kurir" => post("jasa")
			);

			
			$edit = $this->M_checkout->update_table_kurir("m__cart a",$data,"status_cart = 1 and id_member='".$this->session->userdata('id')."'"." AND "."id_merchant= '".'MR'.post("id_merchant")."'");
			if($edit==FALSE){
				$json_data =  array(
					"result" => TRUE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
			}
		}
	}

	


	 public function ajax_action_checkout()
    {
      $this->form_validation->set_rules('id_payment_account', 'id_payment_account', 'required');
        if ($this->form_validation->run() == FALSE) {
            $error     = $this->form_validation->error_array();
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Pastikan Data terisi Semua'
                ),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $count_data = $this->M_checkout->get_count_data('m__order');

            // count_all($table,$where,$joins)
            $kurir_count = $this->M_checkout->count_all('m__cart','id_member ='."'".$this->session->userdata('id')."'".' AND kurir_price = "0"','');

            if($kurir_count > 0){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Atur tujuan pengiriman anda'),
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
           $id_order    = "ORD" . date('his') . $count_data;


           $total_kurir = $this->M_checkout->fetch_table('kurir_price as total','m__cart','id_member ='."'".$this->session->userdata('id')."'".' AND status_cart = "1" GROUP BY name_kurir','','','','',TRUE);
           $harga_kurir = 0;

           foreach ($total_kurir as $key) {
                   $harga_kurir += $key['total'];
           }


            $total = $this->M_checkout->get_row('sum(subtotal) as total','m__cart','id_member ='."'".$this->session->userdata('id')."'".'AND status_cart = 1','','',TRUE);

            $total_chekcout = $total['total']+$harga_kurir + $this->rand_number;
            // echo $total['total'];
            // die();
            $data = array(
            	"id_member" =>$this->session->userdata('id'),
                "id_order" => $id_order,
                "id_payment_account" => post("id_payment_account"),
                "status_order" => 1,
                "total_order" =>$total_chekcout,
                "created_at" => date("Y-m-d H:i:s")
            );
            $add = $this->M_checkout->insert_table("m__order",$data);
            $data_edit = array(
                            "status_cart" => 2,
                            "id_order" => $id_order,
                            "update_at" => date("Y-m-d H:i:s")
                        );
            $this->M_checkout->update_table_where('m__cart', $data_edit,'id_member ='."'".$this->session->userdata('id')."'".'AND status_cart = 1');
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'pembayarans/'.$id_order
                );
                print json_encode($json_data);
            }
            }
            
           
             
        }
    }
    

    }


?>