<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MY_Controller {

	public function __construct() {
		$this->load->model('M_pembayaran');
	}


	public function index(){
	    $data['content'] = 'pembayaran';
		$this->load->view('template',$data);
	}

		public function home(){
	    $data['content'] = 'pembayaran';
		$this->load->view('template',$data);
	}

	public function get_admin_account(){
		$joins = array(
			array(
				'table' => 'a__payment_method d',
				'condition' => 'a.id_payment_method = d.id_payment_method',
				'jointype' => ''
			),
			array(
				'table' => 'a__file b',
				'condition' => 'b.id_file = d.id_file',
				'jointype' => ''
			),
		
		);
		$data = $this->M_pembayaran->fetch_joins('a__payment_account a', '*', $joins,'', '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}

	}

	public function get_order(){
		$id_order= $this->input->post("c3ec0f7b054e729c5a716c8125839829");
		$joins = array(
			array(
				'table' => 'a__payment_account c',
				'condition' => 'c.id_payment_account = a.id_payment_account',
				'jointype' => ''
			),
			array(
				'table' => 'a__payment_method b',
				'condition' => 'b.id_payment_method = c.id_payment_method',
				'jointype' => ''
			),
			array(
				'table' => 'a__file d',
				'condition' => 'd.id_file = b.id_file',
				'jointype' => ''
			),
			
		
		);
		$data = $this->M_pembayaran->fetch_joins('m__order a', 'a.total_order,name_payment_method,name_payment_account,nomor_payment_account,nama_file', $joins,'id_order = '."'".$id_order."'", '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function upload_pembayaran(){
		$this->form_validation->set_rules('id_order', 'id_order', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{

			 if (isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			/*-------------setting attachment upload -------------*/
			$config['upload_path'] = './uploads/bukti_pembayaran/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = 1024 * 8;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file')){
				$data['file_name'] = null;
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$data = $this->upload->data();
			}
		}else{
			$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=>"Harus Mengupload Bukti Pembayaran "),
					"form_error" => 'file proof',
					"redirect" => ''
				);
				print json_encode($json_data);
			die();
        }

       	 $data_file = array(
				"nama_file" => $data['file_name'],
				"id_relation" => post("id_order"),
				"table_relation" => "m__order",
				"created_at" => date("Y-m-d H:i:s")
			);
		$add_file = $this->M_pembayaran->insert_table("a__file",$data_file);

		if($add_file){
			  $last_data= $this->M_pembayaran->get_last_data('id_file', 'a__file','DESC');
			  $data = array(
				"status_order" => 2,
				"id_member"=>$this->session->userdata('id'),
				"id_file" =>$last_data->id_file,
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_pembayaran->update_table("m__order",$data,"id_order",post("id_order"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'dashboard'
				);
				print json_encode($json_data);
			}
		}else{
			$json_data =  array(
					"result" => FALSE,
					"message" => array('head'=> 'Success', 'body'=> 'Mohon Maaf terjadi kesalahan silahkan coba beberaoa saat lagi'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'dashboard'
				);
				print json_encode($json_data);
			  }
		}
	}



}
?>