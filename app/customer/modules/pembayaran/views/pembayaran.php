<div class="content">
        <div class="container">
        <!-- pc show -->
        <div class="row">
            <div class="col-sm-8">
             <div class="card">
                <div style="padding: 2%;text-align: center;">
                                    <label style="font-weight: bold">Segera lakukan pembayaran</label><br>
                                    <code>Jumlah Yang Harus Di Bayar</code><br>
                                    <label style="font-size: 35px" class="total_order"></label>
                                    <div class="alert-div" style="padding: 18px;font-size: 15px">
                                            <div><b>Transfer Tepat Sampai 3 Digit Terakhir</b></div>
                                            <div>Perbedaan Nominal menghambat proses verifikasi </div>
                                            <div>Setelah Transfer Lakukan Konfirmasi Di Samping</div>
                                    </div>
                </div>
             </div>
             </div>
             <div class="col-4 mobile-hide">
                 <div class="card">
                     <div class="row" style="padding: 8px">
                         <div class="col-sm-12" style="text-align: center;padding: 2%">
                         <p style="font-weight: bold">Transfer Pembayaran ke </p>
                         <img  id='icon_payment' style="height: 54px">
                         </div>

                         <div class="col-sm-12" style="text-align: center;">
                         <hr>
                             <div class="row">
                                 <div class="col-sm-12">
                                    <label>Nama Bank</label>
                                    <label>:</label>
                                    <label class="nama_bank"></label>
                                 </div>
                                 <div class="col-sm-12">
                                    <label>No Rekening</label>
                                    <label>:</label>
                                    <label class="no_rek"></label>
                                 </div>
                                 <div class="col-sm-12">
                                    <label>Nama Rekening</label>
                                    <label>:</label>
                                    <label class="nama_rek"></label>
                                 </div>
                             </div>
                         <hr>
                         <div>
                             <button class="btn btn-success" style="width: 100%" onclick="open_modal()">Unggah Bukti Pembayaran</button>
                         </div>
                         </div>
                     </div>
                 </div>
             </div>
        </div>
        <!-- end pc show -->
        </div>
    </div>
</body>

<div class="bottom-navigation-mobile mobile-show">
    <footer>
        <nav class="navbar-bottom-mobile fixed-bottom">
            <div class="navbar-body-bottom">
                <div class="row">
                        <div class="col-8" style="padding-top: 8px;padding-left: 12%">
                            <label class="white-color"><font style="font-size: 25px">Rp.200.000.000</font></label>
                        </div>
                        <div class="col-4" style="padding-top: 8px;padding-right:19%">
                            <button class="btn btn-light" onclick="invoice_choice(2)">
                                    BAYAR
                            </button>
                        </div>
                </div>
            </div>
        </nav>
    </footer>
</div>

<?= $this->load->view('js/js')?>