<div class="content">
 <div style="margin-left: 7%;margin-right: 7%;margin-top: 13%">
        <div class="row ">
            <div class="col-sm-6 ">
                            <img src="" class="image-detail-preview product-detail__img-holder" id="preview-detail">
                            <br>
                            <div >
                                 <center>
                                <?php for ($i=0; $i <4 ; $i++) {?>
                                     <label>
                                    <div class="border-detail-preview-child navbar-pointer">
                                        <div class="img-hover-zoom">
                                            <img src="" class="image-detail-preview-child" onmouseover="preview_detail('image_<?php echo "$i"; ?>')" id="image_<?php echo "$i"; ?>">
                                        </div>
                                    </div>
                                </label>

                             <?php } ?>
                         </center>
                            </div>
                       
            </div>


            <!-- Detail Product -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-9" style="margin-top: 1%">
                        <span class="detail-product-title" id="text_product_title"></span>
                        <div class="row">
                            <div class="col-6">
                                <label class="jenis-detail-product">Jenis : </label>
                                <label id="text_name_category"></label>
                            </div>
                            <div class="col-6">
                                <label class="jenis-detail-product">Stok :</label>
                                <label id="text_product_stock"></label>
                            </div>
                        </div>
                        <div>
                            <label style="font-weight: bold;">Lokasi :</label>
                            <label id="text_lokasi_product"></label>
                            <input type="text" name="" id="expired" value="" style="display: none">
                             <input type="text" name="" id="val_lokasi" value="" style="display: none">
                        </div>
                        <div>
                            <label>99.29% Transaksi Sukses Dari >100 Transaksi </label>
                        </div>
                        <div class='price-diskon-text' id="text_diskon_price"></div>
                        <div class="detail-price" id="text_product_price">
                        </div>

                        <div>
                            <div class="row" style="margin-top: 5%;">
                                <div class="col-sm-6">
                                    <label><b>Jumlah</b></label>
                                    <div class="row" style="margin-left: 1%">
                                        <div>
                                            <input type="number" class="form-control" name="" style="width:50%" value="1" id="qty">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6" style="display: none">
                                    <label><b>Catatan ke penjual</b></label>
                                    <textarea  class="form-control" name=""></textarea>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="row" style="margin-top: 5%;">
                                <div class="col-3">
                                    <!-- <a href="<?php echo base_url().index_page(); ?>Keranjang"> -->

                                        <button class="btn btn-outline-success" style="width: 100%" onclick="add_buy($('#text_name_category').text())">Beli</button>
                                    <!-- </a> -->
                                </div>

                                <div class="col-9">
                                    <button class="btn btn-success" style="width: 100%" onclick="add_cart($('#text_name_category').text())">Tambah Ke Keranjang</button>
                                </div>
                            </div>
                        </div>

                        <div style="margin-top: 5%">
                            <label><b>Estimasi ongkos kirim</b></label>
                            <br>
                             <label style="font-weight: bold"> Berat  :&nbsp;&nbsp; </label><label id="text_product_berat"></label>
                             <br>
                             <label style="font-weight: bold"> Dimensi  :&nbsp;&nbsp; </label><label id="text_product_dimensi"></label>
                             <input type="" name="" style="display: none" id="val_product_berat">
                             <input type="" name="" style="display: none" id="val_product_diameter">
                            <div>
                                <table class="table table-bordered">
                                    <tr class="table-detail-title">
                                        <td>Provinsi&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>Kota Atau Kabupaten</td>
                                        <td>Kurir</td>
                                       
                                    </tr>
                                    <tr>
                                     <td>
                                            <select class="form-control select2" id="provinci"></select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="kota"></select>
                                        </td>
                                        <td>
                                            <select class="form-control select2" id="jasa" onchange ="check_ongkir()">
                                                <option value="jne">JNE</option>
                                                <option value="tiki">TIKI</option>
                                                <option value="pos">POS</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>

                                <table id="preview_ongkir" class="table table-bordered">
                                    
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-3" style="">
                        <div class="detail-catatan-toko">
                            <div class="title-catatan-toko">
                                <label>Catatan Toko</label>
                            </div>
                            <div class="body-catatan-toko">
                                <label id="text_catatan_toko"></label>
                            </div>
                        </div>

                        <div class="detail-pengiriman-toko" style="">
                            <div class="title-catatan-toko">
                                <label> Dukungan Pengiriman</label>
                            </div>
                            <div class="" id="kurir_list">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Description -->
<div class="bottom-navigation-mobile" style="margin-top: 5%"></div>
<div class="detail-description-box ">
    <div class="container">
        <ul class="nav nav-tabs " role="tablist">
            <li class="nav-item navbar-pointer active">
                <a class="nav-link  " href="#informasi" role="tab" data-toggle="tab">
                    <label class="primary-color navbar-pointer">Informasi </label>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link navbar-pointer" href="#ulasan" role="tab" data-toggle="tab">
                    <label class="primary-color navbar-pointer">Ulasan</label>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link navbar-pointer" href="#diskusi" role="tab" data-toggle="tab">
                    <label class="primary-color navbar-pointer">Diskusi Produk</label>
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" style="color: black">
            <div role="tabpanel" class="tab-pane active" id="informasi">
                <div style="padding: 2%" id="text_informasi_product">
                    Ini Halaman Home
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="ulasan">
                <div style="padding: 2%">
                    Ini Halaman Home
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="diskusi">
                <div style="padding: 2%">
                    Ini Halaman Home
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Caraousel inFooter -->
<br>
<br>
<br>
<div class="hide-mobile">
   
        <div  class="card-item-tranding-item">
            <div class="" style="margin-top: 2%">
                <div class="ml4percent mr4percent">
                    <div class="row">
                        <div class="col-8" style="margin-top: 3%">
                            <p class="title-kategory">Paling Sering Di Beli</p>
                        </div>
                        <div class="col-4 text-show-all primary-color text-show-all-item">
                            <p>Lihat Semua</p>
                        </div>

                    </div>
                </div>
                <!-- multuplie -->
                 <div class=" mr4percent multiple-items-tranding" style="margin-left: 22px">
                  <?php foreach ($product_tranding as $product_trandings) { ?>
                  <?php
                    $diskon = $product_trandings['diskon_product'] / 100 * $product_trandings['harga_product'];
                    $diskon_total =  $product_trandings['harga_product'] - $diskon;
                    if($product_trandings['diskon_product'] > 0 ){
                        $discount_title = "<div class='price-diskon'>".$product_trandings['diskon_product']."% OFF"."</div>";
                        $discount_price = "<span class='price-diskon-text'>".addNumber($product_trandings['harga_product'],true)."</span>";
                        $real_price     =    "<div style='' class='primary-color price-item'><b>".addNumber($diskon_total,true)."</b></div>";
                    }else{
                        $discount_price = "<span class=''></span>";
                        $discount_title =  "<div class=''></div>";
                        $real_price         =    "<div style='' class='primary-color price-item'><b>".addNumber($product_trandings['harga_product'],true)."</b></div>";
                    }
                  ?>
                 <div>
                <div class="  " style="margin-top: 2%;margin-left: 32px">
                     <div class="row">
                                            <div class="card-item">
                                                <div class="card">
                                                    <a href="<?php echo base_url().index_page(); ?>products/<?= $product_trandings['id_product'] ?>">
                                                        <div class="img-hover-zoom ">
                                                             <img src="<?= base_url().'uploads/product/';?><?= $product_trandings['name_file'] ?>" style="height: fit-content;max-width: 100%;height: 200px"> 
                                                            <div class="top-right">
                                                                <?= $discount_title; ?>
                                                            </div>
                                                        </div>
                                                        <div style="text-align: left;padding: 6px">
                                                            <div style="" class="title-item"><?= $product_trandings['name_product'] ?></div>
                                                            <?= $discount_price; ?>
                                                            <div style="" class="primary-color price-item"><b><?= $real_price ?></b></div>
                                                            <div style="" class="location-item"><?= $product_trandings['lokasi_product'] ?></div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                    </div>
                    </div>
                </div>
                 <?php } ?>
                </div>
                </div>
                <!-- End Tranding product -->
            </div>
        </div>
</div>
<input type="" name="" style="display: none" id="id_category">
<?= $this->load->view('js/js')?>