<script type="text/javascript">
    $(document).ready(function() {
       ajax_get_product_detail();
       ajax_get_kurir();
       set_provinci();
        
    });

</script>

<script type="text/javascript">
  $(document).ready(function(){
$('.multiple-items').slick({
  slidesToShow: 4,
    prevArrow: '<div class="slide-arrow left_slider"><div class="tombol_kategory" style="cursor:pointer;"><i class="fas fa-chevron-left" style="color: #d6cece;transform: translateY(25%);font-size:12px;cursor:pointer;"></i></div></div>',
   nextArrow: '<div class="slide-arrow right_slider"><div class="tombol_kategory" style="cursor:pointer;"><i class="fas fa-chevron-right" style="color: #d6cece;transform: translateY(25%);font-size:12px;"></i></div></div>'
});

$('.tombol_kategory').hide();



$('.multiple-items-tranding').slick({
  slidesToShow: 3,
  slidesToScroll: 3,
  variableWidth: true,
  variableHeight: true
});
  
  });

</script>
<script type="text/javascript">
      function ajax_get_product_detail(){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>product/get_data_product_by_id/";
          var image_link = "<?php echo base_url();?>/uploads/product/";
          var form_data = new FormData();
          var trHTML = ''; 
          form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');            
          ajaxShowData(link,"POST",form_data,function(response) {
              var diskon =  parseInt(response.data.product[0].diskon_product) /100 * response.data.product[0].harga_product;
              var diskon_total =  response.data.product[0].harga_product - diskon;
             if(response.data.product[0].diskon_product > 0){
                var discount_price =  addNumber(response.data.product[0].harga_product,true);
                var real_price     =  addNumber(diskon_total,true);
              }else{
                var discount_price =   "";
                $('#text_diskon_price').css('display','none');
                var real_price     =   addNumber(response.data.product[0].harga_product,true);
              }


               

              
             $('#text_product_title').text(response.data.product[0].name_product);
             $('#text_product_stock').text(response.data.product[0].stok_product+" Pcs");
             $('#text_lokasi_product').text(response.data.product[0].lokasi_product);
             $('#text_product_price').text(real_price);
             $('#text_catatan_toko').text(response.data.product[0].catatan_product);
             $('#text_informasi_product').html(response.data.product[0].informasi_product);
             $('#text_diskon_price').text(discount_price);
             $('#text_name_category').text(response.data.product[0].name_category);
             $('#text_product_berat').text(response.data.product[0].berat_product+"gr");
             $('#text_product_dimensi').text(response.data.product[0].dimensi_product+"cm");
             $('#expired').val(response.data.product[0].expired_product);
             $('#id_category').val(response.data.product[0].id_category);
             $('#val_lokasi').val(response.data.product[0].kota_product);
             $('#val_product_berat').val(response.data.product[0].berat_product);
             $('#val_product_diameter').val(response.data.product[0].dimensi_product);
             // if(response.data.product[0].diskon_product < 0){
             //    $('#text_product_diskon').css('display','none');
             // }else{
             //    $('#text_product_diskon').text(response.data.product[0].diskon_product +"%OFF");
             // }
              var data = response.data.image;
              for (var i = 0 ; i < 4; i++) {
                  $("#image_"+i).attr("src",image_link+data[i].name_file);
              }

              for (var i = 0 ; i < data.length; i++) {
                  $("#m_image_"+i).attr("src",image_link+data[i].name_file);
              }
                  $("#preview-detail").attr("src",image_link+data[0].name_file);
             $('#page-load').hide();
          });
       
        }

           function ajax_get_kurir(){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>product/ajax_action_get_kurir/";
           var form_data = new FormData();
            var trHTML = ''; 
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');            
          ajaxShowData(link,"POST",form_data,function(response) {
              var data = response.data;
              for (var i = 0 ; i < data.length; i++) {
                 var datas = '<img src="<?= base_url().'uploads/kurir/';?>'+data[i].nama_file+'" class="detail-courir-logo">'+
                                '<div class="row">'+
                                    '<div class="col-12" style="margin-left: 8px">'+
                                        '<label style="font-size: 11px">'+data[i].name_kurir+'</label>'+
                                    '</div>'+
                                '</div>';
                trHTML += datas;
              }
               $('#kurir_list').html(trHTML);  
             $('#page-load').hide();
          });
       
        }

        

</script>



<script type="text/javascript">
// alert(datediff(parseDate(first.value), parseDate(second.value)));
  function add_cart(jenis){
    var date_start= '<?= date("Y-m-d"); ?>';
    var date_end = $('#expired').val();
    if('<?= $this->session->userdata('id') ?>'== ''){
       $('#login-modal').modal('show');
    }else{
      if(jenis.toLowerCase() == "makanan"){
          message("Info",""+"Product ini akan Expired "+ datediff(date_start,date_end)+" Hari Lagi", "info", "cart",'');
      }else if(jenis == "minuman"){
       message("Info",""+"Product ini akan Expired "+ datediff(date_start,date_end)+" Hari Lagi", "info", "cart",'');
      }else{
        ajax_add_cart();
       }
    }
      
  }

  function add_buy(jenis){
    var date_start= '<?= date("Y-m-d"); ?>';
    var date_end = $('#expired').val();
    if('<?= $this->session->userdata('id') ?>'== ''){
       $('#login-modal').modal('show');
    }else{
      if(jenis.toLowerCase() == "makanan"){
          message("Info",""+"Product ini akan Expired "+ datediff(date_start,date_end)+" Hari Lagi", "info", "cart",'');
      }else if(jenis == "minuman"){
       message("Info",""+"Product ini akan Expired "+ datediff(date_start,date_end)+" Hari Lagi", "info", "cart",'');
      }else{
        ajax_add_buy();
       }
    }
      
  }

  function ajax_add_cart(){
     var id_product= '<?= $this->uri->segment(2); ?>';
     var qty       =  $('#qty').val();

      var form_data = new FormData();
      form_data.append('id_product', id_product);
      form_data.append('qty', qty);
      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_add_cart/","POST",form_data);
  }

  function ajax_add_buy(){
     var id_product= '<?= $this->uri->segment(2); ?>';
     var qty       =  $('#qty').val();

      var form_data = new FormData();
      form_data.append('id_product', id_product);
      form_data.append('qty', qty);
      form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_buy/","POST",form_data);
  }


</script>

<script type="text/javascript">
  
  function set_provinci(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_provinci_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "province");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].province_id+"'>"+response.rajaongkir.results[i].province+"</option>";
        }
        $('#provinci').html(html);
        $('#page-load').hide();
        });
  }


    $('#provinci').on('change', function() {
           set_kota_detail(this.value);
           $('#page-load').hide();
    });

   function set_kota_detail(value){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_provinci_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "city?province="+value);
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#kota').html(html);
        $('#page-load').hide();
        });
  }


 function check_ongkir() {
    $('#page-load').show();
    var berat = $('#val_product_berat').val();
    var diameter = $('#val_product_diameter').val();
    var destination = $('#kota').val();
    var origin = $('#val_lokasi').val();
    var jasa = $('#jasa').val();
    var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/get_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    if (jasa != "pos") {
        var text = "Hari";
    } else {
        var text = "";
    }
    form_data.append('weight', berat);
    form_data.append('destination', destination);
    form_data.append('kurir', jasa);
    form_data.append('origin', origin);
    form_data.append('diameter', origin);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
        var html = '';
        var i;
        try {
            var data = response.rajaongkir.results[0].costs;
            // alert(data.length);
            if(data.length != 0){
              for (i = 0; i < response.rajaongkir.results[0].costs.length; i++) {
                  html += "<tr><td>" + data[i].service + "</td><td>" + addNumber(data[i].cost[0].value, true) + "</td><td>" + data[i].cost[0].etd + " " + text + "</td></tr>";
              }
            }else{
               message("Mohon maaf", "Saat ini Jasa  tidak tersedia di kota ini", "error", "info", 3000);
            }
            $('#preview_ongkir').html(html);
        } catch (err) {
             message("Mohon maaf", "Saat ini Jasa  tidak tersedia di kota ini", "error", "info", 3000);
        }

        $('#preview_ongkir').html(html);
        // alert();
        // $('#page-load').hide();
        $('#page-load').hide();
    });

}

    

</script>

<script type="text/javascript">
  function update_jasa_ongkir(){
    
  }
</script>

