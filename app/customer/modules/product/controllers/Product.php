<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_product');
    }
    
    
    
    public function home($param)
    {
        $data['content']          = 'detail_product';
        $joins_product            = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            )
        );
        $data['product_tranding'] = $this->M_product->fetch_joins('mr__product a', 'a.*,b.*,c.*', $joins_product, 'a.status_product != 3', TRUE);
         $data['kurir'] = $this->M_product->fetch_joins('a__kurir a', 'api_kurir,name_kurir','', '', TRUE);
         $data['count_cart'] = $this->M_product->count_all('m__cart', 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1','');
        $this->load->view('template', $data);
    }
    
    public function get_data_product_by_id()
    {
        $id              = $this->input->post('id_product');
        $joins           = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'a__category c',
                'condition' => 'a.id_kategory = c.id_category',
                'jointype' => ''
            )
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_product = ' . "'" . $id . "'", '', TRUE);
        $data['image']   = $this->M_product->fetch_table('name_file ', 'mr__file', 'id_relation = ' . "'" . $id . "'", '', '', '', '', TRUE);
        if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
    
    function ajax_action_get_kurir()
    {
        $joins = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            )
        );
        $data  = $this->M_product->fetch_joins('a__kurir a', '*', $joins, 'a.status_kurir != 3', '', TRUE);
        
        if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
    
    public function ajax_action_add_cart()
    {
        $this->form_validation->set_rules('id_product', 'id_product', 'required');
        $this->form_validation->set_rules('qty', 'qty', 'required');
        $id = post('id_product');
        // $id ='PRD0149590';
        if ($this->form_validation->run() == FALSE) {
            $error     = $this->form_validation->error_array();
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Pastikan Data terisi Semua'
                ),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $count_data = $this->M_product->get_count_data('m__cart');
            $id_cart    = "TR" . date('his') . $count_data;
            
            $product = $this->M_product->get_row('*', 'mr__product_detail', 'id_product = ' . "'" . $id . "'", '', '', TRUE);
            $merchant = $this->M_product->get_row('*', 'mr__product', 'id_product = ' . "'" . $id . "'", '', '', TRUE);
            
            $diskon       = $product['diskon_product'] / 100 * $product['harga_product'];
            $diskon_total = $product['harga_product'] - $diskon;
            if ($product['diskon_product'] > 0) {
                $total = $diskon_total * post('qty');
                $harga = $diskon_total;
            } else {
                $total = $product['harga_product'] * post('qty');
                $harga = $product['harga_product'];
            }
            
            $available = $product['stok_product'] - post('qty');
            if ($available < 0) {
                $error     = $this->form_validation->error_array();
                $json_data = array(
                    "result" => FALSE,
                    "message" => array(
                        'head' => 'Failed',
                        'body' => 'Stok saat ini tidak cukup'
                    ),
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {
                $cart = $this->M_product->get_row('*', 'm__cart', 'id_produk = ' . "'" . $product['id_product'] . "'" . " AND " . 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', '', '', TRUE);
                
                if ($cart['id_cart'] != "") {
                    $qty_all        = $cart['qty'] + post('qty');
                    $data_edit_cart = array(
                        "qty" => $qty_all
                    );
                    $cart_edit      = $this->M_product->update_table('m__cart', $data_edit_cart, 'id_cart',$cart['id_cart']);
                    
                    if ($cart_edit == FALSE) {
                        $json_data = array(
                            "result" => FALSE,
                            "message" => array(
                                'head' => 'Failed',
                                'body' => 'Gagal Memasukkan Data ke keranjang'
                            ),
                            "form_error" => $error,
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    } else {
                        $data_edit = array(
                            "stok_product" => $product['stok_product'] - post('qty')
                        );
                        $this->M_product->update_table('mr__product_detail', $data_edit, 'id_product', $cart['id_produk']);
                        $json_data = array(
                            "result" => TRUE,
                            "message" => array(
                                'head' => 'Success',
                                'body' => 'Data di masukkan di keranjang'
                            ),
                            "form_error" => '',
                            "redirect" => '' . base_url() . $this->config->item('index_page') . 'products/' . $id
                        );
                        print json_encode($json_data);
                    }
                } else {
                    $data = array(
                        "id_cart" => $id_cart,
                        "id_member" => $this->session->userdata('id'),
                        "id_produk" => $product['id_product'],
                        "price" => $harga,
                        "id_merchant" => $merchant['id_merchant'],
                        "qty" => post('qty'),
                        "status_cart" => 1,
                        "subtotal" => $total,
                        "diskon" => $product['diskon_product'],
                        "created_at" => date("Y-m-d H:i:s")
                    );
                    $add  = $this->M_product->insert_table("m__cart", $data);
                    if ($add == FALSE) {
                        $json_data = array(
                            "result" => FALSE,
                            "message" => array(
                                'head' => 'Failed',
                                'body' => 'Gagal Memasukkan Data ke keranjang'
                            ),
                            "form_error" => $error,
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    } else {
                        $data_edit = array(
                            "stok_product" => $product['stok_product'] - post('qty')
                        );
                        $this->M_product->update_table('mr__product_detail', $data_edit, 'id_product', $id);
                        $json_data = array(
                            "result" => TRUE,
                            "message" => array(
                                'head' => 'Success',
                                'body' => 'Data di masukkan di keranjang'
                            ),
                            "form_error" => '',
                            "redirect" => '' . base_url() . $this->config->item('index_page') . 'products/' . $id
                        );
                        print json_encode($json_data);
                    }
                }
            }
        }
    }

     public function ajax_action_buy()
    {
        $this->form_validation->set_rules('id_product', 'id_product', 'required');
        $this->form_validation->set_rules('qty', 'qty', 'required');
        $id = post('id_product');
        // $id ='PRD0149590';
        if ($this->form_validation->run() == FALSE) {
            $error     = $this->form_validation->error_array();
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Pastikan Data terisi Semua'
                ),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $count_data = $this->M_product->get_count_data('m__cart');
            $id_cart    = "TR" . date('his') . $count_data;
            
            $product = $this->M_product->get_row('*', 'mr__product_detail', 'id_product = ' . "'" . $id . "'", '', '', TRUE);
             $merchant = $this->M_product->get_row('*', 'mr__product', 'id_product = ' . "'" . $id . "'", '', '', TRUE);
            $diskon       = $product['diskon_product'] / 100 * $product['harga_product'];
            $diskon_total = $product['harga_product'] - $diskon;
            if ($product['diskon_product'] > 0) {
                $total = $diskon_total * post('qty');
                $harga = $diskon_total;
            } else {
                $total = $product['harga_product'] * post('qty');
                $harga = $product['harga_product'];
            }
            
            $available = $product['stok_product'] - post('qty');
            if ($available < 0) {
                $error     = $this->form_validation->error_array();
                $json_data = array(
                    "result" => FALSE,
                    "message" => array(
                        'head' => 'Failed',
                        'body' => 'Stok saat ini tidak cukup'
                    ),
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {
                $cart = $this->M_product->get_row('*', 'm__cart', 'id_produk = ' . "'" . $product['id_product'] . "'" . " AND " . 'id_member = ' . "'" . $this->session->userdata('id') . "'" . " AND " . 'status_cart = 1', '', '', TRUE);
                
                if ($cart['id_cart'] != "") {
                    $qty_all        = $cart['qty'] + post('qty');
                    $data_edit_cart = array(
                        "qty" => $qty_all
                    );
                    $cart_edit      = $this->M_product->update_table('m__cart', $data_edit_cart, 'id_cart',$cart['id_cart']);
                    
                    if ($cart_edit == FALSE) {
                        $json_data = array(
                            "result" => FALSE,
                            "message" => array(
                                'head' => 'Failed',
                                'body' => 'Gagal Memasukkan Data ke keranjang'
                            ),
                            "form_error" => $error,
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    } else {
                        $data_edit = array(
                            "stok_product" => $product['stok_product'] - post('qty')
                        );
                        $this->M_product->update_table('mr__product_detail', $data_edit, 'id_product', $cart['id_produk']);
                        $json_data = array(
                            "result" => TRUE,
                            "message" => array(
                                'head' => 'Success',
                                'body' => 'Data di masukkan di keranjang'
                            ),
                            "form_error" => '',
                            "redirect" => '' . base_url() . $this->config->item('index_page') . 'keranjang'
                        );
                        print json_encode($json_data);
                    }
                } else {
                    $data = array(
                        "id_cart" => $id_cart,
                        "id_member" => $this->session->userdata('id'),
                        "id_produk" => $product['id_product'],
                        "price" => $harga,
                        "qty" => post('qty'),
                        "status_cart" => 1,
                        "id_merchant" => $merchant['id_merchant'],
                        "subtotal" => $total,
                        "diskon" => $product['diskon_product'],
                        "created_at" => date("Y-m-d H:i:s")
                    );
                    $add  = $this->M_product->insert_table("m__cart", $data);
                    if ($add == FALSE) {
                        $json_data = array(
                            "result" => FALSE,
                            "message" => array(
                                'head' => 'Failed',
                                'body' => 'Gagal Memasukkan Data ke keranjang'
                            ),
                            "form_error" => $error,
                            "redirect" => ''
                        );
                        print json_encode($json_data);
                        die();
                    } else {
                        $data_edit = array(
                            "stok_product" => $product['stok_product'] - post('qty')
                        );
                        $this->M_product->update_table('mr__product_detail', $data_edit, 'id_product', $id);
                        $json_data = array(
                            "result" => TRUE,
                            "message" => array(
                                'head' => 'Success',
                                'body' => 'Data di masukkan di keranjang'
                            ),
                            "form_error" => '',
                            "redirect" => '' . base_url() . $this->config->item('index_page') . 'keranjang'
                        );
                        print json_encode($json_data);
                    }
                }
            }
        }
    }
}






?>