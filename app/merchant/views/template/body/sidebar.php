<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item <?php if($page_active == "dashboard") echo "active";?>">
                <a class="nav-item-hold" href="<?php echo base_url().index_page(); ?>/dashboard">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item <?php if($page_active == "profile") echo "active";?>" >
                <a class="nav-item-hold" href="<?php echo base_url().index_page(); ?>/profile">
                    <i class="nav-icon i-Shop-2"></i>
                    <span class="nav-text">Profil Toko</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item <?php if($page_active == "product") echo "active";?>"  data-item="product">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Shop-4"></i>
                    <span class="nav-text">Etalase Toko</span>
                </a>
                <div class="triangle"></div>
            </li>


             <li class="nav-item <?php if($page_active == "pemesanan") echo "active";?>">
                <a class="nav-item-hold" href="<?php echo base_url().index_page(); ?>/pemesanan">
                    <i class="nav-icon i-Add-Cart"></i>
                    <span class="nav-text">Pemesanan</span>
                </a>
                <div class="triangle"></div>
            </li>

              <li class="nav-item <?php if($page_active == "mutasi") echo "active";?>" >
                <a class="nav-item-hold" href="<?php echo base_url().index_page(); ?>/mutasi">
                    <i class="nav-icon i-Coins"></i>
                    <span class="nav-text">Mutasi</span>
                </a>
                <div class="triangle"></div>
            </li>

             <li class="nav-item <?php if($page_active == "withdraw") echo "active";?>" >
                <a class="nav-item-hold" href="<?php echo base_url().index_page(); ?>/withdraw">
                    <i class="nav-icon i-Inbox-Out"></i>
                    <span class="nav-text">Penarikan Saldo</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
      
      
     
        <ul class="childNav" data-parent="product">
            <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/product">
                    <i class="nav-icon i-Shop-2"></i>
                    <span class="item-name">Produk</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/product/add_product">
                    <i class="nav-icon i-Add "></i>
                    <span class="item-name">Tambah Produk</span>
                </a>
            </li>
             <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/product/trash_product">
                    <i class="nav-icon i-Remove "></i>
                    <span class="item-name">Produk Yang Di Hapus</span>
                </a>
            </li>
        </ul>

     <!--    <ul class="childNav" data-parent="pesanan">

             <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan">
                    <i class="nav-icon i-Full-Cart "></i>
                    <span class="item-name">Semua Pesanan</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan">
                    <i class="nav-icon i-Full-Cart"></i>
                    <span class="item-name">Pesanan Masuk</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan">
                    <i class="nav-icon i-Full-Cart "></i>
                    <span class="item-name">Pesanan Berhasil</span>
                </a>
            </li>
        </ul> -->
      

        


    </div>
    <div class="sidebar-overlay"></div>
</div>