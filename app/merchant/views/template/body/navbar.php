   <body class="text-left">
        
        <!-- Pre Loader Strat  -->
        <div class='loadscreen' id="preloader">

            <div class="loader spinner-bubble spinner-bubble-primary">


            </div>
        </div>

        <div class="app-admin-wrap layout-sidebar-large clearfix">
            <div class="main-header">
            <div class="logo">
                <img src="http://gull-html-laravel.ui-lib.com/assets/images/logo.png" alt="">
            </div>

            <div class="menu-toggle">
                <div></div>
                        <div></div>
                <div></div>
            </div>

            <div class="d-flex align-items-center">
                <!-- Mega menu -->
                <div class="dropdown mega-menu  d-md-block" style="padding-top: 12px;padding-left: 12px">
                   <h4 class="nav_saldo">Rp.11.111</h4>
                </div>
            </div>

            <div style="margin: auto"></div>

            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                <!-- Notificaiton -->
                <div class="dropdown">
                    <div class="badge-top-container" role="button"   data-toggle="modal" data-target="#modal_notif">
                        <span class="badge badge-primary" id="notif_pemesanan"></span>
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                </div>
                <!-- Notificaiton End -->

                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user colalign-self-end">
                        <img src="http://gull-laravel.ui-lib.com/assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> <?= array_data_merchant($this->session->userdata('id'))->name_merchant; ?>
                            </div>
                            <a class="dropdown-item popins" href="<?php echo base_url().index_page(); ?>/login/ajax_action_logout">Sign out</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>