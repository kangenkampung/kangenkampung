 <script src="<?php echo base_url(); ?>assets/admin/ajaxFunction.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/bootstrap.bundle.min.js "></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/perfect-scrollbar.min.js "></script>
<script src="<?php echo base_url(); ?>assets/admin/js/vendor/echarts.min.js "></script>

<script src="<?php echo base_url(); ?>assets/admin/js/es5/echart.options.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/es5/dashboard.v4.script.min.js "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/es5/script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/kangenkampung.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/es5/sidebar.large.script.min.js"></script>
</body>
<script type="text/javascript">
	var link = "<?php echo base_url().$this->config->item('index_page'); ?>/mutasi/get_saldo/";
          var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          ajaxShowData(link, "POST", form_data, function(response) {
               $('.nav_saldo').text(addNumber(response,true));
               if(response < 10000){
               	$('#btn_wd').hide()
               }else{
               	$('#btn_wd').show()
               }
          });
	
	$('.select2').select2(); 
</script>

<script type="text/javascript">
$(function() {
    notif();
});
  function notif(){
     var trHTML = '';
     $('#div_notif').empty();
      var link = "<?php echo base_url().$this->config->item('index_page'); ?>/dashboard/ajax_notif_merchant/";
      var form_data = new FormData();
      ajaxShowData(link, "POST", form_data, function(response) {
               if(response.result){
                    $('#notif_pemesanan').text(response.count)

                    for (var i = 0; i < response.data.data.length; i++) { 
                             var table =  '<div class="col-11">'+
                                             '<div class="row">'+
                                                     '<div class="col-sm-4">'+
                                                          '<h6>Nama Pemesan</h6>'+
                                                     '</div>'+
                                                     '<div class="col-sm-8">'+
                                                          '<h6>'+response.data.data[i].name_member+'</h6>'+
                                                     '</div>'+
                                             '</div>'+
                                             '<div class="row">'+
                                                     '<div class="col-sm-4">'+
                                                          '<h6>Nama Produk</h6>'+
                                                     '</div>'+
                                                     '<div class="col-sm-8">'+
                                                          '<h6 >'+response.data.data[i].name_product+'</h6>'+
                                                     '</div>'+
                                             '</div>'+
                                             '<div class="row">'+
                                                     '<div class="col-sm-4">'+
                                                          '<h6>Jasa Pengiriman</h6>'+
                                                     '</div>'+
                                                     '<div class="col-sm-8">'+
                                                          '<h6>'+response.data.data[i].name_kurir+'</h6>'+
                                                     '</div>'+
                                             '</div>'+
                                             '<hr>'+
                                     '</div>'+    
                                     '<div class="col-1">'+
                                             '<h6>#1</h6>'+
                                     '</div>'+
                                     '<hr>'; 
                              trHTML += table;
                         }
                          $('#div_notif').append(trHTML);
               }
               
          });

     $('#page-load').hide();
      $('#preloader').hide();
  }

</script>
</html>