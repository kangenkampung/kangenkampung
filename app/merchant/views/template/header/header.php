<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kangen Kampung Merchant</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/css/themes/lite-purple.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/kangenkampungadmin.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/vendor/datatables.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">
      <script src="<?php echo base_url(); ?>assets/admin/js/vendor/datatables.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
</head>

<style type="text/css">
 .loader {
position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    color:darkred;
}
</style>
<div id="page-load" style="position: fixed; z-index: 999999; opacity: 0.7;  background: white; width: 100%; height: 100%; display: block;top: 0;  ">
    <div class="loader">
      <div>
        <img src="<?php echo base_url(); ?>/assets/loading.gif"></div>
      </div>
 </div>