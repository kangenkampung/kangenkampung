<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/*-------- GET DATA ARRAY RESELLER --------*/
if(!function_exists('array_data_reseller')) {
	function array_data_merchant($param) {
		$CI = &get_instance();
		$CI->load->database();

		$CI->db->select('*');
		$CI->db->from('mr__merchant');
		$CI->db->where('id_merchant', $param);
		$result = $CI->db->get();
		return $result->row();
	}
}

/*-------- SET IDR NOMINAL --------*/
if(!function_exists('addNumber')) {
	function addNumber($number, $idr = true) {
		$CI = &get_instance();
		$CI->load->database();
		return 'Rp. '. str_replace(',','.', number_format($number));
	}
}


