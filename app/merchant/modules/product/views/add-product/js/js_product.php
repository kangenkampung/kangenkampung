
<script type="text/javascript">
	function target(id){
		$(id).trigger("click");
	}

	function readURL(input,value,icon) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $(value).attr('src', e.target.result);
      $(icon).css('display','none');
    }
    
    reader.readAsDataURL(input.files[0]);
  }
} 
</script>

<script type="text/javascript">
  $(function() {
      ajax_action_get_all_category();
      set_kota();
  });
</script>

<script type="text/javascript">
	   function ajax_action_get_all_category(){
          var link = "<?php echo base_url().'admin.php/'; ?>/category/ajax_action_get_all_category/";
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i; 
          ajaxShowData(link,"POST",form_data,function(response) {
          	 for(i=0; i<response.data.length; i++){
               html += "<option value='"+response.data[i].id_category+"'>"+response.data[i].name_category+"</option>";
           }
                 $('#kategory_product').html(html);
             $('#page-load').hide();
          });     
        }
</script>

<script type="text/javascript">
	 function tambah_product(){
      var name_product      	  =  $('#name_product').val();
      var catatan_product       =  $('#catatan_product').val();
      var minimum_product     	=  $('#minimum_product').val();
      var berat_product 		    =  $('#berat_product').val();
      var id_kategory 			    =  $('#kategory_product').val();
      var informasi_product     =  $('#informasi_product').val();
      var harga_product         =  $('#harga_product').val();
      var expired_product       =  $('#expired_product').val();
      var stok_product          =  $('#stok_product').val();
      var lokasi_product        =  $('#wilayah :selected').text();
      var kota_product          =  $('#wilayah').val();
      var dimensi_product          =  $('#dimensi_product').val();
      var diskon_product        =  $('#diskon_product').val();
      var file_data			 	      = $('#target_1').prop('files')[0];
      var file_data2 		        = $('#target_2').prop('files')[0];
      var file_data3 		        = $('#target_3').prop('files')[0];
      var file_data4 		        = $('#target_4').prop('files')[0];
      // var file_data5            = $('#target_5').prop('files')[0];
      // var file_data6            = $('#target_6').prop('files')[0];
          var form_data         = new FormData();
          form_data.append('file', file_data);
          form_data.append('file2', file_data2);
          form_data.append('file3', file_data3);
          form_data.append('file4', file_data4);
          // form_data.append('file5', file_data5);
          // form_data.append('file6', file_data6);
          form_data.append('name_product', name_product);
          form_data.append('informasi_product', informasi_product);
          form_data.append('catatan_product', catatan_product);
          form_data.append('minimum_product', minimum_product);
          form_data.append('kota_product', kota_product);
          form_data.append('berat_product', berat_product);
          form_data.append('harga_product', harga_product);
           form_data.append('dimensi_product', dimensi_product);
          form_data.append('expired_product', expired_product);
          form_data.append('stok_product', stok_product);
          form_data.append('id_kategory', id_kategory);
          form_data.append('lokasi_product', lokasi_product);
          form_data.append('diskon_product', diskon_product);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_add_product","POST",form_data);

    }
</script>

<script type="text/javascript">
    function set_kota(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "city");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#wilayah').html(html);
        $('#page-load').hide();
        });
  }
</script>