
<script type="text/javascript">
  $(function() {
    ajax_action_get_all_product()
  });
</script>
<script type="text/javascript">
     function ajax_action_get_all_product(){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>/product/get_data_product_by_all/";
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
             var trHTML = ''; 
          ajaxShowData(link,"POST",form_data,function(response) {
            var data = response.data.product;
            for (var i = 0 ; i < data.length; i++) {
              var diskon =  parseInt(data[i].diskon_product) /100 * data[i].harga_product;
              var diskon_total =  data[i].harga_product - diskon;

              if(data[i].diskon_product > 0){
                var discount_price = "<span class='price-diskon-text'>"+addNumber(data[i].harga_product,true)+"</span>";
                var discount_title =  "<div class='price-diskon'>"+data[i].diskon_product+"% OFF</div>";
                var real_price     =    "<div style='' class='primary-color price-item'><b>"+addNumber(diskon_total,true)+"</b></div>";
              }else{
                 var discount_price = "<span class=''></span>";
                 var discount_title =  "<div class=''></div>";
                 var real_price      =    "<div style='' class='primary-color price-item'><b>"+addNumber(data[i].harga_product,true)+"</b></div>";
              }
               

               if(data[i].status_product == "1"){
                  var enabledisable = "<div style='text-align: right;padding: 6px;cursor:pointer'>"+
                                                "<div style='' class='title-item'>"+'<button class="badge badge-success badge-sm" onclick="disabled_product(\''+data[i].id_product+'\')"><i class="i-Yes" style="font-size:15px"></i></button>'+'<button class="badge badge-danger badge-sm" onclick="delete_product(\''+data[i].id_product+'\')"><i class="i-Remove" style="font-size:15px"></i></button>'+"</div>"+
                                            "</div>";
               }else{
                  var enabledisable = "<div style='text-align: right;padding: 6px;cursor:pointer'>"+
                                                "<div style='' class='title-item '>"+'<button class="badge badge-warning badge-sm" onclick="enable_product(\''+data[i].id_product+'\')"><i class="i-Close" style="font-size:15px"></i></button>'+'<button class="badge badge-danger badge-sm" onclick="delete_product(\''+data[i].id_product+'\')"><i class="i-Remove" style="font-size:15px"></i></button>'+"</div>"+
                                            "</div>";
               }
               var datas = "<div class='card-item'>"+
                                   "<div class='card'>"+
                                        "<a href='<?php echo base_url().index_page(); ?>/product/edit_product/"+data[i].id_product+"'>"+
                                            "<div class='img-hover-zoom '>"+
                                                "<img src='"+"<?php echo base_url(); ?>uploads/product/"+data[i].name_file+"' style='height:169px'>"+
                                                "<div class='top-right'>"+
                                                  discount_title+ 
                                                "</div>"+

                                            "</div>"+
                                            "<div style='text-align: left;padding: 6px'>"+
                                                "<div style='' class='title-item'>"+data[i].name_product+"</div>"+
                                                "<div style='' class='title-item'> Stok "+data[i].stok_product+"</div>"+
                                                discount_price+
                                                real_price+
                                                "<div style='' class='location-item'>"+data[i].lokasi_product+"</div>"+
                                            "</div>"+
                                            "</a>"+
                                              enabledisable+
                                        
                                    "</div>"+
                                "</div>";
                trHTML += datas;
            }    
           $('#product_list').html(trHTML);  
           $('#page-load').hide();
          });     
        }
</script>

<script type="text/javascript">
  function disabled_product(id){
          var form_data         = new FormData();
          form_data.append('id_product', id);
          form_data.append('status', 0);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/disabled_product","POST",form_data);
  }

  function enable_product(id){
     var form_data         = new FormData();
     form_data.append('id_product', id);
     form_data.append('status', 1);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/disabled_product","POST",form_data);

  }

  function delete_product(id){
     var form_data         = new FormData();
     form_data.append('id_product', id);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/hapus_product","POST",form_data);

  }
</script>