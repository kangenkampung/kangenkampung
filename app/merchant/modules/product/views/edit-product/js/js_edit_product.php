
<script type="text/javascript">
	function target(id){
		$(id).trigger("click");
	}

	function readURL(input,value,icon) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $(value).attr('src', e.target.result);
      $(icon).css('display','none');
    }
    
    reader.readAsDataURL(input.files[0]);
  }
} 
</script>

<script type="text/javascript">
  $(function() {
      set_kota();
      ajax_action_get_all_category();
      ajax_action_get_product();
  });
</script>

<script type="text/javascript">
	   function ajax_action_get_product(){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>/product/get_data_product_by_id/";
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          form_data.append('id_product', '<?= $this->uri->segment(3); ?>');
          image_link = "<?php echo base_url();?>/uploads/product/";
          ajaxShowData(link,"POST",form_data,function(response) {
            var kategory_product      =  response.data.product[0].id_kategory;
            var name_product          =  $('#name_product').val(response.data.product[0].name_product);
            var catatan_product       =  $('#catatan_product').val(response.data.product[0].catatan_product );
            var minimum_product       =  $('#minimum_product').val(response.data.product[0].minimum_product);
            var berat_product         =  $('#berat_product').val(response.data.product[0].berat_product);
            var id_kategory           =  $("#kategory_product  option[value="+kategory_product+"]").prop("selected", "selected");
            var informasi_product     =  $('#informasi_product').val(response.data.product[0].informasi_product);
              var dimensi_product     =  $('#dimensi_product').val(response.data.product[0].dimensi_product);
            var harga_product         =  $('#harga_product').val(response.data.product[0].harga_product);
            var expired_product       =  $('#expired_product').val(response.data.product[0].expired_product);
            var stok_product          =  $('#stok_product').val(response.data.product[0].stok_product);
            $("#wilayah").select2("val", response.data.product[0].kota_product);
            var diskon_product        =  $('#diskon_product').val(response.data.product[0].diskon_product);
             $('#page-load').hide();
            $("#prev_1").attr("src",image_link+response.data.image[0].name_file);
            $("#prev_2").attr("src",image_link+response.data.image[1].name_file);
            $("#prev_3").attr("src",image_link+response.data.image[2].name_file);
            $("#prev_4").attr("src",image_link+response.data.image[3].name_file);
            $("#prev_5").attr("src",image_link+response.data.image[4].name_file);
            $("#prev_6").attr("src",image_link+response.data.image[5].name_file);
          });  

        }
</script>



<script type="text/javascript">
     function ajax_action_get_all_category(){
          var link = "<?php echo base_url().'admin.php/'; ?>/category/ajax_action_get_all_category/";
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
              var html = '<option value="" selected="selected">Pilih Kategori</option>';
            var i; 
          ajaxShowData(link,"POST",form_data,function(response) {
             for(i=0; i<response.data.length; i++){
               html += "<option value='"+response.data[i].id_category+"'>"+response.data[i].name_category+"</option>";
           }
                 $('#kategory_product').html(html);
             $('#page-load').hide();
          });     
        }
</script>
<script type="text/javascript">
	 function edit_product(){
      var name_product      	  =  $('#name_product').val();
      var catatan_product       =  $('#catatan_product').val();
      var minimum_product     	=  $('#minimum_product').val();
      var berat_product 		    =  $('#berat_product').val();
      var id_kategory 			    =  $('#kategory_product').val();
      var informasi_product     =  $('#informasi_product').val();
      var harga_product         =  $('#harga_product').val();
      var expired_product       =  $('#expired_product').val();
      var stok_product          =  $('#stok_product').val();
      var lokasi_product        =  $('#wilayah :selected').text();
      var lokasi_id_product     =  $('#wilayah').val();
      var diskon_product        =  $('#diskon_product').val();
      var dimensi_product          =  $('#dimensi_product').val();
      var id_product            =  '<?= $this->uri->segment(3); ?>';
      var file_data			 	      = $('#target_1').prop('files')[0];
      var file_data2 		        = $('#target_2').prop('files')[0];
      var file_data3 		        = $('#target_3').prop('files')[0];
      var file_data4 		        = $('#target_4').prop('files')[0];
      // var file_data5            = $('#target_5').prop('files')[0];
      // var file_data6            = $('#target_6').prop('files')[0];
          var form_data         = new FormData();
          form_data.append('file', file_data);
          form_data.append('file2', file_data2);
          form_data.append('file3', file_data3);
          form_data.append('file4', file_data4);
          // form_data.append('file5', file_data5);
          // form_data.append('file6', file_data6);
           form_data.append('dimensi_product', dimensi_product);
          form_data.append('name_product', name_product);
          form_data.append('catatan_product', catatan_product);
          form_data.append('minimum_product', minimum_product);
          form_data.append('berat_product', berat_product);
          form_data.append('harga_product', harga_product);
          form_data.append('kota_product', lokasi_id_product);
          form_data.append('expired_product', expired_product);
          form_data.append('stok_product', stok_product);
          form_data.append('informasi_product', informasi_product);
          form_data.append('id_kategory', id_kategory);
          form_data.append('lokasi_product', lokasi_product);
          form_data.append('diskon_product', diskon_product);
          form_data.append('id_product', id_product);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_edit_product","POST",form_data);

    }
</script>


<script type="text/javascript">
    function set_kota(){
       var link2 = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_ongkir";
        $('#page-load').show();
         var form_data = new FormData();
      form_data.append('param', "city");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
         ajaxShowData(link2,"POST",form_data,function(response) {;
              var html = '<option value="" selected="selected">Pilih Lokasi</option>';
            var i;
        for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#wilayah').html(html);
        $('#page-load').hide();
        });
  }
</script>