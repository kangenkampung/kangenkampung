<div class="main-content-wrap sidenav-open d-flex flex-column">
    <div class="breadcrumb">
        <h1>Pemesanan</h1>
        <ul>
            <li><a href="">System</a></li>
            <li>Cek Pembayaran</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">

        <div class="col-md-12 mb-3">
            <div class="card text-left">
                <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h4 class="card-title mb-3">Table Pengecekan Pesanan</h4> 
                    </div>
                </div>
                   <div style="margin-top: 3%"></div>
                    <div class="table-responsive">
                        <table class="table" id="list_pemesanan">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Nama Member</th>
                                    <th scope="col">Nama Produk</th>
                                    <th scope="col">Nama Kurir</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Tujuan</th>
                                    <th scope="col">Code Pengiriman</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- end of col-->

       
        <!-- end of col-->

    </div>

    <?= $this->load->view('js/js')?> 