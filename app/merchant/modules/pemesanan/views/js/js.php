<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_pemesanan","<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan/ajax_action_datatable_pemesanan",form_data);
    });

</script>
<script type="text/javascript">
	function modal_status(id) {
    var trHTML = '';
    $('#table_cart').empty();
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>pemesanan/ajax_action_get_detail_pemesanan/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        $("#pemesanan_preview").attr("src", "<?= base_url().'uploads/bukti_pembayaran/';?>" + response.data[0].nama_file);
        $('#pemesanan_price').val(addNumber(response.data[0].total_order,true));
        $('#id_order').val(id);
        $('#pemesanan_bank').val(response.data[0].name_payment_method);
        $("#status_kurir option[value='" + response.data[0].status_kurir + "']").attr('selected', 'selected');
        $('#page-load').hide();

        for (var i = 0; i < response.cart.length; i++) { 
         var table = '<tr><td>' + response.cart[i].id_cart +
                  '</td><td>' + response.cart[i].name_product +
                  '</td><td>' + response.cart[i].name_merchant + 
                  '</td><td>' + addNumber(response.cart[i].price,true) +  
                  '</td><td>' +response.cart[i].name_kurir+'</td></tr>' 

    
        trHTML += table;
          }
          $('#table_cart').append(trHTML);

    });
    
    $('#modal_status').modal('show');


}
</script>

<script type="text/javascript">
  function change_status(id){
          var form_data = new FormData();
          form_data.append('id_order', $('#id_order').val());
          form_data.append('status_order', id);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan/ajax_action_change_status/","POST",form_data);

  }
</script>

<script type="text/javascript">
  function resi(){
          
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_tracking/";
          var form_data = new FormData();resi
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          form_data.append('resi', $('#resi').val());
          form_data.append('id_order', $('#resi_id_order').val());
          form_data.append('jasa', $('#resi_jasa').val());
          ajaxShowData(link, "POST", form_data, function(response) {
              if(response.rajaongkir.status.code == 400){
                     message("Mohon Maaf", "Barang Dengan Kode Tidak di temukan di layanan pengiriman "+ $('#resi_jasa').val(), "error","info",10000);
              }else{
                if(response.rajaongkir.result.delivered){
                   message("Mohon Maaf", "Barang Dengan Kode ini Telah sampai Ke Tujuan", "info","info",10000);
                }else{
                   change_resi($('#resi_id_order').val(),$('#resi').val());
                }
              }
              $('#page-load').hide();
          });

  }
</script>


<script type="text/javascript">
  function change_resi(id,resi){
          var form_data = new FormData();
          form_data.append('id_order', id);
          form_data.append('status_order', 3);
          form_data.append('resi', resi);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan/ajax_action_change_resi/","POST",form_data);

  }
</script>


<script type="text/javascript">
function update_resi(id,layanan){
  $('#resi_id_order').val(id);
  $('#resi_jasa').val(layanan);
  $('#modal_resi').modal('show');
 }
</script>

<script type="text/javascript">
  function cek_resi(id){
    var trHTML = '';
    $('#data_resi').empty();
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/pemesanan/ajax_action_get_detail_tracking/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        for (var i = 0; i < response.data.length; i++) { 
          if(response.data[i].pod_receiver == "kosong"){
            var penerima = "Belum Sampai";
          }else{
            var penerima = response.data[i].pod_receiver;
          }
         var table = '<tr><td>' + response.data[i].id_tracking +
                  '</td><td>' + penerima +
                  '</td><td>' + response.data[i].city_name + 
                  '</td><td>' + response.data[i].manifest_description +
                  '</td><td>' +response.data[i].status_tracking+'</td></tr>' 
        trHTML += table;
          }
          $('#data_resi').append(trHTML);
          $('#page-load').hide();

    });
    
     $('#modal_cek_resi').modal('show');





  }
</script>