<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends MY_Controller {

	public function __construct() {
		$this->load->model('M_Pemesanan');
		$this->load->library('recaptcha');
	}

		public function index(){
		$data['content'] = 'Pemesanan';
		$data['page_active'] = 'Pemesanan';
		$this->load->view('template',$data);	
		
	}

	function ajax_action_datatable_pemesanan(){
		$column = "*";
		$table = "m__cart a";
		$column_order = array('id_cart', 'name_member', 'status_cart');
		$column_search = array('id_cart', 'name_member', 'status_cart');
		$order = array('id_cart' => 'DESC'); 
		$where = 'a.id_merchant ='."'".$this->session->userdata('id')."'"." AND (status_cart = 4 OR status_cart = 3 OR status_cart = 0)";
		$joins = array(
			array(
				'table' => 'm__member b',
				'condition' => 'a.id_member = b.id_member',
				'jointype' => ''
			),
			array(
				'table' => 'mr__product c',
				'condition' => 'a.id_produk = c.id_product',
				'jointype' => ''
			),
			array(
				'table' => 'm__alamat d',
				'condition' => 'd.id_alamat = a.id_alamat',
				'jointype' => ''
			),
		);
		$list = $this->M_Pemesanan->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
		
		$link = ''.base_url().$this->config->item('index_page').'/order';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;

			if($key->kode_pengiriman != ""){
				$btn = "btn btn-success btn-sm";
						if($key->status_cart == 3){
				              $text = "Proses Pengiriman";
				          }else if($key->status_cart == 0){
				              $text = "Telah Sampai ke customer";
				          }
			}else{

				$btn = "btn btn-warning btn-sm";
				$text  = "Upload Kode Pengiriman";
			}

			$row = array();
			$row[] = $no;
			$row[] = $key->created_at;
			$row[] = $key->name_member;
			$row[] = $key->name_product;
			$row[] = substr($key->name_kurir, 0, strpos($key->name_kurir, ")")).")";
			$row[] = addNumber($key->subtotal,true);
			$row[] = $key->name_penerima."|".$key->kota_alamat."|".$key->kecamatan_alamat."|".$key->kode_pos_alamat."|<br>".$key->alamat_lengkap;
		

			
			if($key->status_cart == 0){
				$row[] = '<input type="button" class="btn btn-success btn-sm" value="Pesanan telah Selesai"  onclick="cek_resi(\''.$key->kode_pengiriman.'\',\''.$key->kode_pengiriman.'\')">'; ;
					
			}else{
				if($key->kode_pengiriman != ""){
						$row[] = '<input type="button" class="'.$btn.'" value="'.$text.'" onclick="cek_resi(\''.$key->kode_pengiriman.'\',\''.$key->kode_pengiriman.'\')">'; 
					}else{
						$row[] = '<input type="button" class="'.$btn.'" value="'.$text.'" onclick="update_resi(\''.$key->id_cart.'\',\''.$key->layanan_kurir.'\')">';
					}
			}
		

			
			$data[] = $row;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_Pemesanan->count_all($table,$where,$joins),
			"recordsFiltered" => $this->M_Pemesanan->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	  function ajax_action_get_detail_tracking(){
		$id = $this->uri->segment(3);
		$data = $this->M_Pemesanan->fetch_joins('a__tracking', '*', '','no_resi ='."'". $id."'", '',TRUE);
		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,	
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data,
				'cart' => $order_cart
			);
			print json_encode($json_data);
		}
	}



	  function ajax_action_get_detail_pemesanan(){
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__file d',
				'condition' => 'a.id_file = d.id_file',
				'jointype' => ''
			),
			array(
				'table' => 'a__payment_account e',
				'condition' => 'a.id_payment_account = e.id_payment_account',
				'jointype' => ''
			),
			array(
				'table' => 'a__payment_method f',
				'condition' => 'e.id_payment_method = f.id_payment_method',
				'jointype' => ''
			),
		);
		$data = $this->M_Pemesanan->fetch_joins('m__order a', '*', $joins,'a.id_order ='."'". $id."'", '',TRUE);
		$joins_cart = array(
			array(
				'table' => 'mr__product b',
				'condition' => 'a.id_produk = b.id_product',
				'jointype' => ''
			),
			array(
				'table' => 'mr__merchant c',
				'condition' => 'a.id_merchant = c.id_merchant',
				'jointype' => ''
			),
		);


		$order_cart = $this->M_Pemesanan->fetch_joins('m__cart a', '*', $joins_cart,'a.id_order ='."'". $id."'"."GROUP BY id_product", '',TRUE);

		if(count($data) == 0){
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data,
				'cart' => $order_cart
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_change_status(){
		$this->form_validation->set_rules('id_order', 'id_order', 'required');
		$this->form_validation->set_rules('status_order', 'status_order', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$data = array(
				"status_order" => post("status_order"),
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_Pemesanan->update_table("m__order",$data,"id_order",post("id_order"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Merubah  Status'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Sukses Merubah Status'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'pemesanan'
				);
				print json_encode($json_data);
			}

		}
	}


function ajax_action_change_resi(){
		$this->form_validation->set_rules('id_order', 'id_order', 'required');
		$this->form_validation->set_rules('status_order', 'status_order', 'required');
		if($this->form_validation->run()==FALSE){
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE ,
				"message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}else{
			$data = array(
				"status_cart" => post("status_order"),
				"kode_pengiriman" => post("resi"),
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_Pemesanan->update_table("m__cart",$data,"id_cart",post("id_order"));
			if($edit==FALSE){
				$json_data =  array(
					"result" => FALSE ,
					"message" => array('head'=> 'Failed', 'body'=> 'Gagal Merubah  Status'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}else{
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head'=> 'Success', 'body'=> 'Kode Resi telah di tambahkan'),
					"form_error" => '',
					"redirect" => ''.base_url().$this->config->item('index_page').'/pemesanan'
				);
				print json_encode($json_data);
			}

		}
	}


	


    
}
?>