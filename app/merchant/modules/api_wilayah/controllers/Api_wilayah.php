<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_wilayah extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_wilayah');
    }
    

    

     public function ajax_get_provinci()
    {
        $data = $this->M_wilayah->fetch_table("*","sys__provinces",''); 
        print json_encode($data);

    }    

       public function ajax_get_kota()
    {
        $id = post("id");
        $data = $this->M_wilayah->fetch_table("*","sys__regencies","province_id='$id'"); 
        print json_encode($data);

    }    

       public function ajax_get_kecamatan()
    {
        $id = post("id");
        $data = $this->M_wilayah->fetch_table("*","sys__districts","regency_id='$id'"); 
        print json_encode($data);

    }    

       public function ajax_get_kelurahan()
    {
        $id = post("id");
        $data = $this->M_wilayah->fetch_table("*","sys__villages","district_id='$id'");
        print json_encode($data);

    }    

    public function get_data_ongkir(){
        $param = post('param');
        $get = $this->rajaongkir->get_kurir_data($param);
        echo $get;
    }

    public function get_ongkir(){
        $origin = 'origin=501';
        $destination = '&destination='.post('destination');
        $weight = '&weight='.post('weight');
        $kurir = '&courier='.post('kurir');
        $get = $this->rajaongkir->cek_ongkir($origin.$destination.$weight.$kurir);
        echo $get;
    }

    public function get_data_tracking(){
        $resi = "waybill=".post('resi');
        $jasa = '&courier='.post('jasa');
        $get = $this->rajaongkir->cek_tracking($resi.$jasa);
        echo $get;
    }

    
}
?>