<div class="main-content-wrap sidenav-open d-flex flex-column">
    <div class="breadcrumb">
        <ul>
            <li><a href="">Profile</a></li>
            <li>Biodata Toko Anda</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
           
            <div class="col-sm-8">
                 <div class="card-tipis" style="padding: 15px">
                    <div >
                        <div class="popins" style="font-weight: bold;font-size: 19px">Profile Tokomu</div>
                    </div>
                    <div class="row">
                            
                             <div class="col-sm-6">

                                <div style="margin-top: 4%"></div>
                                <div class="popins">Nama Toko</div>
                                <div style="margin-top: 1%"></div>
                                <input type="text" name="" class="form-control" id="name_merchant">

                                <div style="margin-top: 4%"></div>
                                <div class="popins">Owner Toko</div>
                                <div style="margin-top: 2%"></div>
                                <input type="text" name="" class="form-control" id="owner_merchant">


                                 <div style="margin-top: 4%"></div>
                                <div class="popins">Username</div>
                                <div style="margin-top: 2%"></div>
                                <input type="text" name="" class="form-control" id="username_merchant">


                                <div style="margin-top: 4%"></div>
                                <div class="popins">Password</div>
                                <div style="margin-top: 2%"></div>
                                <input type="text" name="" class="form-control" id="password_merchant">


                                  <div style="margin-top: 4%"></div>
                                <div class="popins">No Ponsel</div>
                                <div style="margin-top: 2%"></div>
                                <input type="text" name="" class="form-control" id="no_merchant">

                                

                            </div>

                            <div class="col-sm-6" id="wilayah_toko" >
                                


                                 <div style="margin-top: 4%"></div>
                                <div class="popins">Provinsi Toko</div>
                                <div style="margin-top: 1%"></div>
                                <select class="form-control select2" id="provinsi_merchant">
                                    
                                </select>


                                 <div style="margin-top: 4%"></div>
                                <div class="popins">Kota Toko</div>
                                <div style="margin-top: 1%"></div>
                                <select class="form-control select2" id="kota_merchant"></select>


                                 <div style="margin-top: 4%"></div>
                                <div class="popins">Kecamatan Toko</div>
                                <div style="margin-top: 1%"></div>
                                <select class="form-control select2" id="kecamatan_merchant"></select>


                                 <div style="margin-top: 4%"></div>
                                <div class="popins">Kode Post Toko</div>
                                <div style="margin-top: 1%"></div>
                                <input type="text" name="" class="form-control" id="kode_pos_merchant">

                                  <div style="margin-top: 4%"></div>
                                <div class="popins">Alamat Lengkap</div>
                                <div style="margin-top: 2%"></div>
                                <textarea class="form-control" id="alamat_lengkap_merchant">
                                    
                                </textarea>
                               
                            </div>

                    </div>
                    <div style="text-align: right;">
                      <div style="margin-top: 4%"></div>
                            <button class="btn btn-success" onclick="ajax_edit_profile()">Simpan Perubahan</button>
                    </div>
                </div>
            </div>
             <div class="col-sm-4">
                <div class="card-tipis">
                <div style="padding-top: 15px">
                        <div class="popins" style="font-weight: bold;font-size: 19px"><center>Detail Tokomu</center></div>
                <div style="padding: 25px;text-align: left;">
                    <div >
                        <div class="row">
                        <div class="col-4" style="font-weight: bold;size">
                           <div class="popins">Provinsi</div> 
                        </div>
                        <div class="col-8" id="text_provinsi_merchant">
                        </div>
                        </div>


                         <div class="row">
                        <div class="col-4" style="font-weight: bold;size">
                           <div class="popins">Kota</div> 
                        </div>
                        <div class="col-8" id="text_kota_merchant">
                        </div>
                        </div>


                         <div class="row">
                        <div class="col-4" style="font-weight: bold;size">
                           <div class="popins">Kecamatan</div> 
                        </div>
                        <div class="col-8" id="text_kecamatan_merchant">
                            Jawa Timur 
                        </div>
                        </div>



                           <div class="row">
                        <div class="col-4" style="font-weight: bold;size">
                           <div class="popins">Kode Pos</div> 
                        </div>
                        <div class="col-8" id="text_kode_pos_merchant">
                        </div>
                        </div>

                          <div class="row">
                        <div class="col-sm-12" style="font-weight: bold;size">
                           <div class="popins">Alamat Lengkap</div> 
                        </div>
                        <div class="col-sm-12" id="text_alamat_lengkap_merchant">
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
    <div style="margin-top: 18px"></div>
</div>

<?= $this->load->view('js/js')?>
   
        
  