<script type="text/javascript">
  $(function() {
     ajax_action_get_provinsi();
     ajax_action_get_profile();
      
    //   $('#open_wilayah').on('click', function() {
    //     $('#wilayah_toko').toggle(1000);
    //     $('html, body').animate({
    //     scrollTop: $("#alamat_lengkap_merchant").offset().top
    // }, 2000);
    // });
  });
</script>

<script type="text/javascript">
	 function ajax_action_get_profile() {
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/profile/get_data_product_by_all/";
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        $('#name_merchant').val(response.data.profile[0].name_merchant);
        $('#owner_merchant').val(response.data.profile[0].owner_merchant);
        $('#no_merchant').val(response.data.profile[0].no_merchant);
        $('#alamat_lengkap_merchant').val(response.data.profile[0].alamat_lengkap_merchant);
        $('#text_alamat_lengkap_merchant').html(response.data.profile[0].alamat_lengkap_merchant);
        $('#text_provinsi_merchant').html(response.data.profile[0].provinsi_merchant);
        $('#text_kecamatan_merchant').html(response.data.profile[0].kecamatan_merchant);
        $('#kode_pos_merchant').val(response.data.profile[0].kode_pos_merchant);
        $('#text_kode_pos_merchant').html(response.data.profile[0].kode_pos_merchant);
        $('#text_kota_merchant').html(response.data.profile[0].kota_merchant);
        $('#text_kelurahan_merchant').html(response.data.profile[0].kelurahan_merchant);
        $('#username_merchant').val(response.data.profile[0].username_merchant);

        $("#kota_merchant").select2("val", response.data.profile[0].id_kota);
        $("#province").select2("val", response.data.profile[0].id_provinsi);


        $('#page-load').hide();
    });

}
</script>

<script type="text/javascript">
    function ajax_action_get_provinsi() {
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_ongkir/";
      var form_data = new FormData();
      form_data.append('param', "province");
       form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    
    ajaxShowData(link, "POST", form_data, function(response) {
    var html = '<option value="" selected="selected">Pilih Provinsi</option>';
    var i;
        for (i = 0; i < response.rajaongkir.results.length; i++) {
             html += "<option value='"+response.rajaongkir.results[i].province_id+"'>"+response.rajaongkir.results[i].province+"</option>";
        }
        $('#provinsi_merchant').html(html);
    });
    $('#provinsi_merchant').on('change', function() {
        ajax_action_get_kota(this.value);
    });

}


function ajax_action_get_kota(id) {
    var html = '';
    var i;
    var form_data = new FormData();
    form_data.append('param', "city?province="+id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_ongkir/";
    ajaxShowData(link, "POST", form_data, function(response) {
         for(i=0; i<response.rajaongkir.results.length; i++){
          html += "<option value='"+response.rajaongkir.results[i].city_id+"'>"+response.rajaongkir.results[i].city_name+"</option>";
        }
        $('#kota_merchant').html(html);
        $('#page-load').hide();
    });

    $('#kota_merchant').on('change', function() {
        ajax_action_get_kecamatan(this.value);
    });

}


function ajax_action_get_kecamatan(id) {
    var form_data = new FormData();
    var html = '';
    var i;
      form_data.append('param', "subdistrict?city="+id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/api_wilayah/get_data_ongkir/";
    ajaxShowData(link, "POST", form_data, function(response) {
         for(i=0; i<response.rajaongkir.results.length; i++){
            html += "<option value='" + response.rajaongkir.results[i].subdistrict_id + "'>" + response.rajaongkir.results[i].subdistrict_name + "</option>";
        }
        $('#kecamatan_merchant').html(html);
        $('#page-load').hide();
    });


}





function ajax_edit_profile(){
        var name_merchant = $('#name_merchant').val();
        var owner_merchant = $('#owner_merchant').val();
        var password_merchant = $('#password_merchant').val();
        var alamat_lengkap_merchant = $('#alamat_lengkap_merchant').val();
        var kode_pos_merchant = $('#kode_pos_merchant').val();
        var username_merchant = $('#username_merchant').val();
        var no_merchant = $('#no_merchant').val();

        var provinsi_merchant = $('#provinsi_merchant option:selected' ).text();
        var id_provinsi = $('#provinsi_merchant option:selected' ).val();

        var kota_merchant = $('#kota_merchant option:selected' ).text();

        var id_kota = $('#kota_merchant option:selected' ).val();

        var kecamatan_merchant = $('#kecamatan_merchant option:selected' ).text();
        var id_kecamatan_merchant = $('#kecamatan_merchant option:selected' ).val();
         var form_data = new FormData();
          form_data.append('name_merchant',name_merchant);
          form_data.append('owner_merchant', owner_merchant);
          form_data.append('alamat_lengkap_merchant', alamat_lengkap_merchant);
          form_data.append('kode_pos_merchant', kode_pos_merchant);
          form_data.append('username_merchant', username_merchant);
          form_data.append('no_merchant', no_merchant);
          form_data.append('provinsi_merchant', provinsi_merchant);
          form_data.append('kota_merchant', kota_merchant);
          form_data.append('id_kecamatan', id_kecamatan_merchant);
          form_data.append('kecamatan_merchant', kecamatan_merchant);
          form_data.append('id_provinsi', id_provinsi);
          form_data.append('id_kota', id_kota);
          form_data.append('password_merchant', password_merchant);

          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/profile/ajax_action_edit_merchant/","POST",form_data);
            }

</script>