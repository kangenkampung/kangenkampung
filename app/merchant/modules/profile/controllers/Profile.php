<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_profile');
        $this->load->library('rajaongkir');
    }
    
    public function index()
    {
        $data['content'] = 'profile';
        $data['page_active'] = 'profile';
        $this->load->view('template', $data);
        
    }

    public function get_data_product_by_all(){
        $id= $this->session->userdata('id');
        $joins = array(
            array(
                'table' => 'mr__merchant_detail b',
                'condition' => 'a.id_detail_merchant = b.id_detail_merchant',
                'jointype' => ''
            )
        );
        $data['profile'] = $this->M_profile->fetch_joins('mr__merchant a', '*', $joins,'a.id_merchant = '."'". $id."'",TRUE);
        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }


    public function ajax_action_edit_merchant(){
        $id = $this->session->userdata('id');
            $id_detail_merchant = $this->M_profile->get_row("id_detail_merchant","mr__merchant","id_merchant ="."'".$id."'",'','',TRUE);

            // echo $id_detail_merchant['id_detail_merchant'];
            // die();
            if(post("kota_merchant") != ''){
                $data_two = array(
                "provinsi_merchant" => post("provinsi_merchant"),
                "kota_merchant" => post("kota_merchant"),
                "kecamatan_merchant" => post("kecamatan_merchant"),
                "kelurahan_merchant" => post("kelurahan_merchant"),
                "kode_pos_merchant" => post("kode_pos_merchant"),
                "alamat_lengkap_merchant" => post("alamat_lengkap_merchant"),
                "id_kota" => post("id_kota"),
                "id_provinsi" => post("id_provinsi"),
                "id_kecamatan" => post("id_kecamatan"),
                "update_at" => date("Y-m-d H:i:s")
                );
            $add_two = $this->M_profile->update_table("mr__merchant_detail",$data_two,"id_detail_merchant",$id_detail_merchant['id_detail_merchant']);
            }else{
                 $data_two = array(
                "kode_pos_merchant" => post("kode_pos_merchant"),
                "alamat_lengkap_merchant" => post("alamat_lengkap_merchant"),
                "update_at" => date("Y-m-d H:i:s")
                );
            $add_two = $this->M_profile->update_table("mr__merchant_detail",$data_two,"id_detail_merchant",$id_detail_merchant['id_detail_merchant']);
            }
               
            if(post('password_merchant') == ''){
                   $data = array(
                "name_merchant" => post("name_merchant"),
                "no_merchant" => post("no_merchant"),
                "email_merchant" => post("email_merchant"),
                "owner_merchant" => post("owner_merchant"),
                "username_merchant" => post("username_merchant"),
                "update_at" => date("Y-m-d H:i:s")
            );
            }else{
                $data = array(
                "name_merchant" => post("name_merchant"),
                "no_merchant" => post("no_merchant"),
                "email_merchant" => post("email_merchant"),
                "owner_merchant" => post("owner_merchant"),
                "username_merchant" => post("username_merchant"),
                "password_merchant" => md5(post("password_merchant")),
                "update_at" => date("Y-m-d H:i:s")
            );   
            }
         
            $edit = $this->M_profile->update_table("mr__merchant",$data,"id_merchant",$id);
            if($edit==FALSE){
                $json_data =  array(
                    "result" => TRUE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/profile'
                );
                print json_encode($json_data);
            }
        
    }

    function ajax_action_merchant(){
        $id = $this->uri->segment(3);
        $joins = array(
            array(
                'table' => 'mr__merchant_detail b',
                'condition' => 'a.id_detail_merchant = b.id_detail_merchant',
                'jointype' => ''
            ),
        );
        $data = $this->M_merchant->fetch_joins('mr__merchant a', '*', $joins,'id_merchant = \''. $id.'\'', '',TRUE);

        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
    
    

    
}
?>