<script type="text/javascript">
  $(function() {
      var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_wd","<?php echo base_url().$this->config->item('index_page'); ?>/withdraw/ajax_action_datatable_withdraw",form_data);
  });
</script>




<script type="text/javascript">
	function ajaxwithdraw() {
    Swal.fire({
        title: 'Apakah Anda akan menarik saldo anda??',
        text: "Penarikan saldo minimal Rp.10.000 !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#f5365c',
        confirmButtonText: 'Ya, Tarik semua saldo saya!'
   }).then((result) => {
  if (result.value) {
    $.ajax({
            url: '<?php echo base_url().$this->config->item('index_page'); ?>/withdraw/withdraw_saldo',
            type: "GET",
            dataType: "json",
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    message("Selamat !", data.message.body, "success", "info",1000);
                    setTimeout(function() {
                        window.location = data.redirect
                    }, 500);
                } else {
                    message("Mohon Maaf !", data.message.body, "error", "info",1000);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                message("Mohon Maaf !", 'Please try again later', "error", "info",1000);
            }
        });
  }
})
}
</script>

<script type="text/javascript">
    function show_bukti(id) {
       var trHTML = '';
    $('#table_cart').empty();
    var link = "<?php echo base_url().$this->config->item('index_page'); ?>/withdraw/ajax_action_get_detail_withdraw/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link, "POST", form_data, function(response) {
        $("#preview_wd").attr("src", "<?= base_url().'uploads/withdraw/';?>" + response.data[0].nama_file);
    });
      $('#modal_bukti_wd').modal('show');
      $('#page-load').hide();
    }
</script>

