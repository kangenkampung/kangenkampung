<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class withdraw extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_withdraw');
    }
    
    public function index()
    {
        $data['content'] = 'withdraw';
        $data['page_active'] = 'withdraw';
        $this->load->view('template', $data);
        
    }

    public function withdraw_saldo(){
           $saldo_merchant = $this->M_withdraw->fetch_joins('a__mutasi ', 'sum(total) as jumlah','', 'id_merchant ='."'".$this->session->userdata('id')."' AND status_mutasi = 1",'',TRUE);
           $sum_tambah_kurir = $this->M_withdraw->fetch_joins('a__mutasi ', 'sum(kurir_price) as jumlah','', 'id_merchant ='."'".$this->session->userdata('id')."' AND status_mutasi = 1",'',TRUE);
            $sum_wd = $this->M_withdraw->fetch_joins('a__mutasi ', 'sum(total) as jumlah','', 'id_merchant ='."'".$this->session->userdata('id')."' AND status_mutasi = 3",'',TRUE);

           $saldo = (int)$saldo_merchant[0]->jumlah + (int)$sum_tambah_kurir[0]->jumlah;
           $jumlah  =  $saldo - (int) $sum_wd[0]->jumlah;

           if($jumlah < 10000){
                 $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Penarikan saldo minimal Rp.10.000 saldo anda'. addNUmber($jumlah,true) ),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                 $count_data_detail = $this->M_withdraw->get_count_data('m__withdraw');
           $id_withdraw = "WD".date('his').$count_data_detail;

           

           $count_data_mutasi = $this->M_withdraw->get_count_data('a__mutasi');
            $id_mutasi = date('his')."MTS".$count_data_mutasi;
            // echo $key->id_order;
                  $data = array(
                        "id_mutasi" => $id_mutasi,
                        "id_order" => $id_mutasi+"WD",
                        "id_merchant" => $this->session->userdata('id'),
                        "total" => $saldo,
                        "kurir_price" => 0,
                        "status_mutasi" => 3,
                        "created_at" =>date("Y-m-d H:i:s"),
                        "id_cart" => $id_mutasi+"WD",
                        "desc_mutasi"=>"Penarikan Dana"
                    );
          $cek_mutasi = $this->M_withdraw->insert_table("a__mutasi",$data);
          if($cek_mutasi){
                $data = array(
                "id_withdraw" => $id_withdraw,
                "id_merchant" => $this->session->userdata('id'),
                "jumlah_withdraw" =>  $saldo,
                "status_withdraw" => 1,
                "created_at" => date("Y-m-d H:i:s")
            );
            $add = $this->M_withdraw->insert_table("m__withdraw",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Permintaan penarikan saldo sebesar '.addNumber($saldo,true).' anda sedang di proses'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/withdraw'
                );
                print json_encode($json_data);
            }
          }else{
               $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Saldo Anda kurang ' ),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
          } 
            }
          


         
            
    }


    function ajax_action_datatable_withdraw(){
        $column = "*";
        $table = "m__withdraw";
        $column_order = array('jumlah_withdraw');
        $column_search = array('jumlah_withdraw');
        $order = array('id_withdraw' => 'DESC'); 
        $where = 'id_merchant ='."'".$this->session->userdata('id')."'";
        $joins = '';
        $list = $this->M_withdraw->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $link = ''.base_url().$this->config->item('index_page').'/withdraw';
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->created_at;
            $row[] = addNumber($key->jumlah_withdraw);
            if($key->status_withdraw == 1){
               $row[] = "<badge class='badge badge-warning'>Sedang di proses</badge>";
            }else if($key->status_withdraw == 2){
               $row[] = "<badge class='badge badge-danger'>Penarikan anda di tangguhkan</badge>";
            }else if($key->status_withdraw == 0){
               $row[] = "<badge class='badge badge-success'>Penarikan anda telah berhasil</badge><bagde class='badge badge-warning' style='cursor:pointer' onclick='show_bukti(".$key->id_file.")'>Bukti Transfer</badge>";
           }
            if($key->update_at == ''){
               $row[] = "<badge class='badge badge-warning'>Sedang di proses</badge>";
            }else{
               $row[] = "<badge class='badge badge-success'>Di Kirim tanggal ".$key->update_at."</badge>";
           }
            $data[] = $row;
        }
    
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_withdraw->count_all($table,$where,$joins),
            "recordsFiltered" => $this->M_withdraw->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
            "data" => $data,
        );
        
        echo json_encode($output);
    }

     public function ajax_action_get_detail_withdraw(){
       $id = $this->uri->segment(3);
       $joins ='';
        $data = $this->M_withdraw->fetch_joins('a__file a', '*', $joins,'id_file = '."'".$id."'", '',TRUE);

        if(count($data) == 0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
  
}

?>