<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    
    public function __construct()
    {
        $this->load->model('M_Dashboard');
    }
    
    public function index()
    {
        $data['content'] = 'dashboard';
        $data['page_active'] = 'dashboard';
        $this->load->view('template', $data);
        
    }

    public function ajax_notif_merchant(){
        $id_merchant = "'".$this->session->userdata('id')."'";
            $joins_cart = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'm__member c',
                'condition' => 'a.id_member = c.id_member',
                'jointype' => ''
            ),
        );
        $data['data'] = $this->M_Dashboard->fetch_joins('m__cart a', 'a.*,c.name_member,b.name_product', $joins_cart, 'status_cart = "4" AND a.id_merchant = '.$id_merchant, '',TRUE);

        if(count($data['data']) > 0){
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses get order'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data,
                "count" => count($data['data'])
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal get order'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }
    }

    public function ajax_dashboard_data(){
          $id_merchant = "'".$this->session->userdata('id')."'";
            $joins_cart = array(
            array(
                'table' => 'mr__product b',
                'condition' => 'a.id_produk = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'm__member c',
                'condition' => 'a.id_member = c.id_member',
                'jointype' => ''
            ),
        );
        $data['pemesanan'] = $this->M_Dashboard->fetch_joins('m__cart a', 'a.*,c.name_member,b.name_product', $joins_cart, 'a.id_merchant = '.$id_merchant, '',TRUE);
        $data['data'] = $this->M_Dashboard->fetch_joins('m__cart a', 'a.*,c.name_member,b.name_product', $joins_cart, '(status_cart = "4" OR status_cart = "3" OR status_cart = "0") AND a.id_merchant = '.$id_merchant, '',TRUE,'','',5);
        $data['product'] = $this->M_Dashboard->fetch_joins('mr__product a', '', '', 'a.id_merchant = '.$id_merchant, '',TRUE);

        if(count($data) > 0){
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses get order'),
                "form_error" => '',
                "redirect" => '',
                "pemesanan" => count($data['pemesanan']),
                "product" => count($data['product']),
                "cart"=>$data['data']
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Gagal get order'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }
    }
 
    

    
}
?>