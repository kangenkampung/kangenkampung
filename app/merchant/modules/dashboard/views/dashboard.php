<div class="main-content-wrap sidenav-open d-flex flex-column">
    <div class="breadcrumb">
        <ul>
            <li><a href="">Dashboard</a></li>
            <li>Dashboard Toko Anda</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>

          <div class="row">
                <div class="col-lg-6 col-md-12">
                    <!-- CARD ICON -->
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="i-Data-Upload"></i>
                                    <div class=" mt-2 mb-2 popins" >Product Anda</div>
                                    <p class="text-primary text-24 line-height-1 m-0 count_product"></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="i-Add-Cart"></i>
                                    <div class=" mt-2 mb-2 popins" >Pesanan Anda</div>
                                    <p class="text-primary text-24 line-height-1 m-0 count_pesanan"></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="i-Money-2"></i>
                                    <div class=" mt-2 mb-2 popins" >Penghasilan Anda</div>
                                    <p class="text-primary text-24 line-height-1 m-0 nav_saldo"></p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="card mb-4">
                        <div class="card-body p-0">
                         <div class="card-title m-0 p-3">Pembelian Terakhir</div>
                            <div id="null_cart" style="display: none">
                                <Center>
                                 <i class="i-Add-Cart text-32 mr-3"></i><br>
                                Belum Ada Pembelian
                                </Center>
                            </div>
                            <div id="list_last_order" style="padding: 15px"></div>


                    </div>
                </div>
            </div>
            <!-- end of row -->
           
    <div style="margin-top: 18px"></div>
</div>

<?= $this->load->view('js/js')?>
   
        
  