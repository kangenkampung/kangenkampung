<script type="text/javascript">
		$(function() {
		    dashboard_data();
		});
		function dashboard_data(){
			var trHTML = '';
			var link = "<?php echo base_url().$this->config->item('index_page'); ?>/dashboard/ajax_dashboard_data/";
          var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          ajaxShowData(link, "POST", form_data, function(response) {
               if(response.result){
               	$('.count_pesanan').text(response.pemesanan);
               	$('.count_product').text(response.product);


               	  for (var i = 0; i < response.cart.length; i++) { 

               	if(response.cart[i].status_cart == "3"){
               		var style="btn btn-outline-warning btn-rounded btn-sm";
               		var text = "Segera Proses";
               	}else if(response.cart[i].status_cart == "4"){
               		var style="btn btn-outline-info btn-rounded btn-sm";
               		var text = "Sedang Proses";
               	}else{
               		var style="btn btn-outline-success btn-rounded btn-sm";
               		var text = "Berhasil";
               	}
                             var table = ' <div class="col-sm-12">'+
                               '<div class="row">'+
                                 '<div class="col-8">'+
                                 	  ' <label><i class="i-Add-Cart text-32 mr-3"></i></label>'+
	                                 '<label><div class="flex-grow-1">'+
	                                    '<h6 class="m-0">'+response.cart[i].name_product+'</h6>'+
	                                    '<p class="m-0 text-small text-muted">'+response.cart[i].name_member+'</p>'+
	                                '</div></label>'+
	                              '</div>'+
                                '<div class="col-4">'+
                                    '<button class="'+style+'">'+text+'</button>'+
                                '</div>'+
                                '</div>'+
                            '</div>'; 
                             trHTML += table;
                         }
 					$('#list_last_order').append(trHTML);
               }
          });
		}
		
</script>